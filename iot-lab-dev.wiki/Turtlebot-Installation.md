### Hardware prerequisite

* a fully assembled Turtlebot version 2 :
   * 1x Kinect with electrical cable 12V@1A
   * 1x Netbook running under Ubuntu LTS (see which version below) with electrical cable 19V@2A 
   * 1x a Dock with AC adaptater
* an IoT-LAB M3 node :
   * 1x IoT-LAB nodes, with eletrical cable on the 5V@1A connector
   * 1x RJ45 between the IoT-LAB (uplink port) node and the netbook

### Wi-Fi prerequisite

You must have a **Wi-Fi hotspot** in your experimental room which routing Iot-LAB Gateway nodes private network of your site (eg. 172.16.x.0/21)

Be careful, you have to choose an ip address **at the end** of the subnet. [[View Server configuration files |Configuration]] (eg. prod and dev cfg files) and **don't use an ip already used**.


### Resources configuration

Add your robot in nodes configuration file (eg. m3.csv)

  * node_id = M3 node id (eg. m3-<id>.site_name.iot-lab.info)
  * archi = m3:at86rf231
  * hwaddr = **Laptop Mac address** (eg. dynamic DHCP for M3 gateway node)
  * mobile = 1
  * mobility_type = turtlebot2
  * midspan_port = -1

[[View Configuration page|Configuration]]

### Turtlebot configuration

Get ROS application for managing robots

```
your_computer$ git clone git@github.com:iot-lab/iot-lab-dev.git
your_computer$ cd iot-lab-dev
your_computer$ make setup-robots
your_computer$ cd parts/robots
your_computer$ git checkout <indigo|kinetic>
```

You have to create and add configuration files for your Client site

* a map site used by the AMCL navigation
   * turtlebotv2/iotlab_maps/maps/**\<site\>-iotlab.yaml** => description and scale
   * turtlebotv2/iotlab_maps/maps/**\<site\>-iotlab.pgm** => bitmap image1
* coordinates for Turtlebot's docks **(1)**
   * turtlebotv2/iotlab_maps/goals/**\<site\>_iotlab_docks_pose.yaml** 
* coordinates for Turtlebot's docking area **(2)**
   * turtlebotv2/iotlab_maps/goals/**\<site\>_showroom_docking_area.yaml**

**Be careful**, the name of the robot in this files have a pre-defined syntax **\<mobility_type\>\_\<archi\>\_\<node_id\>** (e.g. turtlebot2_m3_381)

**(1)** These coordinates are used for the initial position set at the beginning of the navigation

**(2)** These coordinates ared used for the docking approach (one meter in front of the dock's target)

### Server configuration file

Add in Install library server configuration files (dev.cfg or prod.cfg) a subsection robot in your entry server name.

```
[server name]
    ...
    ** [[host]]
       ...
    ** [[guest]]
       ...
    [[robot]]
       robot_netbook_eth0_ipaddr = 10.255.255.254
       robot_netbook_eth0_network = 10.255.255.0
       robot_netbook_eth0_netmask = 255.255.255.0
       robot_netbook_eth0_broadcast = 10.255.255.255
       robot_node_ipaddr = 10.255.255.1
       robot_map_file = <site>-iotlab.yaml
       robot_docks_pose = <site>_iotlab_docks_pose.yaml
       robot_docking_area = <site>_iotlab_docking_area.yaml
       robot_battery_s1 = 50
       robot_battery_s2 = 20
       robot_ros_version = <indigo|kinetic>
```
**robot_netbook_*** and **robot_node_ipaddr** parameters are the network configuration for the M3 Iot-LAB node

**robot_battery_*** are the battery life threshold parameters. For example if the battery is below 20 percent the robot will go automatically to DOCK station.

**robot_ros_version** fix the version of ROS (indigo or kinectic)

### Turtlebot Installation

1. Choose your Ubuntu Desktop version

   * for **ROS Indigo** use **Ubuntu 14.04.x** download the last version http://releases.ubuntu.com/14.04/
   * for **ROS Kinetic** use **Ubuntu 16.04.x** download the last version http://releases.ubuntu.com/16.04/ 
  
   Make a bootable USB stick with [Etcher](https://etcher.io).

2. Boot up your image on the robot netbook (Asus X200M **Echap key** for boot sequence)

3. Install options :

  * Language : English
  * Locale : en_US.UTF-8
  * Name and computer name = turtlebot
  * User : login = turtlebot password = turtlebot

  **At the end of the installation** you have to verify your options in Power Management (e.g. Don't Suspend the activity of the netbook)

4. Configure manually netbook network interfaces
  
  * Reminder about robot network interfaces
    * Wi-Fi : link to the IoT-LAB infrastructure
    * Ethernet : link to the embedded IoT-LAB node

  For the Wi-Fi configuration, we use wpa_supplicant configuration https://doc.ubuntu-fr.org/wpa
  with DHCP.

  * Add your robot in the DHCP server of your Client site

       ```
       your_computer$ cd iot-lab-dev/install-lib
       your_computer$ fab <prod|dev> srvdhcp.setup -H root@<host_kvm>:2222
       ```
  * Edit WPA configuration, by copying the content of `template/srvturtlebot2/wpa_<site>.conf`.
 
       ```
       netbook$ vi /etc/wpa.conf
       ```
      
  * Remove if installed **network-manager** package in order to use the static configuration file `/etc/network/interfaces` (see below)

       ```
       netbook$ sudo apt-get remove network-manager
       ```

  * Edit netbook's `/etc/network/interfaces` file and add this section (both Wi-Fi **and** Ethernet), please adapt interface name according to your kernel (check current names with ifconfig -a)

       ```
       auto wlan0
       iface wlan0 inet dhcp
            wpa-conf /etc/wpa.conf

       auto eth0
       iface eth0 inet static
            address 10.255.255.254
            netmask 255.255.255.0
            network 10.255.255.0
       ```

  * Test your Wi-Fi configuration 
       
       ```
       netbook$ sudo reboot
       ```

5. Install openssh-server on the netbook

    ```
    netbook$ sudo apt-get update
    netbook$ sudo apt-get install openssh-server
    ```

  <em>
  **Tip1** - When you specify `fab <dev|prod> ...` command, Fabric will execute the task on all the robots list based on your resources file (eg. based on mobility_type attribute = turtlebot2). If you want to execute the task only on a subset of your robots you can use a robot list id (e.g. based on node_id attribute) with the following command :

    ```
    your_computer$ fab <dev|prod>:robot_ids=1-3+8 <task_name> -H root@<host_kvm>:2222
    ```
  </em>

6. Configure SSH access on the netbook with admins and KVM host keys:

    ```
    your_computer$ fab <dev|prod> turtlebot2.add_ssh -H root@<host_kvm>:2222
    ```

  After this step your robot's netbook is reachable by KVM host with SSH on port 2222

  <em>
  **Tip2** - Add (or create) on your computer a SSH config file (eg. ~/.ssh/config) with this configuration:

  ```
  Host server_name
      User root
      Port 2222
      Hostname <host_kvm>

 Host turtlebot2-<id>
      User root
      Port 2222
      ProxyCommand ssh server_name -W m3-<id>.<site>.iot-lab.info:%p
      StrictHostKeyChecking no
  ```
 
  It allows you to connect directly to the robot netbook from your computer with this command:

  ```
  your_computer$ ssh turtlebot2-<id>
  ```
  </em>
        

7. Configure netbook system and install ROS

  ```
  your_computer$ fab <dev|prod> turtlebot2.setup_ros -H root@<host_kvm>:2222
  ```

after netbook reboot, fix ROS sources:

  ```
  your_computer$ fab <dev|prod> turtlebot2.setup_rosdep -H root@<host_kvm>:2222
  ``` 


8. Test your ROS installation 

  * View ROS service list

     ```
     your_computer$ ssh turtlebot2-<id>
     netbook$ su turtlebot
     netbook$ rosservice list |grep handle
       /turtlebot2_m3_381/handle_dock
       /turtlebot2_m3_381/handle_goal_planner
       /turtlebot2_m3_381/handle_led1
       /turtlebot2_m3_381/handle_led2
       /turtlebot2_m3_381/handle_node_power
     ```

  * DOCK and UNDOCK your robot

    ```
    netbook$ rosservice call /turtlebot2_m3_<id>/handle_dock "Dock_Value: UNDOCK"
    netbook$ rosservice call /turtlebot2_m3_<id>/handle_dock "Dock_Value: DOCK"
    ```

  * View ROS topic diagnostics_agg

    ```
    netbook$ rostopic echo /turtlebot2_m3_<id>/diagnostics_agg
    ```

9. Download on your computer the Yocto images and kernel

  ```
  your_computer$ fab <dev|prod> utils.images.download_images_and_kernel -H root@<devwww|www>.iot-lab.info:2222
  ```

10. Setup the M3 node installation

  We install on the netbook a server NFS/TFTP and DHCP for booting M3 node.

  ```
  your_computer$ fab <dev|prod> turtlebot2.setup_node -H root@<host_kvm>:2222
  ```

### Turtlebot Upgrade Firmware

See official documentation from Yujin [here](https://docs.google.com/document/d/15k7UBnYY_GPmKzQCjzRGCW-4dIP7zl_R_7tWPLM0zKI/edit)

* Connect on a console or via ssh on the Turtlebot. For the upgrade process, you should be physically next to the Turtlebot
* Download and compile the upgrade firmware software for kobuki

```
   wget http://files.yujinrobot.com/kobuki/firmware/stm32flash-0.4.tar.gz
   tar -xvzf stm32flash.tar.gz
   cd stm32flash
   make
```

* Download the latest recommended firmware (1.2.0)

```
    wget http://files.yujinrobot.com/kobuki/firmware/kobuki_firmware_1.2.0.hex
```

* Execute the flash command IMMEDIATELY after turning on the kobuki in flashing mode! (i.e put the switch on Download position and off/on the power button)

```
    ./stm32flash -b 115200 -w kobuki_firmware_1.2.0.hex /dev/ttyUSB0
```

* Turn off the kobuki
* Set the switch on Operation mode on the kobuki 
* Turn on the kobuki
* Verify the update firmware with:`rostopic echo /mobile_base/version_info`
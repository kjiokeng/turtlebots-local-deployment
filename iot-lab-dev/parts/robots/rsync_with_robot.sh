#! /usr/bin/env python

HOST=${1:-m3-19.devgrenoble.iot-lab.info}
HOST=turtlebot@${HOST}

DEST='~/clochette_robot_tests/'
SRC_DIR='turtlebotv2/iotlab_lib/src'

rsync -e "ssh -F ssh_config" -avz --delete scripts/    ${HOST}:~/tests_scripts
rsync -e "ssh -F ssh_config" -avz --delete turtlebotv2 ${HOST}:${DEST}

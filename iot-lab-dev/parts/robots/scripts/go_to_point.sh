#! /bin/bash
readonly D=$(dirname $0)
source "$D/cfg.sh"

echo $#
if [[ "$#" < 2 ]]
then
    echo "Usage: $0 x y [theta] [name]"
    exit 1
fi

X=${1}
Y=${2}
THETA=${3:-0.0}
NAME=${4:-GoToPoint_CLI}

JSON='{"x": '$X', "y": '$Y', "theta": '$THETA', "name": "'$NAME'"}'

curl -X POST -H "Content-Type: application/json" -d "${JSON}" \
    ${URL}/robot/go_to_point ; echo

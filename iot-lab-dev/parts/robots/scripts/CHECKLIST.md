# Tests proceedings

### Basic experiment predefined circuit

1. Start experiment with a given circuit

'''
  experiment-cli submit -d 20 -n test_robot -l devstras,m3,93,mobility=square 
'''

2. Stop experiment

  ```
  experiment-cli stop
  ```

## Start experiment without mobility profile

1. Start experiment with no profile

  ```
  experiment-cli submit -d 20 -n test_robot -l devstras,m3,93
  ```

2. Load pre-defined circuit

  ```
  ./update.sh circuit_square_devstras.json
  ```

3. Stop experiment

  ```
  experiment-cli stop
  ```

## Start experiment without mobility profile and go_to_point tests

1. Start experiment with no profile

  ```
  experiment-cli submit -d 20 -n test_robot -l devstras,m3,93
  ```

2. Go to points A,B,dock,A,B

  ```
  ./go_to_point.sh 9.61	-0.89 -2.43 Point_A
  ./go_to_point.sh 5.81	3.60 0.66 Point_B
  ./dock.sh
  ./go_to_point.sh 9.61	-0.89 -2.43 Point_A
  ./go_to_point.sh 5.81	3.60 0.66 Point_B
  ```

3. Stop experiment

  ```
  experiment-cli stop
  ```

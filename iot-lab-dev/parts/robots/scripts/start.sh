#! /bin/bash
set -e
readonly D=$(dirname $0)
source "$D/cfg.sh"

if [[ "x" == "x$1" ]]
then
    robot_cfg=$(python $D/exp_start_cfg.py .null.json  1  ${SSH_KEY})
else
    robot_cfg=$(python $D/exp_start_cfg.py $1          0  ${SSH_KEY})
fi

robot_cfg=$(echo $robot_cfg)

curl -X POST -H "Content-Type: application/json" --data "${robot_cfg}" \
    ${URL}/exp/start/${EXP_ID}/${EXP_USER}; echo

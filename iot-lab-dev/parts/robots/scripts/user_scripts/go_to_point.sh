#! /bin/bash

if [[ "$#" < 3 ]]
then
    echo "Usage: $0 node x y [theta] [name]"
    exit 1
fi

HOST=${1}
X=${2}
Y=${3}
THETA=${4:-0.0}
NAME=${5:-GoToPoint_CLI}

JSON='{"x": '$X', "y": '$Y', "theta": '$THETA', "name": "'$NAME'"}'

curl -X POST -H "Content-Type: application/json" -d "${JSON}" \
    http://${HOST}:8081/robot/go_to_point ; echo

#! /bin/bash

if [[ "$#" != 1 ]]
then
    echo "Usage: $0 node"
    exit 1
fi

HOST=${1}
curl -X DELETE http://${HOST}:8081/robot/user_env; echo

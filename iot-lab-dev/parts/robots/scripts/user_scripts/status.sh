#! /bin/bash

if [[ "$#" != 1 ]]
then
    echo "Usage: $0 node"
    exit 1
fi

HOST=${1}
curl -X GET http://${HOST}:8081/robot/status; echo

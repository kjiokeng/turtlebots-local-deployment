#! /bin/bash

if [[ "$#" != 3 ]]
then
    echo "Usage: $0 node x y"
    exit 1
fi

HOST=${1}
X=${2}
Y=${3}

QUERY="?x=${X}&y=${Y}"

curl -X GET http://${HOST}:8081/robot/is_reachable${QUERY} ; echo

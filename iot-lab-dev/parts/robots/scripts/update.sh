#! /bin/bash
readonly D=$(dirname $0)
source "$D/cfg.sh"

if [[ "x" == "x$1" ]]
then
    mobility="$D/controlled.json"
else
    mobility="$1"
fi

curl -X POST -H "Content-Type: application/json" --data @"${mobility}" \
    ${URL}/exp/update; echo

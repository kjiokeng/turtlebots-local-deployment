# Robots ROS software


## Overview 

The repository contains the software installed on the mobile node. The
mobile node is composed by three elements: the robot, a laptop and the
IoT-LAB node. Currently the robot is a
[Turtlebot2](http://www.turtlebot.com/), it is planned also to use
Wifibot at Lille. For more informations on the hardware architecture see the [user wiki
page](https://github.com/iot-lab/iot-lab/wiki/Turtlebot2-mobile-node#mobile-node-architecture-description).

The laptop communicates with the IoT-LAB infrastructure through a
dedicated wifi link and it controls the physical robot using
[ROS](http://www.ros.org/). The IoT-LAB node is carried around by the
robot and is accessible like a fixed node through the REST API.  So,
the IoT-LAB infrastructure can control the mobile node by starting the
experiment with a profile, updating the profile and stopping the
experiment.

For the user, the major difference with a fixed node, is that he needs
to give the trajectory circuit to be followed by the mobile node (see
[user wiki
page](https://github.com/iot-lab/iot-lab/wiki/Turtlebot2-mobile-node#predefined-trajectories-on-each-site)
for the different trajectories). The data monitored (node consumption,
robot trajectory,...) are not accessible on-line on our account; they
are transfered at the end of the experimention.

The behaviour of the mobile node is predefined by the software. The
state machine of this behavior is again presented on [user wiki
page](https://github.com/iot-lab/iot-lab/wiki/Turtlebot2-mobile-node#mobile-node-behaviour)

## Pre-requisited

#### Getting started with ROS

ROS is the software which makes the Turtlebot working. It allows nodes
creation. A node is a program which executes specific code and can
interact with other nodes through topics (publisher/subscriber mode :
one message is sent in a topic and many subscribers can receive it),
services (which allows a node to send a request to a specific node)
or actions (same as service except the contacted node will return
state informations to the first node) and can use ROS functions.

The Turtlebot2 is provided with some nodes which are controlling the
hardware. To program the robot's behaviour, we will need to create new
nodes which will interact with the already existing ones.

More informations on [ROS website
introduction](http://www.ros.org/wiki/ROS/Introduction)

#### Getting started with the Turtlebot Navigation

The robots ROS software is based on the [turtlebot_navigation
Tutorial](http://wiki.ros.org/turtlebot_navigation/Tutorials). It
shows you how to deploy and configure the Autonomous Navigation for a
TurtleBot. 

We can notice that a new tutorial, a "Getting Started" Guide for
Developers Interested in Robotics can be found here:
[http://learn.turtlebot.com/](http://learn.turtlebot.com)

## Installation

See the [dev wiki](https://github.com/iot-lab/iot-lab-dev/wiki/Turtlebot-Installation)

## Code structure

The current version is located in ```turtlebotv2/hydro/``` based on [ROS
hydro](http://wiki.ros.org/hydro). In this directory, you will find
several modules.

####  ```kobuki_auto_docking/```

ROS kobuki auto_docking module rewriting in order to be more
robust. For details see
[README.md](https://github.com/iot-lab/robots/blob/master/turtlebotv2/hydro/kobuki_auto_docking/README.md)

####  ```iotlab_turtlebot/```
 Contains 

 * ```goals/``` location for each site map of docks (docks_pose) and
   point near docks (docking_area)
 * ```launch/iotlab_turtlebot.launch``` ROS launching
 * ```scripts/``` used by ROS launching

#####  ```iotlab_maps/```  
 Contains site maps (configuration yaml and images) used by Turtlebot Navigation.

#####  ```iotlab_msgs/```
 Definition of ROS messages and services for our application.
 TODO : list the services

####  ```iotlab_lib/```   
 Main of our software wrapping the Turtlebot Navigation application by :
 * managing the robot and its behavior
 * managing the rest server communicating with the IoT-LAB infrastructure


####  ```autotests/```
 Turtlebot2 old test scripts 


## Documents

 * [Turtlebot2 user manual](https://github.com/iot-lab/iot-lab-dev/wiki/Docs/TurtleBot-2-UserManual.pdf)

 * [Kobuki_quick guide](https://github.com/iot-lab/iot-lab-dev/wiki/Docs/kobuki_quickguide_A3.pdf)

 * [Official ROS functions sheet](https://github.com/iot-lab/iot-lab-dev/wiki/Docs/ROScheatsheet.pdf)

 * [IoT-LAB ROS functions sheet](https://github.com/iot-lab/iot-lab-dev/wiki/Docs/Ros-Groovy-personal-cheated-sheet.pdf)

 * [Lucile Cossou, Master Thesis](http://sed.inrialpes.fr/stages/stages2013/Rapport_Lucile.pdf)

 * [Internal SED Wiki of Lucile](https://sed-wiki.inrialpes.fr/SED/Suivi/SuiviLucileCossou)

## Todo
 * bug correction : robot behavior robustness 
 * bug correction : 'weak' docking in Strasbourg
 * update ```autotests/```

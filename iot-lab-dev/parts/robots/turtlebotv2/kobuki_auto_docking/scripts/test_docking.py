#!/usr/bin/env python

# This file is a part of IoT-LAB robots
# Copyright (C) 2015 Univ. Strasbourg, INRIA (Contact: admin@iot-lab.info)
# Contributor(s) : see AUTHORS file
#
# This software is governed by the CeCILL license under French law
# and abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# http://www.cecill.info.
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

#Import required ROS libraries as defined in your package definition 
import roslib
#roslib.load_manifest('testdocking')
#Import required python libraries
import rospy
import types

#Import required Kobuki's libraries
from kobuki_msgs.msg import SensorState
import commands
from std_msgs.msg import String, Empty
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from math import sqrt
from math import pi
from math import atan
from random import uniform
import sys

#global variables
state = SensorState()
odom_values = Odometry()

#this function return the euclidian distance between (x1,y1) et (x2,y2)
def dist(x1,y1,x2,y2):
	d = sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2))
	return d

#this function returns odometry values
def callback(data):
     odom_values.pose.pose.position.x = data.pose.pose.position.x
     odom_values.pose.pose.position.y = data.pose.pose.position.y

#this function returns charger and battery values
def sensorsCallback(data) :
  state.charger = data.charger
  state.battery = data.battery

#this function makes the robot try to dock and to undock  
def test_docking() :
  rospy.loginfo("Docking test")
  #First, we announce that we will publish data -orders to dock mainly- into 'dock_drive/debug/mode_shift'
  docking = rospy.Publisher('/dock_drive/debug/mode_shift', String)
  rospy.loginfo("Publisher created ")
  #Then we subscribe to '/mobile_base/sensors/core' which will provide us information about battery state
  rospy.Subscriber('/mobile_base/sensors/core', SensorState, sensorsCallback)
  #signals that the node will publish in velocity topic
  pub = rospy.Publisher('mobile_base/commands/velocity', Twist)
  #We subscribe to '/odom' topic. This topic will return data that we will call data for convenience
  rospy.Subscriber('/odom', Odometry, callback)
  #We wait for 1 s for synchronization
  rospy.sleep(2)     
  #measures variables
  base_pose_x = 0
  base_pose_y = 0
  d=0
  theta = 0
  
  #While the charger is discharging -which means the robot isn't on the base- we will do the following things
  if rospy.is_shutdown(): 
	docking.publish('disable')
  while (not rospy.is_shutdown()) :
    if not (d ==0) : 
      rospy.loginfo("The turtlebot is placed at %f m with theta = %f degrees", d, theta*180/pi)
    if state.charger&6 == 0 : 
      sys.stdout.write('Trying to dock') 
      sys.stdout.write('\r')
      sys.stdout.flush()
    i_clock = rospy.get_time()
    time = rospy.get_time()-i_clock
    aborts = 0
    rospy.sleep(1)
    docking.publish('enable')
    rospy.sleep(0.5)
    rospy.loginfo('enable signal sent')
    if rospy.is_shutdown(): 
      docking.publish('disable')
    while (state.charger&6 == 0) & (not rospy.is_shutdown()) & (not aborts)  :
      src_str = ''
      # we check that the robot isn't plug and if it is, we will print on which system
      if state.charger :
	src_str = '(adaptor)' if state.charger&16 else '(dock)'
      # we print information about robot's battery state
      chg = state.charger&6
      if chg==0: chg_str = 'discharging'
      elif chg==2 : chg_str = 'fully charged'
      else : chg_str = 'charging'
      sys.stdout.write('[Robot : ' + "{0:2.1f}".format(state.battery/10.) + 'V -' + chg_str + src_str + ', elapsed time : ' + str(time)+ 's'']')
      sys.stdout.write('\r')
      sys.stdout.flush()
      #rospy.sleep(0.2)
      time = rospy.get_time()-i_clock
      if (time>360) : 
	aborts = 1
	docking.publish('disable')
	rospy.sleep(0.2)
	
    #Notice that docking has been done  
    if not (state.charger&6 == 0) : 
      sys.stdout.write('Docking done')  
      sys.stdout.write('\r')
      sys.stdout.flush()
      base_pose_x = odom_values.pose.pose.position.x
      base_pose_y = odom_values.pose.pose.position.y
      
    cmd = Twist()
    cmd.linear.x=-0.1 #HERE YOU CAN CHOOSE A SPEED
    rospy.loginfo("Moving away from the dock station")
    if state.charger : 
      charging = 1#if the robot is charging
      rospy.sleep(3)
    else : charging = 0
    d= 0
    if rospy.is_shutdown(): 
      docking.publish('disable')
    while (d < 0.1) & (not rospy.is_shutdown()) :
      x_final = odom_values.pose.pose.position.x
      y_final = odom_values.pose.pose.position.y
      pub.publish(cmd)
      d = dist(base_pose_x,base_pose_y,x_final , y_final)
      rospy.sleep(0.2)
      sys.stdout.write('[Robot] distance parcourrue : ' + str(d) )
      sys.stdout.write('\r')
      sys.stdout.flush()
    if charging : #if the robot was charging
      #make it turn with a random angle
      cmd.linear.x = 0
      theta = uniform(0, 2*pi/2) - pi/2
      rospy.loginfo("angle theta : %f"%theta)
      cmd.angular.z = theta
      for i in range(10) :
	pub.publish(cmd)
	rospy.sleep(0.1)
      #make it move for two more meters 
      rospy.sleep(1)
      cmd.angular.z = pi
      for i in range(10) :
	pub.publish(cmd)
	rospy.sleep(0.1)
      #make it move for two more meters 
      rospy.sleep(1)
      cmd.linear.x=0.1 #HERE YOU CAN CHOOSE A SPEED
      cmd.angular.z = 0
      d = 0
      if rospy.is_shutdown(): 
        docking.publish('disable')
      while (d < 0.9) & (not rospy.is_shutdown()) :
	x_final = odom_values.pose.pose.position.x
	y_final = odom_values.pose.pose.position.y
	pub.publish(cmd)
	d = dist(base_pose_x,base_pose_y,x_final , y_final)
	rospy.sleep(0.2)
	sys.stdout.write('[Robot] distance parcourrue : %f ' + str(d) )
	sys.stdout.write('\r')
	sys.stdout.flush()
      theta = atan((y_final-base_pose_y)/(x_final-base_pose_x))
      if aborts == 1 : rospy.loginfo("Turtlebot docking failed.")
      if aborts == 0 : rospy.loginfo("Turtlebot docking succeeded in %f ", time)


      
      
    
    

if __name__ == '__main__' :
  try :
    rospy.init_node("test_docking")
    test_docking()
    rospy.spin()
  except rospy.ROSInterruptException : pass

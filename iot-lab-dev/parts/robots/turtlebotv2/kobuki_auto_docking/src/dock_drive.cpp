/*
 * Copyright (c) 2012, Yujin Robot.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Yujin Robot nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
/**
 * @file /kobuki_driver/src/driver/dock_drive.cpp
 *
 **/

/*****************************************************************************
 ** Includes
 *****************************************************************************/
#include <unistd.h>
#include "kobuki_auto_docking/dock_drive.hpp"

/*****************************************************************************
 ** Defines
 *****************************************************************************/

#define stringfy(x) #x
#define setState(x) {n_state=x; state_str=stringfy(x);}
#define updateState(){ c_state = n_state; }
#define setVel(v,w) {vx=v; wz=w;}
#define setStateVel(x,v,w) {setState(x);setVel(v,w);}

/*****************************************************************************
 ** Namespaces
 *****************************************************************************/

namespace kobuki
{

/*****************************************************************************
 ** Implementation
 *****************************************************************************/
DockDrive::DockDrive() :
    						is_enabled(false), can_run(false)
, n_state(IDLE), state_str("IDLE")
, c_state(IDLE)
, vx(0.0), wz(0.0)
, bump_remainder(0)
, lost(0)
, dock_stabilizer(0)
, dock_detector(0)
, rotated(0.0)
, sig_filt1(0)
{
	//pose.setIdentity();
}

DockDrive::~DockDrive()
{
	;
}

/****************************************************************
 ********************* Mode Shift ********************************
 *****************************************************************
/**
 * \brief Updates the field is_enabled and can_run according to
 * the content of mode_shift topic
 *
 **/

void DockDrive::modeShift(const std::string& mode)
{
	if (mode == "enable")
	{
		is_enabled = true;
		can_run = true;
	}
	if (mode == "disable")
	{
		is_enabled = false;
		can_run = false;
	}
	if (mode == "run")  can_run = true;
	if (mode == "stop") can_run = false;
}


/**************************************************************
 ************************* Update ******************************
 ***************************************************************/
void DockDrive::update(const std::vector<unsigned char> &signal
		, const unsigned char &bumper
		, const unsigned char &charger
		, const ecl::LegacyPose2D<double> &pose)
{
	static ecl::LegacyPose2D<double> pose_priv;
	ecl::LegacyPose2D<double> pose_update;

	double dx = pose.x() - pose_priv.x();
	double dy = pose.y() - pose_priv.y();
	pose_update.x( std::sqrt( dx*dx + dy*dy ) );
	pose_update.heading( pose.heading() - pose_priv.heading() );
	//std::cout << pose_diff << "=" << pose << "-" << pose_priv << " | " << pose_update << std::endl;
	pose_priv = pose;

	ecl::linear_algebra::Vector3d pose_update_rates; //dummy
	update( signal, bumper, charger, pose_update, pose_update_rates );
}


/**
 * @brief Updates the odometry from firmware stamps and encoders.
 *
 * Really horrible - could do with an overhaul.
 *
 * @param time_stamp
 * @param left_encoder
 * @param right_encoder
 * @param pose_update
 * @param pose_update_rates
 */

void DockDrive::update(const std::vector<unsigned char> &signal
		, const unsigned char &bumper
		, const unsigned char &charger
		, const ecl::LegacyPose2D<double> &pose_update
		, const ecl::linear_algebra::Vector3d &pose_update_rates)
{
	/*************************
	 * pre processing
	 *************************/
	//pose update
	//pose *= pose_update;

	//dock_ir signals filtering
	past_signals.push_back(signal);
	unsigned int window = 20;
	while (past_signals.size() > window)
		past_signals.erase( past_signals.begin(), past_signals.begin() + past_signals.size() - window );

	std::vector<unsigned char> signal_filt(signal.size(), 0);
	for ( unsigned int i=0; i<past_signals.size(); i++)
	{
		if (signal_filt.size() != past_signals[i].size()) continue;
		for (unsigned int j=0; j<signal_filt.size(); j++)
			signal_filt[j] |= past_signals[i][j];
	}


	/*************************
	 * debug prints
	 *************************/
	std::ostringstream debug_stream;
	// pose
	//debug_stream << pose;
	// pose_update and pose_update_rates for debugging
	debug_stream << std::fixed << std::setprecision(4)
	<< "[x: "    << std::setw(7) << pose_update.x()
	<< ", y: "  << std::setw(7) << pose_update.y()
	<< ", th: " << std::setw(7) << pose_update.heading()
	<< "]";

	//dock_ir signal
	/*
    debug_stream
      << "[l: "  << binary(signal_filt[2])
      << ", c: " << binary(signal_filt[1])
      << ", r: " << binary(signal_filt[0])
      << "]";
	 */
	std::string far_signal  = "[F: "; //far field
	std::string near_signal = "[N: "; //near field
	for (unsigned int i=0; i<3; i++)
	{
		if (signal_filt[2-i]&FAR_LEFT   ) far_signal  += "L";
		else far_signal  += "-";
		if (signal_filt[2-i]&FAR_CENTER ) far_signal  += "C";
		else far_signal  += "-";
		if (signal_filt[2-i]&FAR_RIGHT  ) far_signal  += "R";
		else far_signal  += "-";
		if (signal_filt[2-i]&NEAR_LEFT  ) near_signal += "L";
		else near_signal += "-";
		if (signal_filt[2-i]&NEAR_CENTER) near_signal += "C";
		else near_signal += "-";
		if (signal_filt[2-i]&NEAR_RIGHT ) near_signal += "R";
		else near_signal += "-";
		far_signal  += " ";
		near_signal += " ";
	}
	far_signal  += "]";
	near_signal += "]";
	debug_stream << far_signal << near_signal;

	//bumper
	{
		std::string out = "[B: ";
		if (bumper&4) out += "L";
		else out += "-";
		if (bumper&2) out += "C";
		else out += "-";
		if (bumper&1) out += "R";
		else out += "-";
		out += "]";
		debug_stream << out;
	}

	//charger
	{
		std::ostringstream oss;
		oss << "[C:" << std::setw(2) << (unsigned int)charger;
		oss << "(";
		if (charger) oss << "ON";
		else oss << "  ";
		oss << ")]";
		debug_stream << oss.str();
	}





	/*************************
	 ********** FSM **********
	 ************************
	 *\brief Pre-processing algorithm : updates the Kobuki speed
	 */
	//std::string debug_str = "";
	debug_str = "";
	do
	{
		/*********Interruptions************/
		if(is_enabled == false)
		{
			disable();
		}
		if( !(c_state == DOCKED_IN) && charger )
		{
			dock_stabilizer = 0;
			setState(DOCKED_IN);
			bump_remainder = 0;
		}
		if (bumper && !(c_state == DOCKED_IN || c_state == ALIGNED_NEAR || c_state == BUMPED_ALIGNED) )
		{
			setState(BUMPED);
			sleep(2);
			bump_remainder =20;
		}

		else if (bumper && (c_state == ALIGNED_NEAR))
		{
			setStateVel(BUMPED_ALIGNED, -0.05, 0.0);
			sleep(2);
			bump_remainder = 20;
		}

		/**********State Update *************/
		updateState();

		switch(c_state)
		{
		case DOCKED_IN : //If the Turtlebot is on the dock station
		{
			dock_stabilizer ++;
			if (dock_stabilizer > 20) //check that it is stabilized
			{
				can_run = false; //if it is, then stop running...
				setStateVel(DONE, 0.0, 0.0); //...we've reached our goal : set the job done
			}
			else
			{
				setStateVel(DOCKED_IN, 0.0, 0.0); //if it is not stabilised yet, wait in this state
				std::ostringstream oss;
				oss << "waiting for stabilisation: " << std::fixed << std::setprecision(2) << std::setw(4) << dock_stabilizer;
				debug_str = oss.str();
			}
			break;
		}

		case DONE : //Final State
		{
			setState(IDLE); //go to the FSM's initial state
			is_enabled = false; //disable the search for docking station
			break;
		}


		case BUMPED : //if the Turtlebot bumped on an obstacle
		{
			bump_remainder -- ;
			if(bump_remainder > 0) // The Turtlebot will move backward
			{
				setStateVel(BUMPED, -0.1, 0.0); //set speed at -0.1m/s
				usleep(100000); //waits for 0.1s (so the Turtlebot will move of 0.01m)
				std::ostringstream oss;
				oss << "bump_remainder: " << std::fixed << std::setprecision(2) << std::setw(4) << bump_remainder;
				debug_str = oss.str();
			} //do this thing bump_remainder = 10 times : the Turtlebot will move of 0.1m
			else //when the Turtlebot has moved back enough, go to LOST state for more security.
			{
				setStateVel(LOST, 0.0, 0.0);
			}
			break;
		}

		case BUMPED_ALIGNED : //if the Turtlebot bumped on an obstacle while aligned near, we will try to stay aligned while moving backward... So we will move exactly the same way as in ALIGNED/ALIGNED_NEAR case, except that this time, the Turtlebot will be moving away from the station and not to the station.
		{
			bump_remainder -- ;
			if(signal_filt[1])
			{
				if(bump_remainder > 0)
				{
					if ( ((signal_filt[1]&NEAR) == NEAR_CENTER) || ((signal_filt[1]&NEAR)== NEAR) )
					{
						setStateVel(BUMPED_ALIGNED, -0.1, 0.0);
						usleep(100000);
						std::ostringstream oss;
						oss << "bump_aligned_center: " << std::fixed << std::setprecision(2) << std::setw(4) << bump_remainder;
						debug_str = oss.str();
					}
					if ( signal_filt[1]&NEAR_LEFT)
					{
						setStateVel(BUMPED_ALIGNED, -0.025, -0.2);
						usleep(100000);
						std::ostringstream oss;
						oss << "bump_aligned_left " << std::fixed << std::setprecision(2) << std::setw(4) << bump_remainder;
						debug_str = oss.str();
					}
					if ( signal_filt[1]&NEAR_RIGHT)
					{
						setStateVel(BUMPED_ALIGNED, -0.025, 0.2);
						usleep(100000);
						std::ostringstream oss;
						oss << "bump_aligned_right: " << std::fixed << std::setprecision(2) << std::setw(4) << bump_remainder;
						debug_str = oss.str();;
					}
					else
					{
						setState(BUMPED);
						usleep(100000);
						debug_str="Lost alignement";
					}
				}
				else
				{
					debug_str = "Lost ! Will wait a while before to try to backup";
					setStateVel(LOST, 0.0, 0.0);
				}
			}
			else
			{
				setStateVel(LOST, 0.0, 0.0);
			}
			break;
		}

		case IDLE : //init state, just allows the turtlebot to move to scan state
		{
			dock_detector = 0;
			rotated = 0.0;
			setStateVel(SCAN, 0.0, 0.66);
			break;
		}

		case SCAN : //tries to get informations about Turtlebot initial position
		{
			rotated += pose_update.heading()/(2*M_PI); //save the number of turns the turtlebot did into rotated variable.
			std::ostringstream oss;
			oss << "rotated: " << std::fixed << std::setprecision(2) << std::setw(4) << rotated;
			debug_str = oss.str();
			if( std::abs(rotated) > 1.0 ) //if this number of turn is bigger than one...
			{
				setStateVel(FIND_STREAM, 0.0, 0.0); //...stop to rotate and go to FIND_STREAM state
				rotated = 0;
			}
			else if ( (signal_filt[1]&FAR_CENTER) || (signal_filt[1]&NEAR_CENTER) ) //else if the Turtlebot front's sensor is in the dock station center stream, there is no need to look for a stream
			{
				setStateVel(ALIGNED, 0.05, 0.00); //...so go to ALIGNED state
				rotated = 0;
			}
			else if (!signal_filt[1])
			{
				setStateVel(SCAN, 0.00, 0.66); //else if the front sensor isn't in any field, turn faster
			}
			else //if this front sensor receives LEFT or RIGHT signal, move slower and update dock_detector variable.
			{
				if (  signal_filt[1]&(FAR_LEFT  + NEAR_LEFT ))
				{
					dock_detector--;
					setStateVel(SCAN, 0.00, 0.10);
				}
				if (  signal_filt[1]&(FAR_RIGHT + NEAR_RIGHT))
				{
					dock_detector++;
					setStateVel(SCAN, 0.00, 0.10);
				}
			}

			break;
		}

		case ALIGNED :
		case ALIGNED_FAR :
		case ALIGNED_NEAR : //if the Turtlebot is ALIGNED
		{
			if ( signal_filt[1] )
			{
				if ( signal_filt[1]&NEAR )
				{
					if ( ((signal_filt[1]&NEAR) == NEAR_CENTER) || ((signal_filt[1]&NEAR) == NEAR) )
					{
						setStateVel(ALIGNED_NEAR, 0.05,  0.0); //if the Turtlebot is in CENTER stream, just move straight forward
						debug_str = "AlignedNearCenter";
					}
					else if (   signal_filt[1]&NEAR_LEFT  )
					{
						setStateVel(ALIGNED_NEAR, 0.025,  0.2); //else if it is a bit on the Left, move a bit to the right
						debug_str = "AlignedNearLeft"  ;
					}
					else if (   signal_filt[1]&NEAR_RIGHT )
					{
						setStateVel(ALIGNED_NEAR, 0.025, -0.2); //else if it is a bit on the right, move a bit to the left
						debug_str = "AlignedNearRight" ;
					}
				}
				else if ( signal_filt[1]&FAR ) //same thing, except that when the Turtlebot is far away the dock station, it is allowed to move faster
				{
					if ( ((signal_filt[1]&FAR) == FAR_CENTER) || ((signal_filt[1]&FAR) == FAR) )
					{
						setStateVel(ALIGNED_FAR, 0.1,  0.0);
						debug_str = "AlignedFarCenter";
					}
					else if (   signal_filt[1]&FAR_LEFT  )
					{
						setStateVel(ALIGNED_FAR, 0.1,  0.3);
						debug_str = "AlignedFarLeft"  ;
					}
					else if (   signal_filt[1]&FAR_RIGHT )
					{
						setStateVel(ALIGNED_FAR, 0.1, -0.3);
						debug_str = "AlignedFarRight" ;
					}
				}
			}
			else //if the Turtlebot doesn't receive any signal on its front sensor, then something unexpected happened and the Turtlebot is set to lost state.
			{
				debug_str = "lost signals";
				setStateVel(LOST, 0.00, 0.00);
			}
			break;
		}

		case FIND_STREAM :
		{
			rotated += pose_update.heading()/(2*M_PI);
			if (rotated > 2.0 )
			{
				setState(LOST);
			}
			else if (dock_detector > 0 )   // == if robot is placed in right side of docking station
			{
				//turn  right , negative direction till get right signal from left sensor
				if ( signal_filt[2]&(FAR_RIGHT+NEAR_RIGHT) )
				{
					setStateVel(GET_STREAM, 0.05, 0.0);
				}
				else if (signal_filt[1]&(FAR_CENTER + NEAR_CENTER) )
				{
					setState(ALIGNED);
				}
				else if (signal[2]&(FAR_LEFT + NEAR_LEFT))
				{
					setState(LOST);
				}
				else
				{
					setStateVel(FIND_STREAM, 0.0, -0.33);
				}
			}
			else if (dock_detector < 0 )     // robot is placed in left side of docking station
			{
				//turn left, positive direction till get left signal from right sensor
				if ( signal_filt[0]&(FAR_LEFT+NEAR_LEFT) )
				{
					setStateVel(GET_STREAM, 0.05, 0.0);
				}
				else if (signal_filt[1]&(FAR_CENTER + NEAR_CENTER) )
				{
					setState(ALIGNED);
				}
				else if (signal[2]&(FAR_RIGHT + NEAR_RIGHT))
				{
					setState(LOST);
				}
				else
				{
					setStateVel(FIND_STREAM, 0.0, 0.33);
				}
			}
			else
			{
				setState(LOST);
				debug_str="dock_detector = 0 ! No IR signal was found !";
			}
			break;

		}

		case GET_STREAM :
		{
			if (dock_detector > 0)   //robot is placed at the right side of the docking station
			{
				if (signal_filt[2]&(FAR_LEFT+NEAR_LEFT))
				{
					dock_detector = 0;
					rotated = 0.0;
					setStateVel(SCAN, 0.0, 0.10);
				}
				else if (!signal_filt[2])
				{
					setStateVel(LOST_STREAM,0.0, 0.0);
				}
				else
				{
					setStateVel(GET_STREAM, 0.05, 0.0);
				}
			}
			else if (dock_detector < 0)     // robot is placed in left side of docking station
			{
				if (signal_filt[0]&(FAR_RIGHT+NEAR_RIGHT))
				{
					dock_detector = 0;
					rotated = 0.0;
					setStateVel(SCAN_LEFT, 0.0, 0.10);
				}
				else if (!signal_filt[0])
				{
					setStateVel(LOST_STREAM,0.0, 0.0);
				}
				else
				{
					setStateVel(GET_STREAM, 0.05, 0.0);
				}
			}
			break;
		}

		case SCAN_LEFT : // exactly the same thing as SCAN, except that this does improve docking speed when this state is called after a GET_STREAM state by allowing the Turtlebot to turn in the right direction instead of having him to do a whole turn
		{
			rotated += pose_update.heading()/(2*M_PI);
			std::ostringstream oss;
			oss << "rotated: " << std::fixed << std::setprecision(2) << std::setw(4) << rotated;
			debug_str = oss.str();
			if( std::abs(rotated) < -1.0 )
			{
				setStateVel(FIND_STREAM, 0.0, 0.0);
			}
			else if ( (signal_filt[1]&FAR_CENTER) || (signal_filt[1]&NEAR_CENTER) )
			{
				setStateVel(ALIGNED, 0.05, 0.00);
			}
			else if (!signal_filt[1])
			{
				setStateVel(SCAN_LEFT, 0.00, -0.66);
			}
			else
			{
				if (  signal_filt[1]&(FAR_LEFT  + NEAR_LEFT ))
				{
					dock_detector--;
					setStateVel(SCAN_LEFT, 0.00, -0.10);
				}
				if (  signal_filt[1]&(FAR_RIGHT + NEAR_RIGHT))
				{
					dock_detector++;
					setStateVel(SCAN_LEFT, 0.00, -0.10);
				}
			}

			break;
		}

		case LOST_STREAM : //If the stream is lost during GET_STREAM. WARNING:THIS IS NOT AN ERROR, NEITHER AN UNEXPECTED CASE. This state is expected to happen in some special cases.
		{
			rotated += pose_update.heading()/(2*M_PI);
			if (rotated > 2.0 )
			{
				setState(LOST);
			}
			else if(dock_detector>0) //if the robot is in Left-Zone
			{
				if ( ! signal_filt[1]) // make the Turtlebot turn on itself until front sensor is activated again
				{
					setStateVel(LOST_STREAM, 0.0, 0.33);
				}
				else
				{
					sig_filt1 = 1; //note that this sensor went to be activated
				}
				if (sig_filt1 == 1) //since it happens,
				{
					if (! signal_filt[2])
					{
						setStateVel(LOST_STREAM, 0.0, - 0.33); // make the turtlebot turn on the other direction
					}
					else if (signal_filt[2]) //until RIGHT sensor is activated
					{
						setStateVel(GET_STREAM, 0.05, 0.0); // then go back to GET_STREAM
					}
				}
			}

			else if(dock_detector<0) // if the Turtlebot is in the Right-Zone
			{
				if(!signal_filt[1]) //same thing as in Left-Zone, in the opposite direction
				{
					setStateVel(LOST_STREAM, 0.0, -0.33);
				}
				else
				{
					sig_filt1 = 1;
				}
				if (sig_filt1 == 1)
				{
					if ( ! signal_filt[0] )
					{
						setStateVel(LOST_STREAM, 0.0, 0.33);
					}
					else if (signal_filt[0])
					{
						setStateVel(GET_STREAM, 0.05, 0.0);
					}
				}
			}
			break;
		}

		case LOST : //if the Turtlebot is lost... This is an error. It only happens for unexpected reasons (BUMPED, No signal received in any direction...)
		{
			sleep(10); //wait, in case the error would be fixed by the time
			if (lost < 5) //then if the Turtlebot hasn't been lost too many times, it will attempt again to dock in
			{
				lost ++;
				std::ostringstream oss;
				oss << "Signal has been lost. Will try again. number of attempts left : " << std::fixed << std::setprecision(2) << std::setw(4) << lost;
				debug_str = oss.str();
				setStateVel(IDLE, 0.0, 0.0);
			}
			else if (dock_detector == 0) //otherwise, if no signal is received it will move to AWAY state
			{
				debug_str = "The Turtlebot lost the station signal too many times.";
				setStateVel(AWAY, 0.0, 0.0);
			}
			else
			{
				debug_str = "The Turtlebot has encountered too many unexpected obstacles.";
				setStateVel(STOP, 0.0, 0.0);
			}
			break;

		}

		case AWAY : //if the Turtlebot doesn't receive any signal from the dock station
		{
			debug_str = "The Turtlebot might not be into the dock-station IR field or the station has been disabled. Autodocking has been aborted.";
			is_enabled = false;
			setStateVel(AWAY, 0.0, 0.0);
			break;
		}
		case STOP :
		{
			dock_stabilizer ++;
			if (dock_stabilizer > 20) //check that it is stabilized
			{
				can_run = false; //if it is, then stop running...
				setStateVel(IDLE, 0.0, 0.0); //... action finished: set the job done
				is_enabled = true;
			}
			else
			{
				setState(STOP);
				debug_str = "The Turtlebot is going to be stopped.";
				is_enabled = false;
			}
			break;
		}

		default :
		{
			dock_detector = 0;
			rotated = 0.0;
			setStateVel(UNKNOWN, 0.0, 0.0);
			break;
		}
		}
	}
	while(0);
	//debug_stream << std::fixed << std::setprecision(4)
	debug_stream << "[vx: " << std::setw(7) << vx << ", wz: " << std::setw(7) << wz << "]";
	debug_stream << "[S: " << state_str << "]";
	debug_stream << "[" << debug_str << "]";
	//debug_stream << std::endl;
	debug_output = debug_stream.str();

	//std::cout << debug_output << std::endl;;

	velocityCommands(vx, wz);
	//  this->vx = vx; this->wz = wz;
	return;
}

void DockDrive::velocityCommands(const double &vx_, const double &wz_)
{
	// vx: in m/s
	// wz: in rad/s
	vx = vx_;
	wz = wz_;
}

std::string DockDrive::binary(unsigned char number) const
{
	std::string ret;
	for( unsigned int i=0; i<6; i++)
	{
		if (number&1) ret = "1" + ret;
		else          ret = "0" + ret;
		number = number >> 1;
	}
	return ret;
}


} // namespace kobuki


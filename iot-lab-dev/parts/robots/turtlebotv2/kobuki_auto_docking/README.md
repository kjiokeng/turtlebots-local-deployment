# kobuki_auto_docking ROS node

## Overview 

We propose a modification of the
[kobuki_node_docking](http://wiki.ros.org/kobuki_auto_docking?distro=indigo)
in indigo ROS version. This node manages the automatic docking of the
kobuki to its dock using infrared sensors. After some tests, we
observe that the current implementation does not provide a robust
behavior: the robot fails to docking in various cases. We need more
robustness for our application.

## Modifications

We modify the algorithms in order to take in account errors like :
 * sensor dead zone 
 * case if the robot is back to the base station (without viewing the IR signal)
 * unexpected case as: IR signal lost, bumper-on against obstacle,...
 
We add a state LOST which allows to prevent these errors. Default
robot velocity also has been reduced improving approach of the station. 

The choice of the modifications was to improve the docking of the
robot in a small space. This choice was not allowed to have very fluid
robot motion.

## TODO

Improving the algorithm;

  * Detection when the robot is blocked on the base station (wheel drop or cliff detected)
  * Detection of the orientation of a bumper-on: If bumper right, turn -pi/2 then move back; If bumper right, turn pi/2 then move back; 
  * When bumpers-on, far from the base station, make an half turn
  * Time delay to detect multiple errors
  * Add a LED on or sound to point unexpected errors

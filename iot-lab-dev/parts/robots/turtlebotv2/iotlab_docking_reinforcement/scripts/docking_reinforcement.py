#!/usr/bin/env python

# This file is a part of IoT-LAB robots
# Copyright (C) 2015 Univ. Strasbourg, INRIA (Contact: admin@iot-lab.info)
# Contributor(s) : see AUTHORS file
#
# This software is governed by the CeCILL license under French law
# and abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# http://www.cecill.info.
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""
    Watchdog for a better kobuki auto-docking.

    Check in background if the auto-docking Rapp is launched.
    If so, check if the motor load is not to heavy more than 30s.
    It means the robot is stucked on the dock, so we stop the 
    auto-docking Rapp, move the robot back for 1 meter, and relaunch
    the auto-docking Rapp until robot is successfully docked.    

    Created for the IoT-LAB Project: http://www.iot-lab.fr
"""

# disabling pylint errors 'F0401' Unable to import,
# pylint:disable=I0011, F0401

import logging

import roslib; roslib.load_manifest('iotlab_lib')
import rospy
import rocon_app_manager_msgs.srv
from rocon_app_manager_msgs.msg import RappList
from rocon_app_manager_msgs.msg import Rapp
from geometry_msgs.msg import Twist
from kobuki_msgs.msg import MotorPower
from diagnostic_msgs.msg import DiagnosticArray

class DockingReinforcement():
    """
        Class DockingReinforcement for auto-docking watchdog
    """

    # ROS params
    turtlebot_name = rospy.get_param('/iotlab/turtlebot_name')
    # ROS topics path
    sub_topic_rapp_list_path = '/' + turtlebot_name + '/rapp_list'
    sub_topic_diagnostics_path = '/' + turtlebot_name + '/diagnostics_agg'
    pub_topic_cmd_vel_path = "/cmd_vel_mux/input/safety_controller"
    pub_topic_motor_power_path = "/mobile_base/commands/motor_power"
    # ROS services paths
    service_rocon_start_path = '/' + turtlebot_name + '/start_rapp'
    service_rocon_stop_path = '/' + turtlebot_name + '/stop_rapp'
    # Variables
    twist_cmd = Twist()
    # Static variables
    CMD_VEL_TIME_FACTOR = 0.1
    MOTOR_STATUS_OK = 0
    MOTOR_STATUS_BAD = 1
    # Configuration variables 
    MAX_DOCKING_ATTEMPTS = 10
    TIMEOUT_DOCKING = 120
    TIMEOUT_MOTOR = 20

    def __init__(self):
        """
            DockingReinforcement Constructor
        """
        # Create ROS Node
        rospy.init_node('docking_reinforcement', anonymous=False, log_level=rospy.DEBUG)
	rospy.loginfo("DockingReinforcement.init")
        # Subscribers
        self.sub_topic_rapp_list = rospy.Subscriber(self.sub_topic_rapp_list_path, RappList, self.rapp_list_callback)
        # Publishers
        self.pub_topic_motor_power = rospy.Publisher(
                            self.pub_topic_motor_power_path, MotorPower, queue_size=10)
        self.pub_topic_cmd_vel = rospy.Publisher(self.pub_topic_cmd_vel_path, Twist, queue_size=10)
        # Variables
        self.sub_topic_diagnostics = None
        self.docking_attempts = 0
        self.diagnostics_values = DiagnosticArray()
        self.timer_docking = None
        self.timer_motor = None
        self.last_motor_current_values = None
        # Shutdown callback 
        rospy.on_shutdown(self.shutdown)
        rospy.spin()

    def rapp_list_callback(self, data):
        """
            Callback function which look at auto-docking rapp status.
            If running, we launch our watchdog.
        """
        rospy.loginfo("DockingReinforcement.callback_rapp_list: event")
	for rapp in data.available_rapps:
            rospy.logdebug("DockingReinforcement.callback_rapp_list: available_rapps rapp.name:%s", rapp.name)
	    if rapp.name == "iotlab_turtlebot/auto_docking" and rapp.status == "Running":
	        rospy.loginfo("DockingReinforcement.callback_rapp_list: auto_docking is %s ", rapp.status)
		self.start_docking_watchdog()
                rospy.loginfo("DockingReinforcement.rapp_list_callback fails: %s", self.docking_attempts)
	    if rapp.name == "iotlab_turtlebot/auto_docking" and rapp.status == "Ready":
	        rospy.loginfo("DockingReinforcement.callback_rapp_list: auto_docking is %s ", rapp.status)
		self.stop_docking_watchdog()
                rospy.loginfo("DockingReinforcement.rapp_list_callback fails: %s", self.docking_attempts)
                # if robot is on dock, it means the docking was successfull
                if self.is_dock():
                    self.docking_attempts = 0
                    rospy.loginfo("DockingReinforcement.rapp_list_callback robot on Docked, reinit fails : %s", self.docking_attempts)

    def start_docking_watchdog(self):
        """
            Function that starts docking watchdog
        """
	rospy.loginfo("DockingReinforcement.start_docking_watchdog")
        # Start diagnostics monitoring
        self.sub_topic_diagnostics = rospy.Subscriber(
            self.sub_topic_diagnostics_path, DiagnosticArray,
            self.diagnostics_callback)
        # Start docking timer
        self.timer_docking = rospy.Timer(rospy.Duration(self.TIMEOUT_DOCKING), self.timer_docking_callback, oneshot=False) 

    def stop_docking_watchdog(self):
        """
            Function that stops docking watchdog
        """
	rospy.loginfo("DockingReinforcement.stop_docking_watchdog")
        # Stop diagnostics monitoring
        if self.sub_topic_diagnostics:
            self.sub_topic_diagnostics.unregister()
        # Stop docking timer
        if self.timer_docking:
            self.timer_docking.shutdown()

    def timer_docking_callback(self, event):
        """
            Callback function when docking timer is expired
            see self.TIMEOUT_DOCKING (default 120s)
        """
        rospy.logerr("DockingReinforcement.timer_docking_callback timeout %s sec expired", self.TIMEOUT_DOCKING)
        if event.last_real!=None:
            rospy.logdebug("DockingReinforcement.timer_docking_callback: %s %s ", event.last_real.to_sec(), event.current_real.to_sec())
        self.timer_task()

    def timer_motor_callback(self, event):
        """
            Callback function when motor timer is expired
            see self.TIMEOUT_MOTOR (default 20s)
        """
        rospy.loginfo("DockingReinforcement.timer_motor_callback timeout %s sec expired", self.TIMEOUT_MOTOR)
        if event.last_real!=None:
            rospy.logdebug("DockingReinforcement.timer_motor_callback: %s %s ", event.last_real.to_sec(), event.current_real.to_sec())
        self.timer_task()

    def timer_task(self):
        """
            Function used by all timers callback.
            When any timer expires, we stop auto-docking Rapp and
            all timers. If max fails is not reached, we relaunch
            the docking approach task.
        """
        # Stop timer_docking callback
        self.timer_docking.shutdown()
        # Stop timer_motor callback
        if self.timer_motor!=None and self.timer_motor.is_alive():
            self.timer_motor.shutdown()
        # Stop auto-docking rapp
        self.stop_docking_app()
        # Increment fails
        self.docking_attempts+=1
        rospy.loginfo("DockingReinforcement.timer_task fails: %s", self.docking_attempts)  
        # Check common timers max attempts fail
        if self.docking_attempts < self.MAX_DOCKING_ATTEMPTS:
            rospy.loginfo("DockingReinforcement.timer_task restart docking approach")  
            # Go back and restart auto-docking rapp
            self.go_back()
            self.start_docking_app()
            # Rearm timer_docking_callback
            self.timer_docking.run()
        else:
            rospy.loginfo("DockingReinforcement.timer_task too many fails: %s", self.docking_attempts)
	    # Reinit fails
            self.docking_attempts=0
	    rospy.loginfo("DockingReinforcement.timer_task reinit fails : %s", self.docking_attempts)
            # Robot docking failure, Houston we have a problem
            self.rescue()

    def diagnostics_callback(self, data):
        """
            Callback function which holds updated
            information from robot battery in the
            object: self.diagnostics_values = DiagnosticArray()
        """
        self.diagnostics_values = data
        motor_current_values = self.motor_current()
        # Check if motor is in heavy load
        if motor_current_values['motor_current_status'] == self.MOTOR_STATUS_BAD:
            if self.timer_motor==None:
	        rospy.loginfo("DockingReinforcement.diagnostics_callback start timer_motor")
                self.timer_motor = rospy.Timer(rospy.Duration(self.TIMEOUT_MOTOR), self.timer_motor_callback, oneshot=False) 
        else:
            if self.timer_motor!=None:
                if self.timer_motor.is_alive():
	            rospy.loginfo("DockingReinforcement.diagnostics_callback stop timer_motor")
                    self.timer_motor.shutdown()
                    self.timer_motor=None
    
    def motor_current(self):
        """
            Function who return into the dictionnary motor_current
            the status of motor current
        """
        motor_current = {}
        for current in self.diagnostics_values.status:
            if current.name != "/Sensors/Motor Current":
                continue
            motor_current['motor_current_message']= current.message
            for value in current.values:
                if value.key == "Left":
                    motor_current['motor_current_left'] = value.value
                if value.key == "Right":
                    motor_current['motor_current_right'] = value.value

        if motor_current['motor_current_message'] != 'All right':
            motor_current['motor_current_status']=self.MOTOR_STATUS_BAD
            rospy.logwarn("DockingReinforcement.motor_current: L=%s R=%s Status=*%s*",
                        motor_current['motor_current_left'],
                        motor_current['motor_current_right'], 
                        motor_current['motor_current_message'])
        else:
            motor_current['motor_current_status']=self.MOTOR_STATUS_OK

        return motor_current

    def is_dock(self):
        """
            Fonction which return 1 if robot is on dock else return 0
        """
        kobuki_battery_source = ""
        for current in self.diagnostics_values.status:
            if current.name == "/Power System/Battery":
                for value in current.values:
                    if value.key == "Source": 
                        kobuki_battery_source = value.value
        if kobuki_battery_source == "Dock": 
            return 1
        else: 
            return 0

    def start_docking_app(self):
        """
            Fonction that starts auto-docking Rapp
        """
        rospy.logdebug("DockingReinforcement.start_docking_app: begin")
        rospy.wait_for_service(self.service_rocon_start_path)
        call_service = rospy.ServiceProxy(self.service_rocon_start_path, rocon_app_manager_msgs.srv.StartRapp)
        try:
            call_service("iotlab_turtlebot/auto_docking", "", "")
            rospy.logdebug("DockingReinforcement.start_docking_app: service called")
        except rospy.ServiceException as exc:
            rospy.logwarn("DockingReinforcement.start_docking_app: service request failed: %s", str(exc))

    def stop_docking_app(self):
        """
            Fonction that stops auto-docking Rapp
        """
        rospy.logdebug("DockingReinforcement.stop_docking_app")
        rospy.wait_for_service(self.service_rocon_stop_path)
        call_service = rospy.ServiceProxy(self.service_rocon_stop_path, rocon_app_manager_msgs.srv.StopRapp)
        try:
            call_service()
            rospy.logdebug("DockingReinforcement.stop_docking_app: service called")
        except rospy.ServiceException as exc:
            rospy.logwarn("DockingReinforcement.stop_docking_app: service request failed: %s", str(exc))

    def go_back(self):
        """
            Fonction that moves the robot one meter backwards from the dock.
            Needed in order to realign properly the robot in front 
            of the dock's infrared beam.
        """
	rospy.loginfo("DockingReinforcement.go_back")
        # Power on motor
        self.pub_topic_motor_power.publish(MotorPower(MotorPower.ON))
        rospy.sleep(1)
        # Go back one meter from dock
        self.go_straight(50, -0.2)

    def go_straight(self, time, speed):
        """
            Function which move the robot has the speed wished
            during a time also pass in arguments
            use: go_straight(time, speed)
        """
        rospy.logdebug("DockingReinforcement.go_straight: begin")
        self.twist_cmd.linear.x = speed  # requested speed
        for _ in range(time):  # requested time = range * 0.1
            self.pub_topic_cmd_vel.publish(self.twist_cmd)
            rospy.sleep(self.CMD_VEL_TIME_FACTOR)
        self.twist_cmd = Twist()
        self.pub_topic_cmd_vel.publish(self.twist_cmd)

    # TODO implement rescue task, or at least standby mode
    # e.g. Shutdown properly node power and netbook in order 
    # to speed up manual reboot/restore process (put robot 
    # on the dock and restart netbook)
    def rescue(self):
        """
            Function called after self.MAX_DOCKING_ATTEMPTS is reached.
        """
	rospy.loginfo("DockingReinforcement.rescue")

    def shutdown(self):
        """
            Function called on the ROS node shutdown.
        """
        rospy.loginfo("Stopping docking_reinforcement node...")
        rospy.sleep(1)


if __name__ == '__main__':
    try:
        DockingReinforcement()
    except rospy.ROSInterruptException:
        rospy.loginfo("Interruption")

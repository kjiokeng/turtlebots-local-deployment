#!/bin/sh

x_target=10
y_target=3

echo "Target is ($x_target, $y_target)."

data='{"x":'$x_target',"y":'$y_target'}'

curl -X POST -H "Content-Type: application/json; charset=UTF-8" -d $data robot:8081/robot/is_reachable

echo "\nTrying to move to target."
curl -X POST -H "Content-Type: application/json; charset=UTF-8" -d $data robot:8081/robot/go_to_point

echo "\nTurtlebot is now at position:"
curl robot:8081/robot/get_position


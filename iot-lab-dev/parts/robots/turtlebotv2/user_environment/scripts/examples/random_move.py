import httplib
import random

#Set request headers
headers = {"Content-type": "application/json"}

def build_pos(a,b):
        return '{"x":'+str(a)+',"y":'+str(b)+'}'

#Ask if goal is reachable
def askReachable(data):
    conn.request("POST", "/robot/is_reachable", data, headers)
    response = conn.getresponse()

    if not "UNREACHABLE" in response.read():
        return True
    return False

#Try to reach goal
def tryReachGoal(data):
    conn.request("POST", "/robot/go_to_point", data, headers)
    response = conn.getresponse()
    return response.read()

#Start http connection
conn = httplib.HTTPConnection("robot:8081")

STEP = 10

#Discover the x scale of the map
x_step = STEP
x_scale = STEP

while True:

    if askReachable(build_pos(x_scale,1)):
        if askReachable(build_pos(x_scale + 1,1)) == False:
            break

        x_scale += x_step

    else:
        x_step //= 2
        if x_step < 1:
            x_step = 1
        x_scale -= x_step

#Discover the x scale of the map
y_step = STEP
y_scale = STEP

while True:

    if askReachable(build_pos(1,y_scale)):
        if askReachable(build_pos(1 , y_scale + 1)) == False:
            break

        y_scale += y_step

    else:
        y_step //= 2
        if y_step < 1:
            y_step = 1
        y_scale -= y_step

print "Map scale is (" + str(x_scale) + ", " + str(y_scale) + ").\n"

#Infinite loop with goals within the map
while True:
    goal = build_pos(random.randint(1, x_scale), random.randint(1, y_scale))
    print "Going to " + goal
    tryReachGoal(goal)
 

import httplib

#Set request headers
headers = {"Content-type": "application/json"}

#Goal
data = '{"x":6.0,"y":4.0}'

#Start http connection
conn = httplib.HTTPConnection("robot:8081")

#Ask if goal is reachable
conn.request("POST", "/robot/is_reachable", data, headers)
response = conn.getresponse()
print response.read()

#Try to reach goal
conn.request("POST", "/robot/go_to_point", data, headers)
response = conn.getresponse()
print response.read()

#Check new position
conn.request("GET", "/robot/get_position")
response = conn.getresponse()
print response.read()

conn.close()

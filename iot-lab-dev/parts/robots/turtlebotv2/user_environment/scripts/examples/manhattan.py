import httplib
import random
import math
import json

#Set request headers
headers = {"Content-type": "application/json"}
next_theta = (math.pi / 2)

def build_pos(x,y):
    return '{"x":'+str(x)+',"y":'+str(y)+'}'

def round_pos(a):
    if (a - math.floor(a)) < 0.5:
        return math.floor(a)
    else:
        return math.ceil(a)

def choose_direction(x, y):
    direction = random.choice(['LEFT','FORWARD','FORWARD','RIGHT'])
    global next_theta
    theta = next_theta
    if direction == 'LEFT':
        print "Choosing left.\n"
	next_theta = theta + (math.pi / 2)
        return (x - math.sin(theta), y + math.cos(theta))
    if direction == 'FORWARD':
	next_theta = theta
        print "Choosing straight forward.\n"
        return (x + math.cos(theta), y + math.sin(theta))
    print "Choosing right.\n"
    next_theta = theta - (math.pi / 2)
    return (x + math.sin(theta), y - math.cos(theta))

#Get robot position
def get_position():
    conn.request("GET", "/robot/get_position")
    response = json.loads(conn.getresponse().read())
    return (response['x'], response['y'])

#Ask if goal is reachable
def ask_reachable(data):
    conn.request("POST", "/robot/is_reachable", data, headers)
    response = conn.getresponse()

    if not "UNREACHABLE" in response.read():
        return True
    return False

#Try to reach goal
def try_reach_goal(data):
    conn.request("POST", "/robot/go_to_point", data, headers)
    print "Going to " + data
    return conn.getresponse().read()

#Start http connection
conn = httplib.HTTPConnection("robot:8081")

#Infinite loop with goals within the map
while True:

    (x,y) = get_position()

    (x_goal, y_goal) = choose_direction(round_pos(x), round_pos(y))
    goal = build_pos(x_goal, y_goal)

    if ask_reachable(goal):
        try_reach_goal(goal)
    else:
        print "Unreachable goal"


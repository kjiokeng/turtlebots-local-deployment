#!/usr/bin/env python

# This file is a part of IoT-LAB robots
# Copyright (C) 2015 Univ. Strasbourg, INRIA (Contact: admin@iot-lab.info)
# Contributor(s) : see AUTHORS file
#
# This software is governed by the CeCILL license under French law
# and abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# http://www.cecill.info.
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""
    USB-RLY02 device running on the Turtlebot.
    Used for properly rebooting the IoT-LAB node plugged on the 5V1A power 
    input of Kobuki base

    Created for the IoT-LAB Project: http://www.iot-lab.fr
"""

# disabling pylint errors 'F0401' Unable to import,
# pylint:disable=I0011, F0401

import serial
import time
import getopt
import sys
from struct import *

import logging

import roslib; roslib.load_manifest('iotlab_node_relay')
import rospy
from std_msgs.msg import String
from std_msgs.msg import String
from iotlab_msgs.msg import Relay

LOGGER = logging.getLogger('iotlab_lib')

SERIAL_PATH = '/dev/usbrly02'
BAUD_RATE = 9600

commands = {
    'relay_1_off': 0x65,
    'relay_1_on': 0x6F,
    'relay_2_off': 0x66,
    'relay_2_on': 0x70,
    'info': 0x5A,
    'relay_states': 0x5B,
}

class NodeRelay(object):
    """
        Class NodeRelay for management of the USB-RLY02 device
    """
    def __init__(self):
        sub_topic_node_relay_path = "/iotlab/node_relay"
        self.sub_topic_node_relay = rospy.Subscriber(sub_topic_node_relay_path, Relay, self.node_relay_callback)
	LOGGER.debug("NodeRelay: object created")
        rospy.spin()

    def node_relay_callback(self, data):
        relay = data.source
        state = data.state
        if relay in [Relay.RELAY1, Relay.RELAY2]:
            command = "relay_" + str(relay) + "_"
            if state in [Relay.OFF, Relay.ON]:
                if state == Relay.OFF:
                    command = command + "off"
                else:
                    command = command + "on"
		LOGGER.info("USBRLY02 %s", command)
                self.send_command(commands[command])
            else:
	    	LOGGER.error("USBRLY02 wrong relay state")
        else:
	    LOGGER.error("USBRLY02 wrong relay number")
        

    def send_command(self,cmd, read_response = False):
        ser = serial.Serial(SERIAL_PATH, 9600)
        ser.write(chr(cmd)+'\n')
        response = read_response and ser.read() or None
        ser.close()
        return response
        
    def turn_relay_1_on(self):
        self.send_command(commands['relay_1_on'])

    def turn_relay_1_off(self):
        self.send_command(commands['relay_1_off'])

    def turn_relay_2_on(self):
        self.send_command(commands['relay_2_on'])

    def turn_relay_2_off(self):
        self.send_command(commands['relay_2_off'])

    def get_relay_states(self):
        states = self.send_command(commands['relay_states'], read_response=True)
        response = unpack('b',states)[0]
        states =  {
            0: {'1': False, '2': False},
            1: {'1': True, '2': False},
            2: {'1': False, '2': True},
            3: {'1': True, '2': True},
        }
        return states[response]

if __name__ == '__main__':
    rospy.init_node('iotlab_node_relay', anonymous=False)
    NodeRelay()

#!/usr/bin/env python
import rospy
import random
import os
from std_msgs.msg import String
import sensor_msgs.msg
import kobuki_msgs.msg
import subprocess
from kobuki_msgs.msg import ButtonEvent
from kobuki_testsuite import Square
from kobuki_testsuite import RotateTest
import math
from geometry_msgs.msg import Twist, Point, Quaternion
import tf
from math import pi
import PyKDL
import roslaunch
import time

def abort():
    print "Failure. Aborting tests."
    rospy.signal_shutdown("Failure. Aborting tests.")

def shutdown_cb():
  print "Shutting down..."

odom_frame = rospy.get_param('~odom_frame', '/odom')
base_frame = '/base_footprint'


def try_minimals():
    proc = subprocess.Popen(["rostopic list"], stdout=subprocess.PIPE, shell=True)
    out,err = proc.communicate()
    if out:
        print "Topics ok. BEEP."
        return True
    else:
        print "No topic"
        return False    

def try_camera():
    proc = subprocess.Popen(["rostopic list"], stdout=subprocess.PIPE, shell=True)
    out,err = proc.communicate()
    if "/camera/"in out:
        print "Camera topics ok. BEEP."
        return True
    else:
        print "No camera topic"   
        return False    

def blinker(led):
    pub = rospy.Publisher('/mobile_base/commands/' + led, kobuki_msgs.msg.Led)
    update_rate = 2
    
    msg = kobuki_msgs.msg.Led()
    rospy.sleep(1.0/update_rate)

    rospy.on_shutdown(shutdown_cb)
    value = 4
        
    while value != 0:
        value = value - 1 
        msg.value = value        
        pub.publish(msg)    
        rospy.sleep(1.0/update_rate)

def publish(pub, v, sec):
    cnt = 0
    m = sec * 2
    while cnt < m and not rospy.is_shutdown():
        pub.publish(v)
        cnt = cnt + 1
        rospy.sleep(0.5)

def mover(vel_x,pub):
    vel = Twist()

    vel.linear.x = vel_x
    rospy.loginfo("Moving forward")
    publish(pub, vel, 7)
    rospy.loginfo("Done")
    vel.linear.x = 0
    publish(pub, vel, 0.5)

def quat_to_angle(quat):
    rot = PyKDL.Rotation.Quaternion(quat.x, quat.y, quat.z, quat.w)
    return rot.GetRPY()[2]
       
def normalize_angle(angle):
    res = angle
    while res > pi:
        res -= 2.0 * pi
    while res < -pi:
        res += 2.0 * pi
    return res

def get_odom(tf_listener):
    try:        
        (trans, rot)  = tf_listener.lookupTransform(odom_frame, base_frame, rospy.Time(0))
    except (tf.Exception, tf.ConnectivityException, tf.LookupException):
        rospy.loginfo("TF Exception")
        return

    return (Point(*trans), quat_to_angle(Quaternion(*rot)))

def rotater(pub_cmd, tf_listener):

    yaw_rate = 1.2

    twist = Twist()
    twist.linear.x = 0
    twist.linear.y = 0
    twist.linear.z = 0
    twist.angular.x = 0
    twist.angular.y = 0
    twist.angular.z = 0

    (position, rotation) = get_odom(tf_listener)
    last_angle = rotation
    delta_angle = 0

    while not rospy.is_shutdown() and not delta_angle >= random.uniform(4.0, 6.0)*pi/5:
        (position, rotation) = get_odom(tf_listener)
        delta_angle = normalize_angle(rotation - last_angle)
        #print delta_angle

        twist.angular.z = yaw_rate
        pub_cmd.publish(twist)
        #rate.sleep()
        rospy.sleep(0.02)
    twist.angular.z = 0
    pub_cmd.publish(twist)

if __name__ == '__main__':
    try:
        if not try_minimals():
            abort()
        else:
            rospy.init_node("test_events")

	    cmdvel_topic ='/cmd_vel_mux/input/teleop'
            pub = rospy.Publisher(cmdvel_topic, Twist)
            cmd_vel_topic = '/mobile_base/commands/velocity'
            pub_cmd = rospy.Publisher(cmd_vel_topic,Twist)

	    tf_listener = tf.TransformListener()
            tf_listener.waitForTransform(odom_frame, '/base_footprint', rospy.Time(), rospy.Duration(1.0))

	    loopstop = False

	    n = 0
	
	    while loopstop == False:

		try:
		    n += 1	
		    print "Experience %d started." % n

                    mover(-0.15, pub)
                    #mover(-0.05, pub)
                    rotater(pub_cmd, tf_listener)
                    #mover(0.09, pub)

	            os.system("roslaunch kobuki_auto_docking activate.launch")

		    time.sleep(20)
		except (KeyboardInterrupt, SystemExit):
		    loopstop = True
		    #abort()
		    #print "%%%%%%%%TEST%%%%%%%%%%%"
		    #raise SystemExit(0)

    except rospy.ROSInterruptException:
        pass

#!/usr/bin/env python

# This file is a part of IoT-LAB robots
# Copyright (C) 2015 Univ. Strasbourg, INRIA (Contact: admin@iot-lab.info)
# Contributor(s) : see AUTHORS file
#
# This software is governed by the CeCILL license under French law
# and abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# http://www.cecill.info.
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import rospy
import random
import os
from std_msgs.msg import String
import sensor_msgs.msg
import kobuki_msgs.msg
import subprocess
from kobuki_msgs.msg import ButtonEvent
from kobuki_testsuite import Square
from kobuki_testsuite import RotateTest
import math
from geometry_msgs.msg import Twist, Point, Quaternion
import tf
from math import pi
import PyKDL
import roslaunch

#odom_frame = rospy.get_param('~odom_frame', '/odom')
#base_frame = '/base_footprint'

def abort():
    print "Failure. Aborting tests."
    rospy.signal_shutdown("Failure. Aborting tests.")

def shutdown_cb():
  print "Shutting down..."

def launch_minimals():
    package = 'turtlebot_bringup'
    executable = 'minimal.launch'

    package = 'rqt_gui'
    executable = 'rqt_gui'

    node = roslaunch.core.Node(package, executable)

    launch = roslaunch.scriptapi.ROSLaunch()
    launch.start()

    process = launch.launch(node)
    print process.is_alive()
    #process.stop()

#launch_minimals()

odom_frame = rospy.get_param('~odom_frame', '/odom')
base_frame = '/base_footprint'


def try_minimals():
    proc = subprocess.Popen(["rostopic list"], stdout=subprocess.PIPE, shell=True)
    out,err = proc.communicate()
    if out:
        print "Topics ok. BEEP."
        #os.system("rosrun sound_play play.py /home/turtlebot/smw_coin.wav")
        return True
    else:
        print "No topic"
        return False    

def try_camera():
    proc = subprocess.Popen(["rostopic list"], stdout=subprocess.PIPE, shell=True)
    out,err = proc.communicate()
    if "/camera/"in out:
        print "Camera topics ok. BEEP."
        #os.system("rosrun sound_play play.py /home/turtlebot/smw_coin.wav")
        return True
    else:
        print "No camera topic"   
        return False    

def blinker(led):
    pub = rospy.Publisher('/mobile_base/commands/' + led, kobuki_msgs.msg.Led)
    #update_rate = rospy.get_param('~update_rate',1.0)
    update_rate = 2
    
    msg = kobuki_msgs.msg.Led()
    rospy.sleep(1.0/update_rate)

    rospy.on_shutdown(shutdown_cb)
    value = 4
        
    while value != 0:
        value = value - 1 
        msg.value = value        
        pub.publish(msg)    
        rospy.sleep(1.0/update_rate)

def publish(pub, v, sec):
    cnt = 0
    m = sec * 2
    while cnt < m and not rospy.is_shutdown():
        pub.publish(v)
        cnt = cnt + 1
        rospy.sleep(0.5)

def mover(vel_x,pub):
    #cmdvel_topic ='/cmd_vel_mux/input/teleop'
    #pub = rospy.Publisher(cmdvel_topic, Twist)
    vel = Twist()

    vel.linear.x = vel_x
    rospy.loginfo("Moving forward")
    publish(pub, vel, 7)
    rospy.loginfo("Done")
    vel.linear.x = 0
    publish(pub, vel, 0.5)

def quat_to_angle(quat):
    rot = PyKDL.Rotation.Quaternion(quat.x, quat.y, quat.z, quat.w)
    return rot.GetRPY()[2]
       
def normalize_angle(angle):
    res = angle
    while res > pi:
        res -= 2.0 * pi
    while res < -pi:
        res += 2.0 * pi
    return res

def get_odom(tf_listener):
    #tf_listener = tf.TransformListener()
    #odom_frame = rospy.get_param('~odom_frame', '/odom')
    #tf_listener.waitForTransform(odom_frame, '/base_footprint', rospy.Time(), rospy.Duration(1.0))
    #base_frame = '/base_footprint'

    try:        
        (trans, rot)  = tf_listener.lookupTransform(odom_frame, base_frame, rospy.Time(0))
    except (tf.Exception, tf.ConnectivityException, tf.LookupException):
        rospy.loginfo("TF Exception")
        return

    return (Point(*trans), quat_to_angle(Quaternion(*rot)))

def rotater(pub_cmd, tf_listener):
    #cmd_vel_topic = '/mobile_base/commands/velocity'
    #pub_cmd = rospy.Publisher(cmd_vel_topic,Twist)

    yaw_rate = 1.2
    #freq = 5
    #rate = rospy.Rate(freq)    

    twist = Twist()
    twist.linear.x = 0
    twist.linear.y = 0
    twist.linear.z = 0
    twist.angular.x = 0
    twist.angular.y = 0
    twist.angular.z = 0

    #max_rotate_count = freq*int(3.14/yaw_rate)

    #start_time =rospy.get_rostime()

    #update = yaw_rate/10.0

    (position, rotation) = get_odom(tf_listener)
    last_angle = rotation
    delta_angle = 0

    while not rospy.is_shutdown() and not delta_angle >= pi/2:
        (position, rotation) = get_odom(tf_listener)
        delta_angle = normalize_angle(rotation - last_angle)
        print delta_angle

        #while math.fabs(twist.angular.z) <= yaw_rate:
        #    twist.angular.z = twist.angular.z + update
        #    pub_cmd.publish(twist)
        #    rospy.sleep(0.04)


        twist.angular.z = yaw_rate
        #now = rospy.get_rostime()
        pub_cmd.publish(twist)
        #rate.sleep()
        rospy.sleep(0.02)
    twist.angular.z = 0
    pub_cmd.publish(twist)

def ButtonEventCallback(data):
    if ( data.state == ButtonEvent.RELEASED and data.button == ButtonEvent.Button0) :
        os.system("roslaunch turtlebot_dashboard turtlebot_dashboard.launch")
    elif ( data.state == ButtonEvent.RELEASED and data.button == ButtonEvent.Button1) :
        blinker("led1")
        blinker("led2")
        cmdvel_topic ='/cmd_vel_mux/input/teleop'
        pub = rospy.Publisher(cmdvel_topic, Twist)
        cmd_vel_topic = '/mobile_base/commands/velocity'
        pub_cmd = rospy.Publisher(cmd_vel_topic,Twist) 
        
        tf_listener = tf.TransformListener()
        tf_listener.waitForTransform(odom_frame, '/base_footprint', rospy.Time(), rospy.Duration(1.0))

        mover(-0.05, pub)
        rotater(pub_cmd, tf_listener)
        mover(0.15, pub)
        rotater(pub_cmd, tf_listener)
        mover(0.15,pub)
        rotater(pub_cmd, tf_listener)
        mover(0.11, pub)
#        rotater(pub_cmd, tf_listener)        
#        mover(0.15, pub)

    elif ( data.state == ButtonEvent.RELEASED and data.button == ButtonEvent.Button2) :
        os.system("roslaunch kobuki_auto_docking activate.launch")

if __name__ == '__main__':
    try:
        #os.system("")
        #launch_minimals()
        if not try_minimals()  or not try_camera():
            abort()
        else:
           # blinker()
            rospy.init_node("test_events")
            rospy.Subscriber("/mobile_base/events/button",ButtonEvent,ButtonEventCallback)
            rospy.spin()
    except rospy.ROSInterruptException:
        pass

#!/bin/sh
gnome-terminal -x sh -c 'roslaunch turtlebot_bringup minimal.launch'
sleep 3
gnome-terminal -x sh -c 'roslaunch turtlebot_bringup 3dsensor.launch'
sleep 3
gnome-terminal -x sh -c 'roslaunch kobuki_auto_docking minimal.launch'
python test.py

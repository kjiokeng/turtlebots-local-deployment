#! /usr/bin/env python
# -*- coding:utf-8 -*-

# This file is a part of IoT-LAB robots
# Copyright (C) 2015 Univ. Strasbourg, INRIA (Contact: admin@iot-lab.info)
# Contributor(s) : see AUTHORS file
#
# This software is governed by the CeCILL license under French law
# and abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# http://www.cecill.info.
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""
REST Server
"""

import sys
import logging
import json
import bottle
from bottle import request

class M3MockRESTServer(object):
 
    HOST = 'localhost'
    PORT = '8080'

    def __init__(self):
        self._app_routing()

    def _app_routing(self):
        """
        Declare the REST supported methods 
        """
        bottle.route('/exp/start/<expid:int>/<username>',
                     'POST')(self.exp_start)
        bottle.route('/exp/stop', 'DELETE')(self.exp_stop)
    
    """
    START an experiment : 
    """
    def exp_start(self, expid, username):
        ret = 0
        return {'ret': ret}

    """
    STOP an experiment : 
    """
    def exp_stop(self):	
        ret = 0
        return {'ret': ret}


#
# Command line functions
#
def _parse_arguments(args):
    """
    Parse arguments:
        [host, port]

    :param args: arguments, without the script name == sys.argv[1:]
    :type args: list
    """
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('host', type=str,
                        help="Server address to bind to")
    parser.add_argument('port', type=int, 
                        help="Server port to bind to")
    arguments = parser.parse_args(args)

    return arguments.host, arguments.port


def _main(args):    
    """
    Command line main function
    """

    HOST, PORT, = _parse_arguments(args[1:])

    _ = M3MockRESTServer()
    bottle.run(host=HOST, port=PORT, server='paste')

if __name__ == '__main__':
    _main(sys.argv)

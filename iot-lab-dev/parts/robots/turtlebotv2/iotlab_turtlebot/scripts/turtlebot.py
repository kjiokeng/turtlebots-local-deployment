#!/usr/bin/env python

# This file is a part of IoT-LAB robots
# Copyright (C) 2015 Univ. Strasbourg, INRIA (Contact: admin@iot-lab.info)
# Contributor(s) : see AUTHORS file
#
# This software is governed by the CeCILL license under French law
# and abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# http://www.cecill.info.
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

""" turtlebot.py - Version 0.0.1 2014/03/01
    Command a robot to move it autonomously via remote services.
"""

import roslib; roslib.load_manifest('iotlab_turtlebot')
import rospy

import iotlab_lib
from iotlab_lib import TurtlebotManager

###############################################################################
# Main
def iotlab_manager():
    manager = TurtlebotManager()
    iotlab_lib.loginfo("IoT-LAB manager Ready")
    rospy.spin()

if __name__ == '__main__':
    #rospy.init_node('iotlab_turtlebot')
    iotlab_manager()

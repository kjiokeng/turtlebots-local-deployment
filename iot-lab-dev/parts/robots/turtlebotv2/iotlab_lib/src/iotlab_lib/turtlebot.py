# -*- coding:utf-8 -*-
# This file is a part of IoT-LAB robots
# Copyright (C) 2015 Univ. Strasbourg, INRIA (Contact: admin@iot-lab.info)
# Contributor(s) : see AUTHORS file
#
# This software is governed by the CeCILL license under French law
# and abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# http://www.cecill.info.
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Turtlebot robot implementation for mobile nodes.

Created for the IoT-LAB Project: https://www.iot-lab.info
"""

import sys
import logging

import rospy

from iotlab_msgs.msg import RobotState, RobotPower
from iotlab_lib import rapp
from iotlab_lib.helpers import yaml_pose_reader
from iotlab_lib import (turtlebot_navigation,
                        turtlebot_power, turtlebot_wheels,
                        turtlebot_iotlab_node, turtlebot_leds)

LOGGER = logging.getLogger('iotlab_lib')


class Turtlebot(object):
    """
        Class Turtlebot contain all function usefull to use robot.
    """
    TURTLEBOT_NAME = rospy.get_param('/iotlab/turtlebot_name')

    STATE_TOPIC = '/{0}/robot_state'.format(TURTLEBOT_NAME)
    POSE_TOPIC = '/{0}/robot_pose'.format(TURTLEBOT_NAME)
    POWER_TOPIC = '/{0}/robot_power'.format(TURTLEBOT_NAME)

    # Robot specific poses
    DOCK_POSE_FILENAME = rospy.get_param('/iotlab/docks_pose_filename')
    DOCKS_AREA_FILENAME = rospy.get_param('/iotlab/docking_area_filename')
    INIT_POSE = yaml_pose_reader(DOCK_POSE_FILENAME, 'pose', TURTLEBOT_NAME)
    DOCK_GOAL = yaml_pose_reader(DOCKS_AREA_FILENAME, 'goals', TURTLEBOT_NAME)

    STATES = (
        'INIT', 'DOCKED', 'UNDOCKING', 'WAIT_POINT', 'GOING_POINT',
        'GOING_DOCK', 'NEAR_DOCK', 'DOCKING',
        'ERROR',
    )

    #######################################################################
    # Generic Interfaces
    def __init__(self):
        # Create a ROS Node First (allow calling 'publish')
        rospy.init_node('iotlab_turtlebot', anonymous=True)

        self.wheels = turtlebot_wheels.Wheels()
        self.power = turtlebot_power.Power(self.POWER_TOPIC)
        self.nav = turtlebot_navigation.Navigation(self.POSE_TOPIC,
                                                   self.INIT_POSE)
        self.iotlab_node = turtlebot_iotlab_node.IoTLABNode()
        self.leds = {1: turtlebot_leds.Led(1), 2: turtlebot_leds.Led(2)}
        self.dock_rapp = rapp.Rapp('iotlab_turtlebot/auto_docking')

        # Robot state, implemented as 'state' property
        self._state = 'INIT'
        self._state_topic = rospy.Publisher(self.STATE_TOPIC, RobotState,
                                            queue_size=10)

        # Topics Subscribers
        rospy.Subscriber(self.POWER_TOPIC, RobotPower, self.power_cb)

        rospy.sleep(1)

        LOGGER.debug('Turtlebot.init')

    @property
    def battery(self):
        """Turtlebot battery level."""
        return self.power.level

    #######################################################################
    # Status specific's functions

    @property
    def state(self):
        """Turtlebot state."""
        return self._state

    @state.setter
    def state(self, state):
        """Set Turtlebot state."""
        assert state in self.STATES
        if self.state == state:
            return
        self._state = state
        self._state_publish()
        LOGGER.debug('Turtlebot.state: %s', self.state)

    def _state_publish(self):
        """Publish robot state."""
        state = RobotState(self.state)
        self._state_topic.publish(state)

    def bookable(self):
        """Turtlebot can run a new experiment.

        It does if on dock with 'high' battery level
        """
        return (self.state == 'DOCKED' and
                self.power.level == 'high')

    def status(self):
        """
            Function return a dictionnary which
            contain status information about the robot.
        """
        status_dict = {
            'power': self.power.status_dict(),
            'bookable': self.bookable(),
            'robot_state': self.state,
            'position': self.nav.position_dict(),
        }

        return status_dict

    #######################################################################
    # Docking specific's functions#

    def undock(self):
        """Undock robot """
        LOGGER.debug('Turtlebot.undock: begin')
        if self._require_state('DOCKED'):  # Maybe handle 'init' here too
            return 1

        if self.power.level != 'high':
            LOGGER.warning('Turtlebot.undock: battery low')
            self.power.battery_not_high_warning()
            return 1

        self.state = 'UNDOCKING'
        self._undock()
        self.state = 'WAIT_POINT'
        return 0

    def _undock(self):
        """Do undock."""
        self.nav.start()
        self.wheels.motor_on()
        self.wheels.reverse_gear_and_turn_back()

    def go_to_dock(self):
        """Move robot to the docking area to prepare docking."""
        LOGGER.debug('Turtlebot.go_to_dock: begin')
        if self._require_state('WAIT_POINT'):
            return 1

        self.state = 'GOING_DOCK'
        ret = self.nav.go_to_pose(self.DOCK_GOAL)
        if ret == 0:
            self.state = 'NEAR_DOCK'
        else:
            LOGGER.error('Turtlebot.go_to_dock: failed')
            self.state = 'ERROR'

        return ret

    def dock(self):
        """Dock robot."""
        LOGGER.debug('Turtlebot.dock: begin')
        if self._require_state('NEAR_DOCK'):
            return 1

        ret = self._dock()
        return ret

    def _dock(self):
        """Do dock robot."""
        # Stop navigation
        self.nav.stop()

        self.state = 'DOCKING'

        # Run docking app
        return self.dock_rapp.start_with_retry()

    def iotlab_node_alim(self, value):
        """Set iotlab node alim 'on' if `value`else 'off'."""
        if value:
            self.iotlab_node.power_on()
        else:
            self.iotlab_node.power_off()
        return 0

    #######################################################################
    # Navigation specific's functions
    def go_to_pose(self, pose, timeout=300):
        """Go to pose. With timeout."""
        if self._require_state('WAIT_POINT'):
            return 1

        self.state = 'GOING_POINT'
        ret = self.nav.go_to_pose(pose, timeout)
        self.state = 'WAIT_POINT'

        return ret

    def is_reachable(self, pose):
        """Checks if the point (x,y) is in a empty space in the map."""
        return self.nav.is_reachable(pose)

    def position_dict(self):
        """Return robot position dict: x, y and theta."""
        return self.nav.position_dict()

    def _require_state(self, *states):
        """Requires current state to be in given `states`."""
        if self.state in states:
            return 0

        LOGGER.error('Turtlebot.%s: state %r should be in %r', self._caller(),
                     self.state, states)
        return 1

    @staticmethod
    def _caller():
        """Caller name of function calling this function."""
        caller = sys._getframe(2)  # pylint:disable=protected-access
        return caller.f_code.co_name

    # # # # # # #
    # Callbacks #
    # # # # # # #

    def power_cb(self, robot_power):
        """Update power state.

        Handle node docked when power == dock.

        :type robot_power: RobotPower
        """
        if robot_power.power_source != 'Dock':
            return

        if self.state in ('INIT', 'DOCKING'):
            # Don't block in callback
            self.wheels.motor_off(block=False)
            self.state = 'DOCKED'
        elif self.state in ('UNDOCKING', 'DOCKED', 'ERROR'):
            pass
        else:
            LOGGER.critical('Error: Power == Dock but previous state'
                            '%r not in %r', self.state, ['INIT', 'DOCKING'])

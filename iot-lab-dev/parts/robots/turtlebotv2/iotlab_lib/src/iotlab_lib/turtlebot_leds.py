# -*- coding:utf-8 -*-

# This file is a part of IoT-LAB robots
# Copyright (C) 2015 Univ. Strasbourg, INRIA (Contact: admin@iot-lab.info)
# Contributor(s) : see AUTHORS file
#
# This software is governed by the CeCILL license under French law
# and abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# http://www.cecill.info.
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Turtlebot.leds implementation.

Manage robot leds
"""

import logging

import rospy

from kobuki_msgs.msg import Led as _Led

from iotlab_msgs.srv import KobukiLed, KobukiLedResponse

LOGGER = logging.getLogger('iotlab_lib')


class Led(object):  # pylint:disable=too-few-public-methods
    """Turtlebot Leds."""

    TURTLEBOT_NAME = rospy.get_param('/iotlab/turtlebot_name')

    LED_SRV_FMT = '/{0}/handle_led%s'.format(TURTLEBOT_NAME)

    LED_TOPIC_FMT = '/mobile_base/commands/led%s'
    COLORS_DICT = {
        'GREEN': _Led(_Led.GREEN),
        'ORANGE': _Led(_Led.ORANGE),
        'RED': _Led(_Led.RED),
        'BLACK': _Led(_Led.BLACK),
    }

    def __init__(self, num):
        assert num in (1, 2)

        self._color = 'BLACK'
        self.num = num

        led_topic = self.LED_TOPIC_FMT % self.num
        self.led = rospy.Publisher(led_topic, _Led, queue_size=10)

        # Led service
        led_srv = self.LED_SRV_FMT % self.num
        self.srv = rospy.Service(led_srv, KobukiLed, self._led_srv)
        LOGGER.debug('Turtlebot.Led%s: service ready', self.num)

    def _shutdown(self):
        """Shutdown service.

        Used in tests"""
        self.srv.shutdown()

    @property
    def color(self):
        """Led color."""
        return self._color

    @color.setter
    def color(self, value):
        """Set Led Color."""
        try:
            self.led.publish(self.COLORS_DICT[value])
            self._color = value
        except KeyError:
            raise ValueError('color must be in %r' % self.COLORS_DICT.keys())

    def _led_srv(self, req):
        """Led service."""
        try:
            self.color = req.Led_Value
            response = 'LED%s [DONE]' % self.num
        except ValueError:
            LOGGER.error('TurtlebotServices: handle led%s', self.num)
            response = 'LED%s [ERROR]' % self.num

        return KobukiLedResponse(response)

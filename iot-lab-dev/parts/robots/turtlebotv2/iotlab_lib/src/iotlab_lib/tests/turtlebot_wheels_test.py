# -*- coding:utf-8 -*-
"""Test iotlab_lib.turtlebot_wheels module."""

import time
import unittest

from mock import Mock

import rospy

from .common import SERVICE_SETUP_WAIT, SERVICE_TEARDOWN_WAIT
from .. import turtlebot_wheels


class TestTurtlebotWheels(unittest.TestCase):
    """Test turtlebot Wheels."""

    def setUp(self):
        rospy.init_node('iotlab_test', anonymous=True)

        self.wheel = turtlebot_wheels.Wheels()
        self.wheel.motor_topic = Mock()
        self.wheel.cmd_vel = Mock()
        rospy.sleep(SERVICE_SETUP_WAIT)

    def tearDown(self):
        self.wheel._shutdown()  # pylint:disable=protected-access
        rospy.sleep(SERVICE_TEARDOWN_WAIT)

    def test_motor(self):
        """Test motor."""
        # TODO verify calls here...
        t_0 = time.time()
        self.wheel.motor_on()
        t_1 = time.time()
        self.wheel.motor_off(block=False)
        t_2 = time.time()

        self.assertTrue(t_1 - t_0 >= 1)
        self.assertTrue(t_2 - t_1 < 1)

    def test_reverse_gear_and_turn_back(self):
        """Test out of dock procedure."""
        # Only real life tested, not unittested... :)
        # TODO monitor odometry test
        self.wheel.reverse_gear_and_turn_back()
        # TODO Ensure called correctly

    def test_motor_service(self):
        """Test motor service."""
        motor_srv = turtlebot_wheels.Wheels.MOTOR_POWER_SRV
        rospy.wait_for_service(motor_srv)
        set_motor = rospy.ServiceProxy(motor_srv,
                                       turtlebot_wheels.KobukiMotorPower)
        # TODO verify calls here...

        # Set motor ON
        req = set_motor('ON')
        self.assertEqual(req.MotorPower_Response, 'Motor Power [DONE]')

        # Set motor OFF
        req = set_motor('OFF')
        self.assertEqual(req.MotorPower_Response, 'Motor Power [DONE]')

        # Invalid color
        req = set_motor('METS LES GAZS!')
        self.assertEqual(req.MotorPower_Response, 'Motor Power [ERROR]')

# -*- coding:utf-8 -*-
"""Test iotlab_lib.helpers module."""

import os
import math
import unittest
from iotlab_lib import helpers

CURRENT_DIR = os.path.dirname(__file__)


def assertPoseEqual(testcase,  # pylint:disable=invalid-name
                    pose, x, y, theta=0.0):
    """Compare Pose to x, y and theta."""
    theta_expected = helpers.theta_from_quaternion(pose.orientation)

    testcase.assertEqual((x, y), (pose.position.x, pose.position.y))
    testcase.assertAlmostEqual(theta, theta_expected)


class TestHelpers(unittest.TestCase):
    """Test helpers functions."""

    def test_pose(self):
        """Test helpers.pose."""
        pose = helpers.pose(1.0, 2.0)
        self.assertEqual((1.0, 2.0, 0.0),
                         (pose.position.x,
                          pose.position.y,
                          pose.position.z))

    def test_theta_quaternion(self):
        """Test Theta/Quaternion conversion."""
        theta = math.pi

        quat = helpers.quaternion_from_theta(theta)
        new_theta = helpers.theta_from_quaternion(quat)
        new_quat = helpers.quaternion_from_theta(new_theta)

        self.assertEqual(theta, new_theta)
        self.assertEqual(quat, new_quat)

    def test_yaml_pose_reader(self):
        """Test yaml_pose_reader."""
        filename = os.path.join(CURRENT_DIR, 'docking_area.yaml')

        pose = helpers.yaml_pose_reader(filename, 'goals', 'dot_one')
        assertPoseEqual(self, pose, 1.1, 2.1, 0.1)

        pose = helpers.yaml_pose_reader(filename, 'goals', 'dot_two')
        assertPoseEqual(self, pose, 1.2, 2.2, 0.2)

# -*- coding:utf-8 -*-

"""Test rest_server implementation."""


import os
import tarfile
from cStringIO import StringIO
import unittest

import mock
import webtest

from iotlab_lib.tests import common
from iotlab_lib import server_rest, turtlebot_manager

CUR_DIR = os.path.dirname(__file__)


class TestRestServer(unittest.TestCase):
    """Test rest_server.TurtlebotRESTServer."""

    EXP_START = '/exp/start/123/harter'
    SSH_KEYS = ['ssh-rsa AAAAA\n', 'ssh-rsa BBBBB\n']
    USER_IP = '1.2.3.4'

    MOBILITY_DICT = common.MOBILITY_DICT.copy()

    def setUp(self):
        self.manager = mock.create_autospec(turtlebot_manager.TurtlebotManager)
        self.rest_server = server_rest.TurtlebotRESTServer(self.manager)
        self.server = webtest.TestApp(self.rest_server)
        self.server.extra_environ.update(
            {'REMOTE_ADDR': str(self.rest_server.SRVEH_ADDR)})

    def test_experiment_running(self):
        """Test an experiment run."""
        json_post = {
            'ssh_keys': self.SSH_KEYS,
            'mobility': self.MOBILITY_DICT,
        }
        # Insert mobility dict inside json request
        self.manager.exp_start.return_value = 0

        ret = self.server.post_json(self.EXP_START, json_post)
        self.assertEqual(ret.json['ret'], 0)
        self.manager.exp_start.assert_called_with(
            'harter', 123, self.SSH_KEYS,
            mobility_dict=self.MOBILITY_DICT, robot_user_env=False)

        # Update to 'robot_user_env'
        self.manager.exp_user_env_start.return_value = 0

        ret = self.server.put('/robot/user_env')
        self.assertEqual(ret.json['ret'], 0)
        self.assertTrue(self.manager.exp_user_env_start.called)

        # Update with mobility
        self.manager.exp_update_mobility.return_value = 0
        json_post = self.MOBILITY_DICT

        ret = self.server.post_json('/exp/update', json_post)
        self.assertEqual(ret.json['ret'], 0)
        self.manager.exp_update_mobility.assert_called_with(self.MOBILITY_DICT)

        # Go to point
        self.manager.exp_go_to_point.return_value = 0

        json_post = {'x': 0.0, 'y': 1.0, 'theta': 3.14, 'name': 'a'}
        ret = self.server.post_json('/robot/go_to_point', json_post)
        self.assertEqual(ret.json['ret'], 0)
        self.manager.exp_go_to_point.assert_called_with(x=0.0, y=1.0,
                                                        theta=3.14, name='a')

        json_post = {'x': 1.0, 'y': 2.0}
        ret = self.server.post_json('/robot/go_to_point', json_post)
        self.assertEqual(ret.json['ret'], 0)
        self.manager.exp_go_to_point.assert_called_with(x=1.0, y=2.0)

        self.manager.exp_go_to_point.reset_mock()
        json_post = {'X': 1.0, 'Y': 2.0}  # invalid args
        ret = self.server.post_json('/robot/go_to_point', json_post)
        self.assertEqual(ret.json['ret'], 1)
        self.assertFalse(self.manager.exp_go_to_point.called)

        # Remove 'robot_user_env'
        self.manager.exp_user_env_stop.return_value = 0

        ret = self.server.delete('/robot/user_env')
        self.assertEqual(ret.json['ret'], 0)
        self.assertTrue(self.manager.exp_user_env_stop.called)

        # Dock in experiment
        self.manager.exp_go_to_dock.return_value = 0
        ret = self.server.post('/exp/dock')
        self.assertEqual(ret.json['ret'], 0)

        # Stop experiment
        self.manager.exp_stop.return_value = 0
        ret = self.server.delete('/exp/stop')
        self.assertEqual(ret.json['ret'], 0)

    def test_user_is_reachable(self):
        """Test is_reachable as user."""
        self.server.extra_environ.update({'REMOTE_ADDR': self.USER_IP})
        params = {'x': 0.0, 'y': 1.1}

        # Reachable
        self.manager.is_reachable.return_value = True
        ret = self.server.get('/robot/is_reachable', params=params)
        self.manager.is_reachable.assert_called_with(0.0, 1.1)
        self.assertTrue(ret.json['reachable'])
        self.assertEqual(ret.json['pose'], params)
        self.manager.is_reachable.reset_mock()

        # Unreachable
        self.manager.is_reachable.return_value = False
        ret = self.server.get('/robot/is_reachable', params=params)
        self.manager.is_reachable.assert_called_with(0.0, 1.1)
        self.assertFalse(ret.json['reachable'])
        self.assertEqual(ret.json['pose'], params)
        self.manager.is_reachable.reset_mock()

        # Malformed requests
        self.manager.is_reachable.reset_mock()
        self.manager.is_reachable.side_effect = RuntimeError

        # Missing parameter
        self.server.get('/robot/is_reachable', params={'x': 0.0}, status=400)
        # Non float
        self.server.get('/robot/is_reachable', params={'x': '', 'y': ''},
                        status=400)
        self.server.get('/robot/is_reachable', params={'x': 'one', 'y': 'two'},
                        status=400)

        # Should not have been called
        self.assertFalse(self.manager.is_reachable.called)

    def test_user_requests(self):
        """Test calling method as non admin."""
        self.server.extra_environ.update({'REMOTE_ADDR': self.USER_IP})

        status = {'yeah': 'ok'}
        self.manager.status.return_value = status
        ret = self.server.get('/robot/status')
        self.assertEqual(ret.json, status)

        version = '1.2.3rc-candidate-gamma'
        self.manager.robot_version.return_value = version
        ret = self.server.get('/robot/version')
        self.assertEqual(ret.json, {'version': version})

        pose = {'x': 0.0, 'y': 1.1, 'theta': 3.14}
        self.manager.position.return_value = pose
        ret = self.server.get('/robot/position')
        self.assertEqual(ret.json, pose)

        # self.route('/robot/go_to_point', 'POST', self.go_to_point)
        # self.route('/robot/is_reachable', 'GET', self.is_reachable)

    def test_unauthorized_requests(self):
        """Test calling methods as a non admin."""
        self.server.extra_environ.update({'REMOTE_ADDR': self.USER_IP})

        self.server.post_json(self.EXP_START, status=403)
        self.server.delete('/exp/stop', status=403)
        self.server.post('/exp/dock', status=403)
        self.server.post('/exp/update', status=403)
        self.server.get('/exp/results/harter_123.tar.gz', status=403)
        self.server.delete('/exp/results/harter_123.tar.gz', status=403)

    def test_exp_results(self):
        """Testing exp_results methods."""

        self.manager.exp_results_get.return_value = os.path.join(
            CUR_DIR, 'test_user_0.tar.gz')
        self.manager.exp_results_del.return_value = 0

        # Get file
        ret = self.server.get('/exp/results/test_user_0.tar.gz')

        # Verify is an archive and file content looks like
        # However, found that here it's a tar archive and not a tar.gz anymore
        archive = StringIO(ret.body)
        with tarfile.open(fileobj=archive) as arch:
            files = arch.getnames()
            self.assertEqual(len(files), 3)
            self.assertTrue(files[-1].endswith('.oml'))

        # Delete file
        ret = self.server.delete('/exp/results/test_user_0.tar.gz')
        self.assertEqual(ret.json['ret'], 0)

    # Admin

    def test_admin_dock_undock(self):
        """Test REST admin dock and undock."""
        self.manager.admin_dock.return_value = 0
        ret = self.server.post('/admin/dock')
        self.assertEqual(ret.json['ret'], 0)

        self.manager.admin_undock.return_value = 0
        ret = self.server.post('/admin/undock')
        self.assertEqual(ret.json['ret'], 0)

    # Compat

    def test_compat_exp_update_mobility(self):
        """Compatibility for update_mobility."""
        controlled = {
            "type": "controlled",
            "coordinates": None,
            "points": None,
        }

        self.manager.exp_user_env_start.return_value = 0

        # Update user env with with mobility controlled
        ret = self.server.post_json('/exp/update', controlled)
        self.assertEqual(ret.json['ret'], 0)
        self.assertTrue(self.manager.exp_user_env_start.called)
        self.assertFalse(self.manager.exp_update_mobility.called)

        # Update mobility with old format
        json_dict = {
            "site_name": "grenoble",
            "type": "predefined",
            "coordinates": [
                {
                    "name": "3",
                    "w": 3.14,
                    "x": 43.71,
                    "y": 26.4,
                    "z": 0.0
                },
                {
                    "name": "2",
                    "w": 0.0,
                    "x": 20.61,
                    "y": 26.4,
                    "z": 0.0
                }
            ],
            "points": [
                "3",
                "2"
            ],
            "ssh_keys": ["# SSH KEYS\n"],
            "trajectory_name": "Jhall_w",
        }
        self.manager.exp_update_mobility.return_value = 0
        self.manager.exp_user_env_start.reset_mock()

        ret = self.server.post_json('/exp/update', json_dict)
        self.assertEqual(ret.json['ret'], 0)
        self.assertFalse(self.manager.exp_user_env_start.called)
        self.assertTrue(self.manager.exp_update_mobility.called)

    def test_compat_mobility_dict(self):
        """Compatibility with existing rest format."""
        json_dict = {
            "site_name": "grenoble",
            "type": "predefined",
            "coordinates": [
                {
                    "name": "3",
                    "w": 3.14,
                    "x": 43.71,
                    "y": 26.4,
                    "z": 0.0
                },
                {
                    "name": "2",
                    "w": 0.0,
                    "x": 20.61,
                    "y": 26.4,
                    "z": 0.0
                }
            ],
            "points": [
                "3",
                "2"
            ],
            "ssh_keys": ["# SSH KEYS\n"],
            "trajectory_name": "Jhall_w",
        }

        expected = {
            'robot_user_env': False,
            'mobility_dict': {
                'coordinates': {
                    '3': {
                        'name': '3',
                        'theta': 3.14,
                        'x': 43.71,
                        'y': 26.4,
                    },
                    '2': {
                        'name': '2',
                        'theta': 0.0,
                        'x': 20.61,
                        'y': 26.4,
                    }
                },
                'points': ['3', '2'],
                'site': 'grenoble',
                'name': 'Jhall_w',
                'type': 'predefined',
                'loop': True,
            }
        }
        # pylint:disable=protected-access
        ret = self.rest_server._mobility_cfg(json_dict)
        self.assertEqual(ret, expected)

        json_dict = {
            'coordinates': None,
            'points': None,
            'site_name': 'strasbourg',
            'ssh_keys': None,
            'trajectory_name': 'square',
            'type': 'predefined'
        }
        expected = {
            'robot_user_env': False,
            'mobility_dict': None,
        }
        # pylint:disable=protected-access
        ret = self.rest_server._mobility_cfg(json_dict)
        self.assertEqual(ret, expected)

        json_dict = {
            "type": "controlled",
            "coordinates": None,
            "points": None,
            "ssh_keys": ["# SSH KEYS\n"]
        }
        expected = {
            'robot_user_env': True,
            'mobility_dict': None,
        }
        # pylint:disable=protected-access
        ret = self.rest_server._mobility_cfg(json_dict)
        self.assertEqual(ret, expected)


class TestServerRestMain(unittest.TestCase):
    """Call rest server main function."""

    @mock.patch('iotlab_lib.turtlebot_manager.TurtlebotManager', autospec=True)
    @mock.patch('bottle.run')
    def test_main_function(self, run_mock, manager):
        """Test starting with main function."""
        server_rest.main()
        self.assertTrue(run_mock.called)
        self.assertTrue(manager.called)

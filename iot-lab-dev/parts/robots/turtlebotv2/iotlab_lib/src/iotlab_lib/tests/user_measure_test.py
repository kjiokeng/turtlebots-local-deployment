# -*- coding:utf-8 -*-
"""Test iotlab_lib.user_measure module."""

import textwrap
import shutil
from datetime import datetime
from collections import namedtuple

import unittest

import mock
from geometry_msgs.msg import PoseStamped

from iotlab_lib import user_measure, helpers

OmlMeasure = namedtuple('measure', ['elapsed', 'schema', 'counter',
                                    's', 'us', 'x', 'y', 'theta'])


class TestRobotPosition(unittest.TestCase):
    """Test user_measure.RobotPosition."""
    TOPIC = 'measure_topic'
    HOSTNAME = 'm3-382'
    EXP_ID = '123'
    LOGDIR = '/tmp/test_iot-lab'
    T_0 = 1450257165

    def setUp(self):
        # Issues with 'mock'
        # pylint: disable=no-member
        self.subscriber_class = mock.patch('rospy.Subscriber').start()
        self.subscriber = self.subscriber_class.return_value

        # Mock datetime.utcnow
        # http://www.voidspace.org.uk/python/mock/examples.html#partial-mocking
        datetime_mock = mock.patch('datetime.datetime').start()
        datetime_mock.utcnow.return_value = datetime.utcfromtimestamp(self.T_0)
        datetime_mock.utcfromtimestamp.side_effect = datetime.utcfromtimestamp
        datetime_mock.side_effect = datetime

    def tearDown(self):
        mock.patch.stopall()
        try:
            shutil.rmtree(self.LOGDIR)
        except OSError:
            pass

    @staticmethod
    def _pose_stamped(secs, usecs, x, y, theta):  # pylint:disable=invalid-name
        """Retun a pose stambed object."""
        posestamped = PoseStamped(pose=helpers.pose(x, y, theta))
        posestamped.header.stamp.secs = int(secs)
        posestamped.header.stamp.nsecs = int(1000 * usecs)
        return posestamped

    def test_user_measure_header(self):
        """Verify measure file header content."""
        logfile = '{s.LOGDIR}/{s.EXP_ID}/robot/{s.HOSTNAME}_header.oml'
        logfile = logfile.format(s=self)

        position_log = user_measure.RobotPosition(self.TOPIC, logfile,
                                                  self.HOSTNAME, self.EXP_ID)
        position_log.start()
        position_log.stop()

        expected = textwrap.dedent('''\
        protocol: 4
        domain: 123
        start-time: 1450257165
        sender-id: m3-382
        app-name: robot_position_measures
        schema: 0 _experiment_metadata subject:string key:string value:string
        schema: 10 robot_pose timestamp_s:uint32 timestamp_us:uint32 x:double y:double theta:double
        content: text

        ''')  # noqa

        result = open(logfile).read()
        self.assertEqual(expected, result)

    def test_user_measure(self):
        """Verify measure file content."""
        logfile = '{s.LOGDIR}/{s.EXP_ID}/robot/{s.HOSTNAME}.oml'
        logfile = logfile.format(s=self)

        position_log = user_measure.RobotPosition(self.TOPIC, logfile,
                                                  self.HOSTNAME, self.EXP_ID)
        position_log.start()

        # Start sending measures
        pose_callback = self.subscriber_class.call_args[0][2]

        t_0 = position_log.datetime_timestamp_s(position_log.t_0)
        for i in range(0, 50):
            pose = self._pose_stamped(t_0 + 1 + i, 15000 * i,
                                      x=i, y=i,
                                      theta=3.20 * i / 50)
            pose_callback(pose)

        position_log.stop()

        result_list = open(logfile).read().splitlines()
        self.assertEqual(result_list[7], 'content: text')
        self.assertEqual(result_list[8], '')

        measure_list = [l.split('\t') for l in result_list[9:]]
        self.assertEqual(len(measure_list), 50)
        # verify measures
        for i, meas in enumerate(measure_list):
            self.assertEqual(len(meas), 8)
            meas = OmlMeasure(float(meas[0]), int(meas[1]), int(meas[2]),
                              int(meas[3]), int(meas[4]),
                              float(meas[5]), float(meas[6]), float(meas[7]))
            self.assertEqual(meas.schema,
                             user_measure.RobotPosition.ROBOT_OML_SCHEMA)
            self.assertEqual(meas.counter, i + 1)
            current_time = meas.s + (float(meas.us) / 1000000)
            self.assertEqual(current_time, meas.elapsed + t_0)

            # check x y
            self.assertEqual(meas.x, float(i))
            self.assertEqual(meas.y, float(i))
            # check theta
            self.assertTrue(meas.theta >= 0.0)

# -*- coding:utf-8 -*-

"""Test common stuff."""

import time


MOBILITY_DICT = {
    'type': 'predefined',
    'name': 'Jhall', 'site': 'grenoble',
    'points': ['a', 'b'],
    'coordinates': {
        'a': {'x': 0.0, 'y': 1.0, 'theta': 0.0, 'name': 'a'},
        'b': {'x': 0.1, 'y': 1.1, 'theta': 0.1, 'name': 'b'},
    },
    'loop': True,
}


SERVICE_SETUP_WAIT = 1
SERVICE_TEARDOWN_WAIT = 1


def assert_timeout(timeout, fct, *args, **kwargs):
    """ Wait at max `timeout` for `fct(*args, **kwargs)` to return True.
    :return: True if fct was True before timeout False otherwise.
    :rtype: bool
    """
    time_ref = time.time()
    while True:
        if fct(*args, **kwargs):
            return
        if time.time() > (time_ref + timeout):
            assert False, 'Timeout %d for fct %r' % (timeout, fct)
        time.sleep(0.1)

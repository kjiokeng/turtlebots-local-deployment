# -*- coding:utf-8 -*-
"""Test iotlab_lib.config module."""


import re
import unittest

from iotlab_lib import config


class TestConfig(unittest.TestCase):
    """Test config functions."""

    def test_node_hostname(self):
        """Test node hostname is m3-NUMBER."""
        hostname = config.node_hostname()
        self.assertTrue(re.match(r'm3-\d+$', hostname))

    def test_read_srveh_addr(self):
        """Test srveh address is an ip in 172.16 range."""
        addr = config.read_srveh_addr()
        self.assertTrue(re.match(r'172\.16\.\d+\.\d+$', addr))

    def test_version(self):
        """Test reading version."""
        version = config.robot_version()
        self.assertTrue(re.match(r'\d+(\.\d+)+$', version))

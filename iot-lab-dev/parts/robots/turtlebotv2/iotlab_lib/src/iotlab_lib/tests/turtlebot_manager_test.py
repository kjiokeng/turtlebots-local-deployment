# -*- coding:utf-8 -*-
"""Test iotlab_lib.turtlebot_manager module."""

import os
import shutil
import re
import unittest

from mock import patch, create_autospec

from iotlab_lib.tests import common
from iotlab_lib import turtlebot_manager, mobility, config

CUR_DIR = os.path.dirname(__file__)


class TestTurtlebotManager(unittest.TestCase):
    """Test manager experiment management."""

    SSH_KEYS = ['0\n', '1\n']
    USER = 'test_user'
    EXP_ID = 0

    USER_MEAS_DIR = config.MEAS_DIR_FMT.format(user=USER, exp_id=EXP_ID)

    MOBILITY_DICT = common.MOBILITY_DICT.copy()

    STATUS = {
        'x': 21.7100000000005,
        'kobuki_battery_source': 'Dock',
        'laptop_battery_status': 'OK',
        'kobuki_percentage': '100',
        'robot_battery_status': 'Healthy',
        'robot_state': 'DOCKED',
        'y': -0.3000000000000188,
        'battery_state': 'LOAD',
        'laptop_percentage': 98,
        'theta': 0.0,
        'bookable': True,
    }

    POSITION = {
        'y': -0.30000000000001875,
        'x': 21.7100000000005,
        'theta': 0.0003490658503986222,
    }

    MEASURE_DIR = os.path.join(CUR_DIR, 'user_measure')

    def setUp(self):
        # Strange patching to make Pylint recognize classes

        # user_measure
        self.user_measure = create_autospec(
            turtlebot_manager.user_measure.RobotPosition)
        self.user_meas_cls = patch(
            'iotlab_lib.turtlebot_manager.user_measure.RobotPosition').start()
        self.user_meas_cls.return_value = self.user_measure

        # Turtlebot
        self.robot = create_autospec(turtlebot_manager.turtlebot.Turtlebot)
        robot_cls = patch(
            'iotlab_lib.turtlebot_manager.turtlebot.Turtlebot').start()
        robot_cls.return_value = self.robot

        # Mobility
        self.mobility = create_autospec(
            turtlebot_manager.mobility_manager.MobilityManager)
        mobility_cls = patch(
            'iotlab_lib.turtlebot_manager.mobility_manager.MobilityManager'
        ).start()
        mobility_cls.return_value = self.mobility

        self.mobility.start.return_value = 0
        self.mobility.update.return_value = 0

        # UserEnv
        self.user_env = create_autospec(turtlebot_manager.user_env.UserEnv)
        user_env_cls = patch(
            'iotlab_lib.turtlebot_manager.user_env.UserEnv').start()
        user_env_cls.return_value = self.user_env

        self.user_env.start.return_value = 0
        self.user_env.stop.return_value = 0

        # Create manager
        self.manager = turtlebot_manager.TurtlebotManager()

        # Clean measure files
        shutil.rmtree(self.USER_MEAS_DIR, ignore_errors=True)

    def tearDown(self):
        patch.stopall()

    def test_class_init(self):
        """Test the class variable values."""
        hostname = self.manager.NODE_HOSTNAME
        self.assertTrue(re.match(r'm3-\d+$', hostname))

        version = self.manager.ROBOT_VERSION
        self.assertTrue(re.match(r'\d+(\.\d+)+$', version))

    def test_experiment_mobility(self):
        """Test an experiment without mobility."""
        ret = self.manager.exp_start(self.USER, self.EXP_ID, self.SSH_KEYS,
                                     self.MOBILITY_DICT, robot_user_env=True)
        # Start OK
        self.assertEqual(ret, 0)
        self.assertTrue(self.mobility.start.called)

        # check position log call args
        log_file = self.user_meas_cls.call_args[0][1]
        self.assertTrue(re.match(
            r'/iotlab/users/test_user/.iot-lab/0/robot/m3-\d+.oml', log_file))

        self.assertTrue(self.user_env.setup.called)
        self.assertTrue(self._mobility_updated_with_mobility())
        self.assertTrue(self.user_env.start.called)

        # Exp mobility during experiment
        self.mobility.reset_mock()
        ret = self.manager.exp_update_mobility(self.MOBILITY_DICT)
        self.assertEqual(ret, 0)
        self.assertTrue(self._mobility_updated_with_mobility())

        # Start user env
        self.user_env.start.reset_mock()
        ret = self.manager.exp_user_env_start()
        self.assertEqual(ret, 0)
        self.assertTrue(self.user_env.start.called)

        # Stop user env
        self.user_env.stop.reset_mock()
        ret = self.manager.exp_user_env_stop()
        self.assertEqual(ret, 0)
        self.assertTrue(self.user_env.stop.called)

        # Point reachable
        self.robot.is_reachable.return_value = True
        reachable = self.manager.is_reachable(0, 0)
        self.assertEqual(reachable, True)
        self.assertTrue(self.robot.is_reachable.called)

        # Exp go_to_point during experiment
        self.mobility.reset_mock()
        self.user_env.start.reset_mock()
        ret = self.manager.exp_go_to_point(0, 0, 0.0, 'point_a')
        self.assertEqual(ret, 0)
        self.assertTrue(self._mobility_updated_with_mobility())
        self.assertFalse(self.user_env.start.called)

        # Get status
        self.robot.status.return_value = self.STATUS
        ret_dict = self.manager.status()
        self.assertTrue(self.robot.status.called)
        self.assertEqual(ret_dict, self.STATUS)

        # Get position
        self.robot.position_dict.return_value = self.POSITION
        ret_dict = self.manager.position()
        self.assertTrue(self.robot.position_dict.called)
        self.assertEqual(ret_dict, self.POSITION)

        # Get version
        ret_version = self.manager.robot_version()
        self.assertEqual(ret_version, '2.0\n')

        ret = self.manager.exp_stop()
        # Stop ok
        self.assertEqual(ret, 0)
        self.assertTrue(self.user_env.stop.called)
        self.assertTrue(self.user_env.teardown.called)
        self.assertTrue(self.mobility.stop_async.called)
        self.assertTrue(self.user_measure.stop.called)

    def _mobility_updated_with_mobility(self):
        """True if mobility.update was called with a Mobility object."""
        ret = True
        ret &= self.mobility.update.called

        mob = self.mobility.update.call_args[0][0]
        ret &= mob is not None
        ret &= isinstance(mob, mobility.Mobility)
        return ret

    def test_experiment_no_mobility(self):
        """Test an experiment without mobility."""

        ret = self.manager.exp_start(self.USER, self.EXP_ID, self.SSH_KEYS)

        # Start OK
        self.assertEqual(ret, 0)
        self.assertTrue(self.mobility.start.called)
        self.assertTrue(self.user_env.setup.called)
        self.mobility.update.assert_called_with(None)
        self.assertFalse(self.user_env.start.called)

        self.mobility.dock.return_value = 0
        ret = self.manager.exp_go_to_dock()
        # go_to_dock ok
        self.assertEqual(ret, 0)
        self.assertTrue(self.mobility.dock.called)

        ret = self.manager.exp_stop()
        # Stop ok
        self.assertEqual(ret, 0)
        self.assertTrue(self.user_env.stop.called)
        self.assertTrue(self.user_env.teardown.called)
        self.assertTrue(self.mobility.stop_async.called)
        self.assertTrue(self.user_measure.stop.called)

    def test_experiment_stop_standalone(self):
        """Test stop can be called without start."""
        ret = self.manager.exp_stop()
        self.assertEqual(ret, 0)

    def test_exp_start_invalid_mobility(self):
        """Test start experiment with invalid mobility."""
        inval_mob = {'no_valid_data': 1}

        ret = self.manager.exp_start(self.USER, self.EXP_ID, self.SSH_KEYS,
                                     inval_mob)
        self.assertEqual(ret, 1)
        self.assertFalse(self.user_measure.called)
        self.assertFalse(self.mobility.start.called)
        self.assertFalse(self.user_env.setup.called)
        self.assertFalse(self.mobility.update.called)
        self.assertFalse(self.user_env.start.called)

    def test_exp_start_mobility_fail(self):
        """Test start experiment with mobility start failed."""

        self.mobility.start.return_value = 42

        ret = self.manager.exp_start(self.USER, self.EXP_ID, self.SSH_KEYS)
        self.assertEqual(ret, 42)
        self.assertTrue(self.user_measure.start.called)
        self.assertTrue(self.mobility.start.called)

        # Stopped in the middle
        self.assertFalse(self.user_env.setup.called)
        self.assertFalse(self.mobility.update.called)
        self.assertFalse(self.user_env.start.called)

    def test_update_mobility_invalid(self):
        """Test update mobility with invalid mobility."""
        inval_mob = {'no_valid_data': 1}

        ret = self.manager.exp_update_mobility(inval_mob)
        self.assertEqual(ret, 1)
        self.assertFalse(self.mobility.update.called)
        self.assertFalse(self.user_env.start.called)

    def test_go_to_point_inval(self):
        """Test go_to_point with invalid mobility."""

        ret = self.manager.exp_go_to_point('x_0', 'x_1', 'theta_12', 'point_a')
        self.assertEqual(ret, 1)
        self.assertFalse(self.mobility.called)
        self.assertFalse(self.user_env.start.called)

    def test_is_reachable_inval(self):
        """Test is_reachable with invalid mobility."""

        ret = self.manager.is_reachable('x_0', 'x_1')
        self.assertEqual(ret, False)
        self.assertFalse(self.robot.is_reachable.called)

    def test_exp_results_del_no_files(self):
        """Test experiment_results_del without files."""
        self.assertFalse(os.path.exists(self.USER_MEAS_DIR))

        # Call 'exp_results_del' without files, should be no errors
        ret = self.manager.exp_results_del(self.USER, self.EXP_ID)
        self.assertEqual(ret, 0)

        self.assertFalse(os.path.exists(self.USER_MEAS_DIR))

    def test_exp_results(self):
        """Test experiment_results_get and delete."""
        # Tests measures files are configured for m3-19 hostname
        self.manager.NODE_HOSTNAME = 'm3-19'

        # Copy files
        shutil.copytree(self.MEASURE_DIR, self.USER_MEAS_DIR)
        ref_meas_file = os.path.join(self.USER_MEAS_DIR, 'robot',
                                     '%s.oml' % self.manager.NODE_HOSTNAME)
        meas_file_content = open(ref_meas_file).read()

        # Call 'exp_results_get'
        archive = self.manager.exp_results_get(self.USER, self.EXP_ID)
        self.assertTrue(archive.endswith('.tar.gz'))

        with turtlebot_manager.tarfile.open(archive) as arch:
            files = arch.getnames()
            # directory, directory/type, oml_file
            self.assertEqual(len(files), 3)

            # last file should be oml file
            oml_file = files[-1]
            self.assertTrue(oml_file.endswith('.oml'))

            # Verify OML content
            content = arch.extractfile(oml_file).read()
            self.assertEqual(content, meas_file_content)

        self.assertTrue(os.path.exists(self.USER_MEAS_DIR))

        # delete
        ret = self.manager.exp_results_del(self.USER, self.EXP_ID)
        self.assertEqual(ret, 0)
        self.assertFalse(os.path.exists(self.USER_MEAS_DIR))

    # Admin methods
    def test_admin_dock_undock(self):
        """Test admin dock and undock methods."""
        # pylint:disable=protected-access
        self.robot._dock.return_value = 0
        ret = self.manager.admin_dock()
        self.assertEqual(ret, 0)
        self.assertTrue(self.robot._dock.called)

        self.robot.undock.return_value = 0
        ret = self.manager.admin_undock()
        self.assertEqual(ret, 0)
        self.assertTrue(self.robot.undock.called)

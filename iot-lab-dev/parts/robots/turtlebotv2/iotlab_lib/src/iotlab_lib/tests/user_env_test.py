# -*- coding:utf-8 -*-
"""Test user_env."""


import os
import time
import subprocess
import unittest

from iotlab_lib import config, user_env


class TestUserEnv(unittest.TestCase):
    """UserEnv class integration tests."""
    SSH_KEYS = [open(os.path.expanduser('~/.ssh/id_rsa.pub')).read()]
    PORT = str(user_env.UserEnv.LOCAL_SSH_PORT)

    def setUp(self):
        self.user_env = user_env.UserEnv(config.DOCKER0_ADDR,
                                         config.USER_ENV_FOLDER)
        self.user_env.stop()
        self.user_env.teardown()

    def tearDown(self):
        self.user_env.stop()
        self.user_env.teardown()

    def _ssh_cmd(self, cmd, num_try=1, delay=1, stderr=None):
        """Try exec ssh command 'num_try' times."""
        ssh_cmd = ['ssh', '-oStrictHostKeyChecking=no', '-p', self.PORT,
                   'root@localhost', cmd]
        ssh_cmd = ['timeout', '5'] + ssh_cmd

        for _ in range(0, num_try - 1):
            try:
                return subprocess.check_output(ssh_cmd, stderr=stderr)
            except subprocess.CalledProcessError:
                time.sleep(delay)

        # raise interrupt here
        return subprocess.check_output(ssh_cmd, stderr=stderr)

    def test_start_and_access(self):
        """Test starting docker container and using it."""

        self.assertRaises(subprocess.CalledProcessError,
                          self._ssh_cmd, 'hostname', stderr=subprocess.STDOUT)

        # Setup ssh keys
        ret = self.user_env.setup(self.SSH_KEYS)
        self.assertEqual(ret, 0)

        # Create environment
        ret = self.user_env.start()
        self.assertEqual(ret, 0)

        # Multiple start don't break
        ret = self.user_env.start()
        self.assertEqual(ret, 0)

        # Exec hostname
        out = self._ssh_cmd('hostname', num_try=5)
        self.assertEqual(out, 'userenv\n')

        # Ping robot
        self._ssh_cmd('ping -c 1 robot', num_try=5)  # No exception

    def test_stop_not_started(self):
        """Test stoping user_env not started."""
        ret = self.user_env.stop()
        self.assertEqual(ret, 0)

        ret = self.user_env.teardown()
        self.assertEqual(ret, 0)

# -*- coding:utf-8 -*-
"""Test iotlab_lib.turtlebot_leds module."""

import unittest

import rospy
from kobuki_msgs.msg import Led as _Led

from .common import assert_timeout, SERVICE_SETUP_WAIT, SERVICE_TEARDOWN_WAIT
from .. import turtlebot_leds


class TestTurtlebotLed(unittest.TestCase):
    """Test turtlebot Led."""

    def setUp(self):
        rospy.init_node('iotlab_test', anonymous=True)

        self.led = turtlebot_leds.Led(1)
        self.color = None

        topic = turtlebot_leds.Led.LED_TOPIC_FMT % 1
        self.led_topic = rospy.Subscriber(topic, _Led, self._save_color)
        rospy.sleep(SERVICE_SETUP_WAIT)

    def tearDown(self):
        self.led.color = 'BLACK'
        self.led._shutdown()  # pylint:disable=protected-access
        rospy.sleep(SERVICE_TEARDOWN_WAIT)

    def _save_color(self, color):
        self.color = color

    def _color_topic_equal(self, color):
        """Assert color is equal with timeout."""
        assert_timeout(1, lambda: self.color == color)

    def test_set_color(self):
        """Test Led.color."""
        # Color was verified with my eyes too :D

        self.led.color = 'RED'
        self._color_topic_equal(_Led(_Led.RED))
        self.assertEqual(self.led.color, 'RED')

        self.led.color = 'ORANGE'
        self._color_topic_equal(_Led(_Led.ORANGE))
        self.assertEqual(self.led.color, 'ORANGE')

        try:
            self.led.color = 'WHITE'
            self.fail()
        except ValueError:
            # Previous value
            self.assertEqual(self.led.color, 'ORANGE')

    def test_led_service(self):
        """Test led service."""
        led_srv = turtlebot_leds.Led.LED_SRV_FMT % 1
        rospy.wait_for_service(led_srv)
        set_led = rospy.ServiceProxy(led_srv, turtlebot_leds.KobukiLed)

        # Set led
        req = set_led('GREEN')
        self._color_topic_equal(_Led(_Led.GREEN))
        self.assertEqual(self.led.color, 'GREEN')
        self.assertEqual(req.Led_Response, 'LED1 [DONE]')

        # Invalid color
        req = set_led('CHARTREUSE')
        self.assertEqual(req.Led_Response, 'LED1 [ERROR]')
        rospy.sleep(1)
        self.assertNotEqual(self.led.color, 'CHARTREUSE')

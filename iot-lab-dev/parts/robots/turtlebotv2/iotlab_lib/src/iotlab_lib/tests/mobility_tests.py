# -*- coding:utf-8 -*-
"""Test iotlab_lib.helpers module."""

# pylint:disable=protected-access

import unittest

from iotlab_lib import mobility
from .helpers_test import assertPoseEqual


class TestMobility(unittest.TestCase):
    """Test creating mobility and using it."""

    def setUp(self):
        self.points = ['a', 'b', 'c', 'a']
        self.coords = {
            'a': {'x': 0.0, 'y': 1.0},
            'b': {'x': 0.1, 'y': 1.1, 'theta': 0.1, 'name': 'b'},
            'c': {'x': 0.2, 'y': 1.2, 'theta': 0.2},
        }

    def _assert_equal_mob(self, mob1, mob2):
        """Compare mobilities."""
        points1 = []
        points2 = []
        for _ in range(0, 10):
            points1.append(mob1.next())
            points2.append(mob2.next())

        self.assertEqual(points1, points2)

        # iterator can't be compared with dict
        mob1._goals_iter = None
        mob2._goals_iter = None
        self.assertEqual(mob1.__dict__, mob2.__dict__)

    def test_create_mobility_circuit(self):
        """Create a mobility with a circuit."""
        mob = mobility.Mobility('predefined', self.points,
                                self.coords, loop=True)

        # Do circuit
        p_list = [mob.next() for _ in range(0, 8)]

        # Verify values (name, value)
        self.assertEqual(p_list[0][0], 'a')
        assertPoseEqual(self, p_list[0][1], 0.0, 1.0)

        self.assertEqual(p_list[1][0], 'b')
        assertPoseEqual(self, p_list[1][1], 0.1, 1.1, 0.1)

        self.assertEqual(p_list[2][0], 'c')
        assertPoseEqual(self, p_list[2][1], 0.2, 1.2, 0.2)

        # Circuit is looping
        self.assertEqual(p_list[0], p_list[3])
        self.assertEqual(p_list[0], p_list[4])
        self.assertEqual(p_list[0], p_list[7])
        self.assertEqual(p_list[1], p_list[5])
        self.assertEqual(p_list[2], p_list[6])

    def test_mobility_from_dict(self):
        """Compare mobility with mobility from dict."""
        mob = mobility.Mobility('userdefined', self.points,
                                self.coords, loop=True)

        # Compare with 'from_dict'
        mobility_dict = {
            'type': 'userdefined',
            'points': list(self.points),
            'coordinates': self.coords.copy(),
            'loop': True,
        }
        mob_from_dict = mobility.Mobility.from_dict(mobility_dict)
        self._assert_equal_mob(mob, mob_from_dict)

        # Test 'None case
        self.assertIsNone(mobility.Mobility.from_dict(None))

    def test_mobility_from_dict_error(self):
        """Try creating a mobility dict with an invalid dict."""
        mobility_dict = {'invali': None}
        self.assertRaises(ValueError,
                          mobility.Mobility.from_dict, mobility_dict)

        # Missing 'points' and 'coordinates'
        mobility_dict = {'type': 'userdefined'}
        self.assertRaises(ValueError,
                          mobility.Mobility.from_dict, mobility_dict)

    def test_invalid_point(self):
        """Try creating a mobility with an invalid point."""
        self.assertRaises(ValueError, mobility.Mobility,
                          'predefined', ['a'], {'a': {'x': 'x_0', 'y': 'y_0'}})

    def test_mobility_traj(self):
        """Create a mobility with a trajectory that don't loop."""
        mob = mobility.Mobility('predefined', self.points, self.coords,
                                loop=False)
        # Do circuit
        i = 0
        for point in mob:  # don't use enumerate, we want to call __iter__
            i += 1
            self.assertIsNotNone(point)
        self.assertEqual(i, 4)

        # Not looping
        for _ in range(i, i + 4):
            self.assertRaises(StopIteration, mob.next)

    def test_mobility_point(self):
        """Test Mobility 'stop'."""
        mob = mobility.Mobility.from_point(1.1, 2.2, 3.0)
        self.assertEqual(mob._type, 'point')

        point = mob.next()
        self.assertIsNotNone(point)
        # Verify point
        self.assertEqual(point[0], 'point')  # Default
        assertPoseEqual(self, point[1], 1.1, 2.2, 3.0)

        # Second should be None
        self.assertRaises(StopIteration, mob.next)

    def test_mobility_stop(self):
        """Test Mobility 'stop'."""
        mob = mobility.Mobility.from_stop()
        self.assertEqual(mob._type, 'stop')
        self.assertRaises(StopIteration, mob.next)

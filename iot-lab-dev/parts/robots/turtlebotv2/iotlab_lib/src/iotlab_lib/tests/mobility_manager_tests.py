# -*- coding:utf-8 -*-
"""Test iotlab_lib.turtlebot_manager module."""

import time
import threading
import unittest

import mock
from mock import Mock, create_autospec

from iotlab_msgs.msg import RobotState, RobotPower
from iotlab_lib import turtlebot, mobility_manager, mobility
from .common import assert_timeout


class TestMobilityManager(unittest.TestCase):
    """Test MobilityManager."""

    STEPS = 1.0

    def setUp(self):

        self.robot = self.robot_mock()
        self.manager = mobility_manager.MobilityManager(self.robot)

        self._set_robot_state('DOCKED')

    def tearDown(self):
        self.manager.stop_async()
        self.manager.wait_stopped(10)

    def robot_mock(self):
        """Create a mock robot object."""
        robot = create_autospec(turtlebot.Turtlebot)
        robot.STATE_TOPIC = turtlebot.Turtlebot.STATE_TOPIC

        robot.wheels = Mock()
        robot.power = Mock()
        robot.battery = 'high'
        robot.nav = Mock()

        robot.undock.return_value = 0
        robot.undock.side_effect = self._undock

        robot.go_to_dock.return_value = 0
        robot.go_to_dock.side_effect = self._go_to_dock

        robot.dock.return_value = 0
        robot.dock.side_effect = self._dock

        robot.go_to_pose.return_value = 0
        robot.go_to_pose.side_effect = self._go_to_pose

        return robot

    # Robot mocks
    def _undock(self):
        self._set_robot_state('UNDOCKING')
        time.sleep(self.STEPS)
        self._set_robot_state('WAIT_POINT')
        return mock.DEFAULT

    def _go_to_dock(self):
        self._set_robot_state('GOING_DOCK')
        time.sleep(self.STEPS)
        self._set_robot_state('NEAR_DOCK')
        return mock.DEFAULT

    def _dock(self):
        self._set_robot_state('DOCKING')
        threading.Timer(self.STEPS, self._set_robot_state,
                        args=['DOCKED']).start()
        return mock.DEFAULT

    def _go_to_pose(self, pose, timeout=300):  # pylint:disable=unused-argument
        self._set_robot_state('GOING_POINT')
        time.sleep(self.STEPS)
        self._set_robot_state('WAIT_POINT')
        return mock.DEFAULT

    def _set_robot_state(self, state):
        if self.robot.state == 'DOCKED':
            self.robot.power.source = 'Dock'
        else:
            self.robot.power.source = 'Battery'

        self.robot.state = state
        self.manager._robot_state_cb(  # pylint:disable=protected-access
            RobotState(state))

    def _set_robot_battery(self, level):
        self.robot.battery = level
        power = RobotPower(battery_level=level,
                           power_source=self.robot.power.source)
        self.manager._robot_power_cb(power)  # pylint:disable=protected-access

    def _assert_num_go_to_pose(self, num):
        """Wait until go_to_pose called `num` times only.

        Ensures there are no more goals and call count == `num`."""
        assert_timeout(20, lambda: self.robot.go_to_pose.call_count >= num)
        assert_timeout(10, lambda: self.robot.state == 'WAIT_POINT')
        self.assertEqual(self.robot.go_to_pose.call_count, num)
        self.assertTrue(not self.manager.has_goal())

    def test_complete_experiment(self):
        """Test complete experiment.

        + trajectory
        + update, circuit with loop
        + no more battery
        + battery reloaded
        + circuit starts again
        """
        # TODO maybe verify called points...
        trajectory = mobility.Mobility('userdefined', ['a', 'b', 'c'],
                                       {'a': {'x': 0.0, 'y': 0.0},
                                        'b': {'x': 1.0, 'y': 1.0},
                                        'c': {'x': 2.0, 'y': 2.0}},
                                       loop=False)

        circuit = mobility.Mobility('predefined', ['1', '2', '3'],
                                    {'1': {'x': 10.0, 'y': 10.0},
                                     '2': {'x': 20.0, 'y': 20.0},
                                     '3': {'x': 30.0, 'y': 30.0}},
                                    loop=True)

        ret = self.manager.start()
        self.assertEqual(ret, 0)

        # Update trajectory
        ret = self.manager.update(trajectory)
        self.assertEqual(ret, 0)

        # Wait until no more points
        self._assert_num_go_to_pose(3)

        self.assertEqual(3, self.robot.go_to_pose.call_count)
        self.assertEqual(0, self.robot.go_to_dock.call_count)
        self.robot.go_to_pose.reset_mock()

        # Update circuit
        ret = self.manager.update(circuit)
        self.assertEqual(ret, 0)
        # Wait few points
        assert_timeout(20, lambda: self.robot.go_to_pose.call_count > 5)
        self.assertEqual(0, self.robot.go_to_dock.call_count)

        # Battery 'medium'
        self._set_robot_battery('medium')
        self.robot.go_to_pose.reset_mock()

        # keeps running with battery medium
        assert_timeout(20, lambda: self.robot.go_to_pose.call_count > 5)
        self.assertEqual(0, self.robot.go_to_dock.call_count)

        # Battery 'low'
        self._set_robot_battery('low')
        assert_timeout(5, lambda: self.robot.state not in ('WAIT_POINT',
                                                           'GOING_POINT'))
        self.robot.go_to_pose.reset_mock()

        # Wait docked
        assert_timeout(5, lambda: self.robot.state == 'DOCKED')
        self.assertEqual(0, self.robot.go_to_pose.call_count)
        self.assertEqual(1, self.robot.go_to_dock.call_count)
        self.robot.go_to_dock.reset_mock()

        # Battery 'medium'
        self._set_robot_battery('medium')
        time.sleep(5)
        self.assertEqual(0, self.robot.go_to_pose.call_count)
        # Not going out

        # Battery 'high' goes back in circuit
        self._set_robot_battery('high')
        time.sleep(5)
        assert_timeout(20, lambda: self.robot.go_to_pose.call_count > 5)

        ret = self.manager.stop_async()
        self.assertEqual(ret, 0)
        self.manager.wait_stopped(10)

    def test_go_to_point(self):
        """Test update with a point Mobility."""
        ret = self.manager.start()
        self.assertEqual(ret, 0)

        mob = mobility.Mobility.from_point(0, 0, name='toto')
        ret = self.manager.update(mob)
        self.assertEqual(ret, 0)

        # Wait until no more points
        self._assert_num_go_to_pose(1)

        # Go to dock
        self.assertEqual(0, self.robot.go_to_dock.call_count)
        ret = self.manager.dock()
        self.assertEqual(ret, 0)
        assert_timeout(10, lambda: self.robot.state == 'DOCKED')
        self.assertEqual(1, self.robot.go_to_dock.call_count)

        ret = self.manager.stop_async()
        self.assertEqual(ret, 0)
        self.manager.wait_stopped(10)

        # Circuit only one point
        self.assertEqual(1, self.robot.go_to_pose.call_count)
        self.assertEqual(1, self.robot.go_to_dock.call_count)

    def test_no_mobility(self):
        """Test experiment without mobility."""
        ret = self.manager.start()
        self.assertEqual(ret, 0)

        ret = self.manager.stop_async()
        self.assertEqual(ret, 0)
        self.manager.wait_stopped(10)

        # Stayed in dock
        self.assertEqual(0, self.robot.undock.call_count)
        self.assertEqual(0, self.robot.dock.call_count)

    def test_start_invalid_state(self):
        """Test starting with an invalid state."""
        self._set_robot_state('INIT')

        ret = self.manager.start()
        self.assertEqual(ret, 1)

    def test_start_error_in_loop(self):
        """Test an error in manager loop."""

        ret = self.manager.start()
        self.assertEqual(ret, 0)
        time.sleep(1)
        self._set_robot_state('INIT')
        time.sleep(2)
        self.assertFalse(self.manager.is_alive())

    def test_update_not_started(self):
        """Test update without mobility running."""

        # update allowed without mobility
        ret = self.manager.update(None)
        self.assertEqual(ret, 0)

        # Update not allowed with any mobility
        mob = mobility.Mobility.from_stop()
        ret = self.manager.update(mob)
        self.assertEqual(ret, 1)

    def test_dock_not_started(self):
        """Test dock without mobility running."""
        ret = self.manager.dock()
        self.assertEqual(ret, 1)

# -*- coding:utf-8 -*-
# This file is a part of IoT-LAB robots
# Copyright (C) 2015 Univ. Strasbourg, INRIA (Contact: admin@iot-lab.info)
# Contributor(s) : see AUTHORS file
#
# This software is governed by the CeCILL license under French law
# and abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# http://www.cecill.info.
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Turtlebot.navigation module.

Manage ROS navigation.
"""

import logging
from copy import deepcopy

import rospy
import actionlib
from rocon_std_msgs.msg import KeyValue

from nav_msgs.srv import GetPlan
from actionlib_msgs.msg import GoalStatus
from move_base_msgs.msg import MoveBaseGoal, MoveBaseAction
from geometry_msgs.msg import Pose, PoseStamped

from iotlab_lib import helpers, rapp


LOGGER = logging.getLogger('iotlab_lib')


class Navigation(object):
    """Manage ROS navigation.

    :param initial_pose: Robot initial pose
    :type initial_pose: Pose
    """
    TURTLEBOT_NAME = rospy.get_param('/iotlab/turtlebot_name')

    MOVEBASE = '/{0}/move_base'.format(TURTLEBOT_NAME)
    MOVEBASE_MAKE_PLAN = '/{0}/move_base/make_plan'.format(TURTLEBOT_NAME)
    LAST_NAV_SERVICE = '/{0}/move_base/set_parameters'.format(TURTLEBOT_NAME)

    # map on GoalStatus
    GOAL_STATUS_STR = ('PENDING', 'ACTIVE', 'PREEMPTED', 'SUCCEEDED',
                       'ABORTED', 'REJECTED', 'PREEMPTING', 'RECALLING',
                       'RECALLED', 'LOST')

    def __init__(self, pose_topic, initial_pose):
        self.pose = Pose()

        parameters = self._nav_initial_pose_parameters(initial_pose)

        self.rapp = rapp.Rapp('iotlab_turtlebot/map_nav',
                              parameters=parameters)

        # Publishers
        self.movebase = actionlib.SimpleActionClient(self.MOVEBASE,
                                                     MoveBaseAction)
        # Subscribers
        rospy.Subscriber(pose_topic, PoseStamped, self._pose_cb)

    def position_dict(self):
        """Return current position in a dict with x, y, theta."""
        position = {
            'x': self.pose.position.x,
            'y': self.pose.position.y,
            'theta': helpers.theta_from_quaternion(self.pose.orientation),
        }
        return position

    def start(self):
        """Start navigation Rapp."""
        LOGGER.info('Navigation.start')

        ret = self.rapp.start()
        if ret:
            return ret

        self._wait_ready()
        LOGGER.info('Navigation.start: started')
        return 0

    def _wait_ready(self, timeout=30):
        """Wait navigation rapp started."""
        LOGGER.debug('Navigation._wait_ready: Wait last service started')
        rospy.wait_for_service(self.LAST_NAV_SERVICE, timeout=timeout)
        # Wait a bit more, was not enough... costmap is not ready
        LOGGER.info('Navigation.start: Waiting 10 more seconds')
        rospy.sleep(10)

    @staticmethod
    def _nav_initial_pose_parameters(initial_pose):
        """Return nav rapp start parameters that:

        * Set initial pose
        """
        theta = helpers.theta_from_quaternion(initial_pose.orientation)
        parameters = [
            KeyValue('initial_pose_x', str(initial_pose.position.x)),
            KeyValue('initial_pose_y', str(initial_pose.position.y)),
            KeyValue('initial_pose_a', str(theta)),
        ]
        return parameters

    def stop(self):
        """Stop Navigation Rapp."""
        LOGGER.info('Navigation.stop')
        ret = self.rapp.stop()
        if ret:
            return ret

        # Wait stopped, hack
        rospy.sleep(10)
        return 0

    def go_to_pose(self, goal, timeout=300):
        """Go to `goal` in `timeout`."""
        assert isinstance(goal, Pose), 'Should be Pose object'
        timeout = rospy.Duration(timeout)

        ros_goal = self.movebasegoal_from_pose(goal)
        self.movebase.wait_for_server()
        self.movebase.send_goal(ros_goal)
        result = 0

        LOGGER.debug('Turtlebot.go_to_pose: x=%s y=%s',
                     goal.position.x, goal.position.y)

        # Allow 5 minutes to get there
        finished_within_time = self.movebase.wait_for_result(timeout)
        self.movebase.wait_for_result()

        if self.movebase.get_state() == GoalStatus.SUCCEEDED:
            LOGGER.debug("Turtlebot.go_to_pose: ok")
            result = 0
        else:
            LOGGER.error("Turtlebot.go_to_pose: could not execute goal")
            result = 1
        if not finished_within_time:
            self.movebase.cancel_goal()
            LOGGER.debug("Turtlebot.go_to_pose: goal timed out")
            result = 2
        else:
            state = self.movebase.get_state()
            if state == GoalStatus.SUCCEEDED:
                LOGGER.debug("Turtlebot.go_to_pose: goal succeeded")
            else:
                LOGGER.error("Turtlebot.go_to_pose: failed with error code:%s",
                             self.GOAL_STATUS_STR[state])

        return result

    @staticmethod
    def movebasegoal_from_pose(pose):
        """Create MoveBaseGoal from position."""
        goal = MoveBaseGoal()
        goal.target_pose.header.frame_id = 'map'
        goal.target_pose.header.stamp = rospy.Time.now()
        goal.target_pose.pose = deepcopy(pose)
        return goal

    def is_reachable(self, pose):  # pylint:disable=invalid-name
        """ Checks if the point (x,y) is in a empty space in the map """
        LOGGER.debug("Trying to find a plan to (%s, %s)",
                     pose.position.x, pose.position.y)

        rospy.wait_for_service(self.MOVEBASE_MAKE_PLAN, timeout=5)
        make_plan = rospy.ServiceProxy(self.MOVEBASE_MAKE_PLAN, GetPlan)

        # Convert iotlab to ROS
        start = PoseStamped(pose=deepcopy(self.pose))
        start.header.frame_id = 'map'
        target = PoseStamped(pose=deepcopy(pose))
        target.header.frame_id = 'map'

        # TODO will fail when robot is moving...
        # http://answers.ros.org/question/49394/make_plan-service-call-fail/
        response = make_plan(start=start, goal=target, tolerance=0.0)

        reachable = bool(response.plan.poses)
        return reachable

    # # # # # # #
    # Callbacks #
    # # # # # # #

    def _pose_cb(self, pose_stamped):
        """Update robot position.

        :type pose_stamped: PoseStamped
        """
        self.pose = deepcopy(pose_stamped.pose)

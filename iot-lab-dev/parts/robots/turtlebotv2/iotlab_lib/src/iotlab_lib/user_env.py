# -*- coding:utf-8 -*-
"""User docker container management module."""

import os
import subprocess
import logging

LOGGER = logging.getLogger('iotlab_lib')


class UserEnv(object):
    """Create user container.

    :param ip_addr: ip address for the container
    :param user_env_folder: user environment directory
    """
    FNULL = open(os.devnull, 'w')

    NAME = 'userenv'
    IMAGE = 'iotlab/user_env'
    LOCAL_SSH_PORT = 2223
    PORT_MAPPING = '%d:22' % LOCAL_SSH_PORT
    HOST_ADD = 'robot:{ip_addr}'

    def __init__(self, ip_addr, user_env_folder):
        self.ip_addr = ip_addr
        self.user_env_folder = user_env_folder
        self.running = False

    def setup(self, ssh_keys):
        """Setup container.

        Init ssh authorized keys file
        """
        self._authorized_keys_write(ssh_keys)
        return 0

    def teardown(self):
        """Teardown container.

        Clear ssh authorized keys
        """
        self._authorized_keys_remove()
        return 0

    def start(self):
        """Start user container."""
        if self.running:
            return 0

        # Remove container if it still exists
        self._stop()

        ret = 0
        ret += self._build()
        ret += self._start()
        self.running = True
        return 0

    def stop(self):
        """Stop user container."""
        # Remove container
        self._stop()
        self.running = False
        return 0

    def _build(self):
        """Build docker image."""
        LOGGER.debug('Building Docker image…')
        cmd = ['docker', 'build', '-t', self.IMAGE, self.user_env_folder]
        return abs(subprocess.call(cmd))

    def _start(self):
        """Start docker container."""
        host_add = self.HOST_ADD.format(ip_addr=self.ip_addr)
        LOGGER.debug('Starting container…')

        cmd = [
            'docker', 'run', '-d',
            '-p', self.PORT_MAPPING,
            '-h', self.NAME,
            '--name', self.NAME,
            '--add-host', host_add,
            self.IMAGE
        ]
        return abs(subprocess.call(cmd))

    def _stop(self):
        """Stop docker container."""
        # Hide errors
        subprocess.call(['docker', 'rm', '-f', self.NAME], stderr=self.FNULL)
        return 0

    def _authorized_keys_write(self, ssh_keys):
        """Write user ssh keys to authorized_keys."""
        # Write to 'dockerfile settings'
        authorized_keys = self._authorized_keys_path()

        # Write all keys
        with open(authorized_keys, 'w+') as auth_file:
            for key in ssh_keys:
                auth_file.write('%s\n' % key)

    def _authorized_keys_remove(self):
        """Trim authorized keys file."""
        authorized_keys = self._authorized_keys_path()
        open(authorized_keys, 'w+').close()  # Truncate

    def _authorized_keys_path(self):
        """Path to dockerfile settings authorized_keys path."""
        return os.path.join(self.user_env_folder, 'authorized_keys')


def _main():  # pragma: no cover
    """Start container, wait for Ctrl+C, and quit."""
    import signal
    from iotlab_lib import config

    ssh_keys = [open(os.path.expanduser('~/.ssh/id_rsa.pub')).read()]
    user_env = UserEnv(config.DOCKER0_ADDR, config.USER_ENV_FOLDER)
    user_env.setup(ssh_keys)
    user_env.start()
    try:
        print 'Type Ctrl+C to quit'
        signal.pause()
    except KeyboardInterrupt:
        print 'Ctrl+C'
    finally:
        user_env.stop()
        user_env.teardown()


if __name__ == '__main__':  # pragma: no cover
    _main()

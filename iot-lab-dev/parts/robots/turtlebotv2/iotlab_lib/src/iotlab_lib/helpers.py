# -*- coding:utf-8 -*-

# This file is a part of IoT-LAB robots
# Copyright (C) 2015 Univ. Strasbourg, INRIA (Contact: admin@iot-lab.info)
# Contributor(s) : see AUTHORS file
#
# This software is governed by the CeCILL license under French law
# and abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# http://www.cecill.info.
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Pose Helpers functions.

Created for the IoT-LAB Project: https://www.iot-lab.info
"""

import functools
import yaml

from tf.transformations import quaternion_from_euler, euler_from_quaternion
from geometry_msgs.msg import Pose, Quaternion


def pose(x, y, theta=0.0, **_):  # pylint:disable=invalid-name
    """Create a 'Pose' object from coordinates."""
    pose_ = Pose()
    pose_.position.x = float(x)
    pose_.position.y = float(y)
    pose_.orientation = quaternion_from_theta(float(theta))
    return pose_


EULER_YAW = 2


def theta_from_quaternion(ori):
    """Return yaw angle, the theta orientation of the pose quaternion."""
    quaternion = (ori.x, ori.y, ori.z, ori.w)  # convert to tuple
    euler = euler_from_quaternion(quaternion)
    theta = euler[EULER_YAW]  # yaw
    return theta


def quaternion_from_theta(theta):
    """Return quaternion for theta."""
    return Quaternion(*quaternion_from_euler(0, 0, float(theta)))


def yaml_pose_reader(filename, section, entry):
    """Read given section/entry pose from yaml file."""
    with open(filename) as filename:
        data = yaml.safe_load(filename)
    goal = data[section][entry]

    return pose(goal['x'], goal['y'], goal['theta'])


def syncronized(tlockname):
    """A decorator to place an instance based lock around a method """
    def _wrap(func):
        """ Decorator implementation """
        @functools.wraps(func)
        def _wrapped_f(self, *args, **kwargs):
            """ Function protected by 'rlock' """
            tlock = self.__getattribute__(tlockname)
            tlock.acquire()
            try:
                return func(self, *args, **kwargs)
            finally:
                tlock.release()
        return _wrapped_f
    return _wrap

# -*- coding:utf-8 -*-

# This file is a part of IoT-LAB robots
# Copyright (C) 2015 Univ. Strasbourg, INRIA (Contact: admin@iot-lab.info)
# Contributor(s) : see AUTHORS file
#
# This software is governed by the CeCILL license under French law
# and abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# http://www.cecill.info.
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Turtlebot.wheels implementation.

Manage robot motor and wheels
"""

import math
import logging
import threading
import contextlib
from copy import deepcopy

import rospy

from kobuki_msgs.msg import MotorPower
from geometry_msgs.msg import Pose, Twist
from nav_msgs.msg import Odometry

from iotlab_lib.helpers import theta_from_quaternion

from iotlab_msgs.srv import KobukiMotorPower, KobukiMotorPowerResponse

LOGGER = logging.getLogger('iotlab_lib')


class Wheels(object):
    """Robot Wheels management."""

    TURTLEBOT_NAME = rospy.get_param('/iotlab/turtlebot_name')

    MOTOR_POWER_SRV = '/{0}/handle_motor_power'.format(TURTLEBOT_NAME)

    MOTOR_TOPIC = '/mobile_base/commands/motor_power'
    CMD_VEL_TOPIC = '/cmd_vel_mux/input/safety_controller'
    ODOM_TOPIC = '/odom'

    CMD_VEL_TIME_FACTOR = 0.1

    # Factor between requested speed and effective one
    # Tested with given speed, may be different with other speeds…
    DRIVE_SPEED = 0.2
    DRIVE_REAL_SPEED_FACTOR = 0.80  # good enough
    ROT_SPEED = 1.2
    ROT_REAL_SPEED_FACTOR = 0.70  # also good enough (0.1 precision)

    ODOM_DEBUG_TIMEOUT = 10

    def __init__(self):
        self.motor_topic = rospy.Publisher(self.MOTOR_TOPIC, MotorPower,
                                           queue_size=10)

        self.cmd_vel = rospy.Publisher(self.CMD_VEL_TOPIC, Twist,
                                       queue_size=10)

        self._odom_event = threading.Event()
        self._odom_pose = Pose()

        # Motor power service
        self.srv = rospy.Service(self.MOTOR_POWER_SRV, KobukiMotorPower,
                                 self._motor_srv)
        LOGGER.debug('Turtlebot.Wheels.motor_power: Service ready')

    def _shutdown(self):
        """Shutdown service.

        Used in tests"""
        self.srv.shutdown()

    # Odometry debug functions

    @property
    def odom_pose(self):
        """Odom position attribute with an event syncronisation.

        When reading a value, wait until a new value is published.
        """
        self._odom_event.clear()
        self._odom_event.wait(self.ODOM_DEBUG_TIMEOUT)
        return self._odom_pose

    @odom_pose.setter
    def odom_pose(self, position):
        self._odom_event.set()
        self._odom_pose = position

    @contextlib.contextmanager
    def odom_monitoring(self):
        """Monitor odometry context manager."""
        odom_topic = rospy.Subscriber(self.ODOM_TOPIC, Odometry, self._odom_cb)
        yield
        odom_topic.unregister()

    def _odom_cb(self, odometry):
        """Update odometry.

        :type odom: Odometry
        """
        self.odom_pose = deepcopy(odometry.pose.pose)

    # Motor

    def motor_on(self, block=True):
        """Turn on wheels motor."""
        LOGGER.debug('Turtlebot.Wheels.motor on')
        self._motor(MotorPower.ON, block)

    def motor_off(self, block=True):
        """Turn off wheels motor."""
        LOGGER.debug('Turtlebot.Wheels.motor off')
        self._motor(MotorPower.OFF, block)

    def _motor(self, motor_power, block=True):
        """Set motor power

        :type motor_power: MotorPower value
        """
        self.motor_topic.publish(MotorPower(motor_power))
        if block:
            rospy.sleep(1)

    def _motor_srv(self, req):
        """Motor power service."""
        LOGGER.debug("Turtlebot.Wheels.motor_power [%s]", req.State)

        response = 'Motor Power [DONE]'
        if req.State == 'ON':
            self.motor_on()
        elif req.State == 'OFF':
            self.motor_off()
        else:
            response = 'Motor Power [ERROR]'
            LOGGER.error('Turtlebot.Wheels.motor_power: error')

        return KobukiMotorPowerResponse(response)

    # Move

    def go_straight(self, distance, speed=DRIVE_SPEED):
        """Go straight for `distance` meters."""
        LOGGER.debug('Turtlebot.Wheels.go_straight: %.02f m', distance)

        time = abs(distance / (speed * self.DRIVE_REAL_SPEED_FACTOR))

        twist = Twist()
        twist.linear.x = math.copysign(speed, distance)
        return self._move_wheels(time, twist)

    # Rotating

    def rotate(self, angle, speed=ROT_SPEED):
        """Rotate `angle`."""
        LOGGER.debug('Turtlebot.Wheels.rotate: %.02f rad', angle)

        time = abs(angle / (speed * self.ROT_REAL_SPEED_FACTOR))

        twist = Twist()
        twist.angular.z = math.copysign(speed, angle)
        self._move_wheels(time, twist)

    def _move_wheels(self, time, twist):
        """Move wheels for 'time' with 'twist'.

        :type twist: Twist
        """
        num = int(time / self.CMD_VEL_TIME_FACTOR)

        for _ in range(num):
            self.cmd_vel.publish(twist)
            rospy.sleep(self.CMD_VEL_TIME_FACTOR)

        # stop
        self.cmd_vel.publish(Twist())

    # Combined move

    def reverse_gear_and_turn_back(self):
        """Move robot back one meter and turn 180 degrees."""
        with self.odom_monitoring():
            initial_pose = self.odom_pose
            self.go_straight(-1)
            inter_pose = self.odom_pose
            self.rotate(math.pi)
            final_pose = self.odom_pose

        # log results
        distance = self._dist(initial_pose, inter_pose)
        angle = self._angle_diff(inter_pose, final_pose)

        LOGGER.debug('Turtlebot.Wheels.reverse_gear_and_turn_back'
                     ' for distance %.02f and angle %.02f', distance, angle)

    @staticmethod
    def _dist(pose_1, pose_2):
        """Dist between two poses.

        :type pose_1: Pose
        :type pose_2 Pose
        """
        p_1 = pose_1.position
        p_2 = pose_2.position
        return math.sqrt((p_2.x - p_1.x) ** 2 + (p_2.y - p_1.y) ** 2)

    @staticmethod
    def _angle_diff(first, last):
        """Diff in theta angle between `first` and `last.

        :type first: Pose
        :type last: Pose
        """
        theta_1 = theta_from_quaternion(first.orientation)
        theta_2 = theta_from_quaternion(last.orientation)
        return theta_2 - theta_1

# -*- coding:utf-8 -*-

# This file is a part of IoT-LAB robots
# Copyright (C) 2015 Univ. Strasbourg, INRIA (Contact: admin@iot-lab.info)
# Contributor(s) : see AUTHORS file
#
# This software is governed by the CeCILL license under French law
# and abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# http://www.cecill.info.
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Turtlebot.power implementation."""

import logging

import rospy
from diagnostic_msgs.msg import DiagnosticArray
# KevinJio
from smart_battery_msgs.msg import SmartBatteryStatus
# from sensor_msgs.msg import BatteryState

from iotlab_msgs.msg import RobotPower


LOGGER = logging.getLogger('iotlab_lib')


class Power(object):
    """Robot Power management."""

    TURTLEBOT_NAME = rospy.get_param('/iotlab/turtlebot_name')

    DIAGNOSTIC_TOPIC = '/{0}/diagnostics_agg'.format(TURTLEBOT_NAME)
    LAPTOP_BATTERY_TOPIC = '/{0}/laptop_charge'.format(TURTLEBOT_NAME)

    POWER_TOPIC_FREQ = 1.0

    BATTERY_THRESHOLD_HIGH = rospy.get_param('/iotlab/battery_s1')
    BATTERY_THRESHOLD_LOW = rospy.get_param('/iotlab/battery_s2')

    def __init__(self, power_topic):
        self.laptop = 0.0
        self.laptop_status = 'STALE'
        self.robot = 0.0
        self.robot_status = 'STALE'
        self.source = None

        self._power_topic = rospy.Publisher(power_topic, RobotPower,
                                            queue_size=10, latch=True)
        self._subscribe_topics()
        self._wait_init()

        rospy.Timer(rospy.Duration(self.POWER_TOPIC_FREQ), self._publish)

    @property
    def level(self):
        """Batteries level in ('high', 'medium', 'low')."""
        high, low = self.BATTERY_THRESHOLD_HIGH, self.BATTERY_THRESHOLD_LOW

        if self.laptop > high and self.robot > high:
            return 'high'
        elif self.laptop > low and self.robot > low:
            return 'medium'
        else:
            return 'low'

    def status_dict(self):
        """Power info dict."""
        ret = {
            'robot_battery_status': self.robot_status,
            'laptop_battery_status': self.laptop_status,

            'kobuki_percentage': self.robot,
            'kobuki_battery_source': self.source,

            'laptop_percentage': self.laptop,

            'battery_level': self.level,
        }
        # Run sanity check as in previous version
        self._sanity_check()
        return ret

    def battery_not_high_warning(self):
        """Print battery warning on low battery."""
        if self.robot < self.BATTERY_THRESHOLD_HIGH:
            LOGGER.warning('Turtlebot.Power.robot battery too low')
        if self.laptop < self.BATTERY_THRESHOLD_HIGH:
            LOGGER.warning('Turtlebot.Power.laptop battery too low')

    def _publish(self, _):
        """Publish to robot_power topic."""
        power = RobotPower(self.level, self.source)
        self._power_topic.publish(power)

    def _sanity_check(self):
        """Sanity check of the batteries health."""
        if self.laptop_status not in ('OK', 'Maximum'):
            LOGGER.warning('Laptop battery problem *%s*', self.laptop_status)
        if self.robot_status not in ('Healthy', 'Maximum'):
            LOGGER.warning('Robot battery problem *%s*', self.robot_status)

    def _subscribe_topics(self):
        """Subscribe to RAW sensors topics."""
        rospy.Subscriber(self.DIAGNOSTIC_TOPIC, DiagnosticArray,
                         self._diagnostic_cb)
        # KevinJio
        rospy.Subscriber(self.LAPTOP_BATTERY_TOPIC, SmartBatteryStatus,
                         self._laptop_cb)
        # rospy.Subscriber(self.LAPTOP_BATTERY_TOPIC, BatteryState,
        #                  self._laptop_cb)

    @staticmethod
    def _wait_init():
        """Wait until callbacks have been called.

        Lazy to really wait until all called (1Hz) so be sure, wait 2 secs.
        """
        rospy.sleep(2)

    # # # # # # # # # # #
    # Sensors callbacks #
    # # # # # # # # # # #
    def _laptop_cb(self, smart_battery_status):
        """Laptop battery callback."""
        self.laptop = smart_battery_status.percentage

    def _diagnostic_cb(self, diagnostic_array):
        """Diagnostic_agg callback.

        Read robot and status power infos.
        """
        for current in diagnostic_array.status:
            if current.name == '/Power System':
                self._status_diag(current.values)
            elif current.name == '/Power System/Battery':
                self._battery_diag(current.values)

    def _status_diag(self, values):
        """Status diagnostic infos."""
        for value in values:
            if value.key == 'Laptop Battery':
                self.laptop_status = value.value
            elif value.key == 'mobile_base_nodelet_manager: Battery':
                self.robot_status = value.value

    def _battery_diag(self, values):
        """Robot battery infos."""
        for value in values:
            if value.key == 'Percent':
                self.robot = value.value
            elif value.key == 'Source':
                self.source = value.value

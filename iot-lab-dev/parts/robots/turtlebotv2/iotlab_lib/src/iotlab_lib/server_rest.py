# -*- coding:utf-8 -*-

# This file is a part of IoT-LAB robots
# Copyright (C) 2015 Univ. Strasbourg, INRIA (Contact: admin@iot-lab.info)
# Contributor(s) : see AUTHORS file
#
# This software is governed by the CeCILL license under French law
# and abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# http://www.cecill.info.
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Rest Server running on the Turtlebot

Created for the IoT-LAB Project: https://www.iot-lab.info
"""

import os
import logging
import functools

import bottle
from bottle import request

import netaddr

from iotlab_lib import config
from iotlab_lib import turtlebot_manager

LOGGER = logging.getLogger('iotlab_lib')


class TurtlebotRESTServer(bottle.Bottle):
    """Class to manage the Turtlebot REST server."""
    HOST = '0.0.0.0'
    PORT = '8081'

    # For admin verification
    SRVEH_ADDR = None
    SRVROS_ADDR = None
    LOCALHOST_ADDR = netaddr.IPAddress('127.0.0.1')

    def __init__(self, manager):
        super(TurtlebotRESTServer, self).__init__()
        self._class_init()
        self._app_routing()
        self.manager = manager

    @classmethod
    def _class_init(cls):
        """Init class attributes at runtime.

        It depends on specific config file that may not be present during
        tests. It allows testing.
        """
        cls.SRVEH_ADDR = netaddr.IPAddress(config.read_srveh_addr())
        cls.SRVROS_ADDR = netaddr.IPAddress(config.read_srvros_addr())

    def _app_routing(self):
        """Declare the REST supported methods."""

        # Admin methods
        self.admin_route('/exp/start/<exp_id:int>/<user>', 'POST',
                         self.exp_start)
        self.admin_route('/exp/stop', 'DELETE', self.exp_stop)
        self.admin_route('/exp/results/<user>_<exp_id:int>.tar.gz', 'GET',
                         self.exp_results_get)
        self.admin_route('/exp/results/<user>_<exp_id:int>.tar.gz', 'DELETE',
                         self.exp_results_del)

        # Future user routes
        self.admin_route('/exp/dock', 'POST', self.exp_dock)
        self.admin_route('/exp/update', 'POST', self.exp_update_mobility)

        self.admin_route('/admin/dock', 'POST', self.admin_dock)
        self.admin_route('/admin/undock', 'POST', self.admin_undock)
        self.admin_route('/admin/reset_node', 'PUT', self.iotlab_node_reset)

        # User methods
        self.route('/robot/status', 'GET', self.robot_status)
        self.route('/robot/version', 'GET', self.robot_version)
        self.route('/robot/position', 'GET', self.robot_position)
        self.route('/robot/go_to_point', 'POST', self.go_to_point)
        self.route('/robot/is_reachable', 'GET', self.is_reachable)
        self.route('/robot/user_env', 'PUT', self.exp_user_env_start)
        self.route('/robot/user_env', 'DELETE', self.exp_user_env_stop)

    def admin_route(self, path, method, callback, *route_args, **route_kwargs):
        """Add a route working only for admins."""
        # Wrap callback in admin verifying function
        admin_callback = self._admin_request(callback)

        # Add route with admin wrapped callback
        self.route(path, method, callback=admin_callback,
                   *route_args, **route_kwargs)

    def _admin_request(self, func):
        @functools.wraps(func)
        def _wrapped_f(*args, **kwargs):
            """Call rest method only if request is admin."""
            ip_addr = request.get('REMOTE_ADDR')
            is_admin = self.is_admin_ip(ip_addr)

            if is_admin:
                return func(*args, **kwargs)
            else:
                LOGGER.warning('Non admin, aborting. IP addr == %s', ip_addr)
                bottle.response.status = 403
                return 'Error: 403 Forbidden\n'
        return _wrapped_f

    def is_admin_ip(self, ip_addr):
        """Verify that the ip_addr is an admin request.

        * It is from experiment handler
        """
        ip_addr = netaddr.IPAddress(ip_addr)

        # It should be only a Whitelist!

        is_admin = False
        is_admin |= (ip_addr == self.LOCALHOST_ADDR)
        is_admin |= (ip_addr == self.SRVEH_ADDR)
        is_admin |= (ip_addr == self.SRVROS_ADDR)
        # allow kvm host too ?

        return is_admin

    def exp_start(self, user, exp_id):
        """Start an experiment.

        :param user: user of the experiment owner
        :param exp_id: experiment id
        """
        LOGGER.info('Rest server: start experiment %s:%s', exp_id, user)
        LOGGER.debug('Json Request: %s', request.json)
        json_dict = dict(request.json)

        # Get ssh_keys, if None, default to no keys
        ssh_keys = json_dict.get('ssh_keys', None) or []
        mob_cfg = self._mobility_cfg(json_dict)

        ret = self.manager.exp_start(user, exp_id, ssh_keys, **mob_cfg)
        return {'ret': ret}

    def exp_stop(self):
        """Stop an experiment."""
        LOGGER.info('Rest server: stop experiment')
        ret = self.manager.exp_stop()
        return {'ret': ret}

    def exp_update_mobility(self):
        """Update robot profile, aka mobility model (ie circuit)."""
        LOGGER.info('Rest server: update profile')

        mobility_dict = dict(request.json)

        # Compat old version
        if (mobility_dict or {}).get('type') == 'controlled':
            ret = self.manager.exp_user_env_start()
            return {'ret': ret}
        if mobility_dict and 'loop' not in mobility_dict:
            mobility_dict = self._mobility_compat(mobility_dict)

        ret = self.manager.exp_update_mobility(mobility_dict)

        return {'ret': ret}

    def exp_user_env_start(self):
        """Start user environment."""
        ret = self.manager.exp_user_env_start()
        return {'ret': ret}

    def exp_user_env_stop(self):
        """Stop user environment."""
        ret = self.manager.exp_user_env_stop()
        return {'ret': ret}

    def _mobility_cfg(self, json_dict):
        """Extract mobility and user_env config from json request."""
        mob_cfg = {
            'robot_user_env': json_dict.get('robot_user_env', False),
            'mobility_dict': json_dict.get('mobility', None),
        }

        # Compatibility
        if 'coordinates' in json_dict:
            coords = json_dict['coordinates']
            if coords is not None and isinstance(coords, dict):
                # Transitional
                mob_cfg['mobility_dict'] = json_dict.copy()
            else:
                mob_cfg['mobility_dict'] = self._mobility_compat(json_dict)
        if json_dict.get('type') == 'controlled':
            mob_cfg['robot_user_env'] = True

        return mob_cfg

    @staticmethod
    def _mobility_compat(compat_mobility):
        LOGGER.warning('Deprecated mobility format')

        # Convert coordinates
        coords = compat_mobility['coordinates']
        if coords is None:
            return None

        mobility_dict = {
            'name': compat_mobility.get('trajectory_name', 'NONAME'),
            'site': compat_mobility.get('site_name', 'NOSITE'),
            'type': compat_mobility['type'],
            'points': compat_mobility['points'],
            'coordinates': None,  # Set in a second time
            # ssh_keys ignored
            'loop': True,
        }

        # Convert list to dict
        assert isinstance(coords, list)

        coordinates = {}
        for point in coords:
            p_new = {'x': point['x'], 'y': point['y'], 'theta': point['w'],
                     'name': point['name']}
            coordinates[point['name']] = p_new

        mobility_dict['coordinates'] = coordinates

        return mobility_dict

    def exp_results_get(self, user, exp_id):
        """Download compressed results from a finished experiment.

        :param user: user of the experiment owner
        :param exp_id: experiment id
        """
        LOGGER.info('Restserver: download log experiment %s:%s', user, exp_id)
        archive = self.manager.exp_results_get(user, exp_id)

        root = os.path.dirname(archive)
        name = os.path.basename(archive)

        LOGGER.debug('Restserver.exp_results: %s file for download', archive)
        return bottle.static_file(name, root=root)

    def exp_results_del(self, user, exp_id):
        """Delete results from a finished experiment."""
        ret = self.manager.exp_results_del(user, exp_id)
        return {'ret': ret}

    def go_to_point(self):
        """Go to given point.

        Point in request: x, y, theta=0.0, name=None
        """
        LOGGER.info('RestServer: go to point')
        json_dict = dict(request.json)

        try:
            point = self._point(json_dict)
        except (KeyError, TypeError):
            LOGGER.warning('RestServer: Invalid coordinates')
            return {'ret': 1, 'err': 'Invalid Coordinates'}

        result = self.manager.exp_go_to_point(**point)
        return {'ret': result}

    @staticmethod
    def _point(json_dict):
        """Get a point from a json_dict."""
        point = {
            'x': float(json_dict['x']),
            'y': float(json_dict['y']),
        }
        if json_dict.get('theta', None) is not None:
            point['theta'] = float(json_dict['theta'])
        if json_dict.get('name', None) is not None:
            point['name'] = str(json_dict['name'])
        return point

    def is_reachable(self):
        """Tell if point is reachable by the robot.

        :query_string: x:float y:float
        """
        try:
            # pylint:disable=invalid-name
            x = float(request.query.x)  # pylint:disable=no-member
            y = float(request.query.y)  # pylint:disable=no-member
        except (ValueError, TypeError) as err:
            LOGGER.error(
                'RestServer.is_reachable: Invalid query string: %r', err)
            bottle.response.status = 400
            return 'Requires x:float and y:float query strings\n'

        reachable = self.manager.is_reachable(x, y)
        return {'reachable': reachable, 'pose': {'x': x, 'y': y}}

    def robot_position(self):
        """Return robot position dict: x, y and theta."""
        return self.manager.position()

    def robot_status(self):
        """Get the status of a robot."""
        return self.manager.status()

    def robot_version(self):
        """Get the version of a robot."""
        return {'version': self.manager.robot_version()}

    def exp_dock(self):
        """Dock robot in experiment."""
        LOGGER.info('Rest server: exp_dock')
        ret = self.manager.exp_go_to_dock()
        return {'ret': ret}

    # Admin commands

    def admin_dock(self):
        """Admin dock robot."""
        LOGGER.info('Rest server: admin_dock')
        ret = self.manager.admin_dock()
        return {'ret': ret}

    def admin_undock(self):
        """Admin undock robot."""
        LOGGER.info('Rest server: admin_undock')
        ret = self.manager.admin_undock()
        return {'ret': ret}

    def iotlab_node_reset(self):
        """Reset iotlab node."""
        LOGGER.info('Rest server: reset_iotlab_node')
        ret = self.manager.iotlab_node_reset()
        return {'ret': ret}


def main():
    """Command line main function."""
    manager = turtlebot_manager.TurtlebotManager()
    server = TurtlebotRESTServer(manager)
    server.run(host=server.HOST, port=server.PORT, server='paste')

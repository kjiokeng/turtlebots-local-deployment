# -*- coding:utf-8 -*-

# This file is a part of IoT-LAB robots
# Copyright (C) 2015 Univ. Strasbourg, INRIA (Contact: admin@iot-lab.info)
# Contributor(s) : see AUTHORS file
#
# This software is governed by the CeCILL license under French law
# and abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# http://www.cecill.info.
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""MobilityManager manages the robot mobility.

Created for the IoT-LAB Project: https://www.iot-lab.info
"""

import threading

import rospy

from iotlab_msgs.msg import RobotPower, RobotState

from iotlab_lib import helpers, logger, turtlebot
from iotlab_lib.mobility import Mobility


LOGGER = logger.logger_custom_format('MobilityManager.%(funcName)s: ')


class MobilityManager(threading.Thread):
    """Class to manage a Robot Mobility."""

    STATES = ('OFF', 'STARTING', 'RUNNING', 'FINISHING', 'ENDLOOP')
    MOBILITY_STOP = Mobility.from_stop()
    STOP_TIMEOUT = 10 * 60

    def __init__(self, robot):
        """Circuit initialization."""
        threading.Thread.__init__(self)

        self.robot = robot
        self._event_wait = threading.Event()

        self._next_goal = None
        self.mobility = None
        self.mobility_lock = threading.RLock()

        self.state = 'OFF'
        self.go_dock = False

        # Topics Subscribers
        rospy.Subscriber(turtlebot.Turtlebot.POWER_TOPIC, RobotPower,
                         self._robot_power_cb)
        rospy.Subscriber(turtlebot.Turtlebot.STATE_TOPIC, RobotState,
                         self._robot_state_cb)

    def start(self):
        """Begin mobility behavior."""
        if self.robot.state != 'DOCKED':
            LOGGER.error('Not in dock %r', self.robot.state)
            return 1
        threading.Thread.__init__(self)

        self.go_dock = False
        ret = self.update()
        if ret == 0:
            self.state = 'STARTING'

        super(MobilityManager, self).start()
        return ret

    def stop_async(self):
        """End Mobility."""
        LOGGER.debug('')
        if self.state == 'OFF':
            return 0
        self.update()
        self.state = 'FINISHING'
        self.got_event()

        return 0

    def wait_stopped(self, timeout=STOP_TIMEOUT):
        """Wait asyncronous stop finished.

        :raises: RuntimError on timeout
        """
        _ = self.state == 'OFF' or self.join(timeout)

    def run(self):
        """Mobility thread."""
        LOGGER.info('Thread')
        self.state = 'RUNNING'
        self._debug_while_state()

        while self._do_loop():
            self.wait_event()
            self.loop()

        LOGGER.info('Breaking loop')
        self.state = 'OFF'
        LOGGER.debug('Out thread')

    def loop(self):
        """Mobility loop."""
        robot_state = self.robot.state

        if robot_state in ('ERROR', 'INIT'):
            LOGGER.error('Error state: %r', robot_state)
            self.state = 'ENDLOOP'

        elif robot_state == 'NEAR_DOCK':
            self.robot.dock()

        elif robot_state == 'DOCKED' and self.state == 'FINISHING':
            # Finished cleanly
            self.state = 'ENDLOOP'

        elif robot_state == 'DOCKED' and self._has_goal_and_battery():
            self.robot.undock()

        elif robot_state == 'WAIT_POINT' and self._dock_required():
            self.robot.go_to_dock()

        elif robot_state == 'WAIT_POINT' and self.has_goal():
            # mobility may be updated between has_goal() and next_goal()
            goal = self.next_goal()
            if goal is not None:
                self.go_to_goal(goal)

        else:
            # Else case, don't print debug
            return

        self._debug_while_state()

    def _debug_while_state(self):
        """Print debug infos in while."""
        LOGGER.debug('Mobility %s robot %s', self.state, self.robot.state)

    def go_to_goal(self, goal):
        """Go to given goal.

        :type goal: (name, Pose) tuple
        """
        assert goal is not None
        name, pose = goal
        LOGGER.info('%s', name)

        self.robot.go_to_pose(pose)

        if not self.has_goal():
            LOGGER.debug('No next points, wait')

    @helpers.syncronized('mobility_lock')
    def update(self, mobility=None):
        """Update Mobility."""
        LOGGER.debug(mobility)

        # Don't allow setting a mobility if finishing or stopped
        if mobility is not None and self.state not in ('RUNNING', 'STARTING'):
            LOGGER.error('Failed not in mobility')
            return 1

        if mobility is None:
            # Use a empty mobility
            mobility = self.MOBILITY_STOP

        self.mobility = mobility
        self._next_goal = None

        self.got_event()
        return 0

    @helpers.syncronized('mobility_lock')
    def has_goal(self):
        """Is if there are available goals."""
        self._next_goal = self.next_goal()
        return self._next_goal is not None

    @helpers.syncronized('mobility_lock')
    def next_goal(self):
        """Get next goal or None."""
        # Try returning saved goal
        if self._next_goal is not None:
            goal = self._next_goal
            self._next_goal = None
            return goal

        return next(self.mobility, None)

    def dock(self):
        """Require robot to go to dock."""
        if self.state != 'RUNNING':
            LOGGER.error('Failed not in mobility')
            return 1

        self.go_dock = True
        return self.update(None)  # Unlocks loop

    # # # # # # # #
    # Wait event  #
    # # # # # # # #

    def got_event(self):
        """Got event, unlock 'wait_event'."""
        self._event_wait.set()

    def wait_event(self):
        """Block until event/update."""
        self._event_wait.wait()
        self._event_wait.clear()

    # # # # # # # #
    # Callbacks  #
    # # # # # # # #

    def _robot_power_cb(self, robot_power):
        """Stop mobility on low battery.

        :type robot_power: RobotPower
        """
        self.got_event()

    def _robot_state_cb(self, robot_state):
        """Robot state updated, unlock the loop.

        :type robot_state: RobotState
        """
        if robot_state.state == 'DOCKED':
            self.go_dock = False

        self.got_event()

    # # # # # # # #
    # Conditions  #
    # # # # # # # #

    def _do_loop(self):
        """Keep thread running if rospy and mobility is running."""
        ret = (self.state in ('RUNNING', 'FINISHING') and
               not rospy.is_shutdown())
        return ret

    def _has_goal_and_battery(self):
        return self.has_goal() and self.robot.battery == 'high'

    def _dock_required(self):
        ret = (self.state == 'FINISHING' or
               self.robot.battery == 'low' or
               self.go_dock)
        return ret

# -*- coding:utf-8 -*-
"""Configuration reader module."""

import os

import ConfigParser


NODE_IP_ADDR = '10.255.255.1'
NODE_FS = '/iotlab/images/%s/' % NODE_IP_ADDR

ROBOT_VERSION = '/home/turtlebot/iotlab_version'
BRNODES_CFG = '/home/turtlebot/brnodes.cfg'
DOCKER0_ADDR = '172.17.42.1'
DOCKER_SUBNET = '%s/16' % DOCKER0_ADDR

MEAS_DIR_FMT = '/iotlab/users/{user}/.iot-lab/{exp_id}'
MEAS_ARCHIVE_FMT = os.path.join(MEAS_DIR_FMT, '{user}_{exp_id}.tar.gz')
MEAS_FILE_FMT = os.path.join(MEAS_DIR_FMT, 'robot', '{hostname}.oml')

USER_ENV_FOLDER = '/home/turtlebot/catkin_ws_indigo/src/user_environment/'


def node_hostname():
    """Read Node hostname."""
    node_conf_hostname = os.path.join(NODE_FS, 'conf', 'hostname')
    with open(node_conf_hostname) as _cfg:
        return _cfg.read().strip().lower()


def read_srveh_addr():
    """Srveh ip address."""
    config = ConfigParser.ConfigParser()
    config.read(BRNODES_CFG)
    return config.get('brnodes', 'brnodes_srveh_ipaddr')


def read_srvros_addr():
    """Srveh ip address."""
    config = ConfigParser.ConfigParser()
    config.read(BRNODES_CFG)
    try:
        return config.get('brnodes', 'brnodes_srvros_ipaddr')
    except ConfigParser.NoOptionError:
        return '127.0.0.1'  # no harm default result


def robot_version():
    """Get the version of a robot."""
    with open(ROBOT_VERSION) as version_f:
        return version_f.readline()

# -*- coding:utf-8 -*-

# This file is a part of IoT-LAB robots
# Copyright (C) 2015 Univ. Strasbourg, INRIA (Contact: admin@iot-lab.info)
# Contributor(s) : see AUTHORS file
#
# This software is governed by the CeCILL license under French law
# and abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# http://www.cecill.info.
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Log robot position in a OML file.

Created for the IoT-LAB Project: http://www.iot-lab.fr
"""

import os
import math
import textwrap
import logging
import datetime
import calendar

import rospy
from geometry_msgs.msg import PoseStamped

from iotlab_lib import helpers, logger

LOGGER = logger.logger_custom_format('RobotPosition: ')


class RobotPosition(object):
    """
        Class RobotPosition contain all function usefull to log robot position.
    """
    measure_logger = logging.getLogger('user_mesure')
    measure_logger.setLevel(logging.INFO)
    measure_logger.propagate = 0  # avoid log duplication to root logger

    ROBOT_OML_SCHEMA = 10
    ROBOT_OML_SCHEMA_NAME = 'robot_pose'

    APP_NAME = 'robot_position_measures'

    _fmt = {
        'schema': ROBOT_OML_SCHEMA,
        'schema_name': ROBOT_OML_SCHEMA_NAME,
        'app_name': APP_NAME,
    }

    OML_HEADER = textwrap.dedent('''\
    protocol: 4
    domain: {exp_id}
    start-time: {start_time}
    sender-id: {hostname}
    app-name: %(app_name)s
    schema: 0 _experiment_metadata subject:string key:string value:string
    schema: %(schema)s %(schema_name)s timestamp_s:uint32 timestamp_us:uint32 x:double y:double theta:double
    content: text

    ''') % _fmt  # noqa

    MEAS_FMT = '{elapsed:f}\t%(schema)d\t{counter:d}\t{t_s:d}\t{t_us:06d}\t{pos.x:f}\t{pos.y:f}\t{theta:f}'  # noqa  # pylint:disable=line-too-long
    MEAS_FMT = MEAS_FMT % _fmt

    assert len(OML_HEADER.splitlines()) == 9  # Verify OML header length

    def __init__(self, pose_topic_path, logfile, hostname, exp_id):
        # init counter
        self.theta_before = 0.0
        self.counter = 0
        self.t_0 = datetime.datetime.utcnow()
        start_s = self.datetime_timestamp_s(self.t_0)

        LOGGER.debug('%s-%s', logfile, start_s)

        # init logger
        oml_header = self.OML_HEADER.format(hostname=hostname, exp_id=exp_id,
                                            start_time=start_s)
        self.log_handler = self._create_log_handler(logfile, oml_header)

        # Topics
        self.pose_topic = None
        self.pose_topic_path = pose_topic_path

    def start(self):
        """Start measures."""
        LOGGER.debug('start')
        self.measure_logger.addHandler(self.log_handler)
        self.pose_topic = rospy.Subscriber(self.pose_topic_path, PoseStamped,
                                           self._pose_callback)

    def stop(self):
        """Stop measures"""
        LOGGER.debug('stop')
        self.pose_topic.unregister()
        self.measure_logger.removeHandler(self.log_handler)

    def write(self, msg, *args):
        """ Write message to out oml file """
        self.measure_logger.info(msg, *args)

    @classmethod
    def _create_log_handler(cls, logfile, header):
        """Create 'user_measure' log handler.

        * Create log directory
        * Write header in file
        * Create logging handles
        """

        try:
            os.makedirs(os.path.dirname(logfile), mode=0777)
        except OSError:  # pragma: no cover
            pass

        with open(logfile, 'w') as _file:
            _file.write(header)

        log_handler = logging.FileHandler(logfile, 'a', None, False)
        log_handler.setLevel(logging.INFO)
        log_handler.setFormatter(logging.Formatter('%(message)s'))

        return log_handler

    def _pose_callback(self, posestamped):
        """Position updated.

        :param posestamped: new position
        """
        pose = posestamped.pose.position
        theta = helpers.theta_from_quaternion(posestamped.pose.orientation)
        # Re-arange theta
        theta = self._theta_atan_compensation(theta)

        self.counter = self.counter + 1

        # Time
        current = self._datetime_from_posestamped(posestamped)
        elapsed_time = (current - self.t_0).total_seconds()  # float
        t_s = self.datetime_timestamp_s(current)
        t_us = current.microsecond

        meas = self.MEAS_FMT.format(elapsed=elapsed_time, counter=self.counter,
                                    t_s=t_s, t_us=t_us, pos=pose, theta=theta)
        self.write(meas)

    @staticmethod
    def datetime_timestamp_s(datetime_obj):
        """Return 'epoch' timestamp.

        http://stackoverflow.com/a/1077362/395687
        """
        return calendar.timegm(datetime_obj.utctimetuple())

    @staticmethod
    def _datetime_from_posestamped(posestamped):
        """Create a datetime object from PoseStamped timestamp."""
        stamp = posestamped.header.stamp
        msecs = (stamp.nsecs / 1000)

        pose_time = datetime.datetime.utcfromtimestamp(stamp.secs)
        pose_time = pose_time.replace(microsecond=msecs)
        return pose_time

    def _theta_atan_compensation(self, theta):
        # Discontinuity compensation of the atan2 used to compute
        # euler angles from quaternion
        # http://en.wikipedia.org/wiki/Atan2
        if theta < 0.0 and self.theta_before > 3.0:
            theta = theta + 2 * math.pi

        self.theta_before = theta
        return theta

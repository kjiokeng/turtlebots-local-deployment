# -*- coding:utf-8 -*-
"""Rapp tasks management."""

import threading
import logging

import rospy

from rocon_app_manager_msgs.msg import RappList
from rocon_app_manager_msgs.srv import StartRapp, StopRapp

LOGGER = logging.getLogger('iotlab_lib')


class Rapp(object):
    """Rocon Rapp manager."""
    TURTLEBOT_NAME = rospy.get_param('/iotlab/turtlebot_name')
    ROCON_SRV_START = '/{0}/start_rapp'.format(TURTLEBOT_NAME)
    ROCON_SRV_STOP = '/{0}/stop_rapp'.format(TURTLEBOT_NAME)
    RAPP_LIST = '/{0}/rapp_list'.format(TURTLEBOT_NAME)

    def __init__(self, name, remappings=(), parameters=()):
        self.name = name
        self.remappings = remappings
        self.parameters = parameters

        self._stopped_event = threading.Event()

        rospy.Subscriber(self.RAPP_LIST, RappList, self._rapp_list_cb)

    def start(self, timeout=5.0):
        """Start given Rapp."""
        self._stopped_event.clear()

        ret = self._start()
        if ret:
            return ret

        # Wait timeout if rapp crashes
        error = self._stopped_event.wait(timeout)
        if error:
            LOGGER.error('Rapp.start(%s) failed', self.name)
            return 1

        return 0

    def start_with_retry(self, retry=5, delay=5, *args, **kwargs):
        """Try starting it `retry` times with `delay` between each try."""
        for count in range(0, retry):
            LOGGER.debug('Rapp.start(%s): try %d', self.name, count)
            ret = self.start(*args, **kwargs)
            if ret == 0:
                break
            rospy.sleep(delay)
        return ret

    def stop(self):
        """Stop rapp."""
        return self._stop()

    def _start(self):
        """Call rocon rapp start."""
        rospy.wait_for_service(self.ROCON_SRV_START)
        call_service = rospy.ServiceProxy(self.ROCON_SRV_START, StartRapp)
        try:
            call_service(self.name, self.remappings, self.parameters)
            LOGGER.debug('Rapp.start(%s): service called', self.name)
        except rospy.ServiceException as exc:
            LOGGER.error('Rapp.start(%s): request failed: %s', self.name, exc)
            return 1
        return 0

    def _stop(self):
        """Stop Rapp."""
        rospy.wait_for_service(self.ROCON_SRV_STOP)
        call_service = rospy.ServiceProxy(self.ROCON_SRV_STOP, StopRapp)
        try:
            call_service()
            LOGGER.debug('Rapp.stop: service called')
        except rospy.ServiceException as exc:
            LOGGER.critical('Rapp.stop: service request failed: %s', exc)
            return 1
        return 0

    def _rapp_list_cb(self, rapplist):
        """RappList callback."""
        rapps = self.rapplist_to_dict(rapplist)

        # Current Rapp stopped
        if rapps.get(self.name) == 'Ready':
            self._stopped_event.set()

    @staticmethod
    def rapplist_to_dict(rapplist):
        """Convert to dict."""
        return {rapp.name: rapp.status for rapp in rapplist.available_rapps}

# -*- coding:utf-8 -*-

# This file is a part of IoT-LAB robots
# Copyright (C) 2015 Univ. Strasbourg, INRIA (Contact: admin@iot-lab.info)
# Contributor(s) : see AUTHORS file
#
# This software is governed by the CeCILL license under French law
# and abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# http://www.cecill.info.
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""
    Logger for the iotlab_lib

    Created for the IoT-LAB Project: http://www.iot-lab.fr
"""

import logging
from logging.handlers import RotatingFileHandler

BASE_FMT = '%(asctime)s :: %(levelname)s :: '
PACKAGE = 'iotlab_lib'
LOGGER = logging.getLogger(PACKAGE)


class DispatchingFormatter(object):
    """Dispacter to get Same handlers but different formatter for each logger.

    All credits goes to
        http://stackoverflow.com/a/1742512/395687
    """
    FORMATTERS = {}

    def __init__(self, default_format):
        self._default_formatter = logging.Formatter(default_format)

    def format(self, record):
        """Format record with formatter configured for record.name."""
        formatter = self.FORMATTERS.get(record.name, self._default_formatter)
        return formatter.format(record)

    @classmethod
    def add_formatter(cls, name, fmt):
        """Register formatter `format` for logger `name`."""
        cls.FORMATTERS[name] = logging.Formatter(fmt)


def init_loggers(log_folder):
    """ Init the 'iotlab_lib' file logger.

    Log files will be put in 'log_folder'
    """

    log_folder += '/'

    if LOGGER.handlers != []:
        return

    # configuration log
    loglevel = logging.DEBUG
    dispatch_formatter = DispatchingFormatter(BASE_FMT + '%(message)s')
    LOGGER.setLevel(loglevel)

    # Server logs
    log_path = log_folder + 'turtlebot.log'
    log = RotatingFileHandler(log_path, 'a', maxBytes=1000000, backupCount=1)
    log.setLevel(logging.INFO)
    log.setFormatter(dispatch_formatter)
    LOGGER.addHandler(log)

    # debug log
    log_path = log_folder + 'turtlebot_debug.log'
    log = RotatingFileHandler(log_path, 'a', maxBytes=1000000, backupCount=1)
    log.setLevel(logging.DEBUG)
    log.setFormatter(dispatch_formatter)
    LOGGER.addHandler(log)


def logger_custom_format(msg_prepend, base_fmt=BASE_FMT, base_logger=PACKAGE):
    """Create logger with same config as `base_logger` but specific format.

    Make formatter message be 'msg_prepend %(message)s'
    """
    log_pkg = '%s.%s' % (base_logger, msg_prepend)

    fmt = base_fmt + '{0}%(message)s'.format(msg_prepend)

    # Create sublogger but don't duplicate log
    logger = logging.getLogger(log_pkg)
    # Register new format for logger
    DispatchingFormatter.add_formatter(log_pkg, fmt)

    return logger

# -*- coding:utf-8 -*-

# This file is a part of IoT-LAB robots
# Copyright (C) 2015 Univ. Strasbourg, INRIA (Contact: admin@iot-lab.info)
# Contributor(s) : see AUTHORS file
#
# This software is governed by the CeCILL license under French law
# and abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# http://www.cecill.info.
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Turtlebot.iotlab_node implementation.

Manage node power
"""

import logging

import rospy

from kobuki_msgs.msg import ExternalPower
from iotlab_msgs.msg import Relay

from iotlab_msgs.srv import KobukiExternalPower, KobukiExternalPowerResponse

LOGGER = logging.getLogger('iotlab_lib')


class IoTLABNode(object):
    """IoT-LAB Node management."""

    TURTLEBOT_NAME = rospy.get_param('/iotlab/turtlebot_name')

    NODE_POWER_SRV = '/{0}/handle_node_power'.format(TURTLEBOT_NAME)

    ALIM_NODE = ExternalPower.PWR_5V1A
    RELAY = Relay.RELAY1

    EXTERNAL_POWER_TOPIC = '/mobile_base/commands/external_power'
    RELAY_TOPIC = '/iotlab/node_relay'

    def __init__(self):
        self.external_power = rospy.Publisher(self.EXTERNAL_POWER_TOPIC,
                                              ExternalPower, queue_size=10)
        self.relay = rospy.Publisher(self.RELAY_TOPIC, Relay, queue_size=10)

        # IoT-LAB node power service
        rospy.Service(self.NODE_POWER_SRV, KobukiExternalPower,
                      self._power_srv)
        LOGGER.debug('Turtlebot.IoTLABNode.node_power: Service ready')

    def power_on(self):
        """Power on iotlab node."""
        LOGGER.info("Turtlebot.IoTLABNode.power_on")
        self._power(ExternalPower.ON, Relay.ON)

    def power_off(self):
        """Power off iotlab node."""
        LOGGER.info("Turtlebot.IoTLABNode.power_off")
        self._power(ExternalPower.OFF, Relay.OFF)

    def _power_srv(self, req):
        """IoT-LAB node power service."""
        LOGGER.debug('Turtlebot.IoTLABNode.node_power [%s]', req.State)

        response = 'IoT-LAB Node External Power [DONE]'
        if req.State == 'ON':
            self.power_on()
        elif req.State == 'OFF':
            self.power_off()
        else:
            response = 'IoT-LAB Node External Power [ERROR]'
            LOGGER.error('Turtlebot.IoTLABNode.node_power: error')
        return KobukiExternalPowerResponse(response)

    def _power(self, alim, relay):
        """Set node alim and relay mode."""
        self._set_alim(alim)
        self._set_relay(relay)
        rospy.sleep(1)

    def _set_alim(self, mode):
        """Set node alimentation in 'mode'."""
        power = ExternalPower(self.ALIM_NODE, mode)
        self.external_power.publish(power)

    def _set_relay(self, mode):
        """Set node relay mode."""
        relay = Relay(self.RELAY, mode)
        self.relay.publish(relay)

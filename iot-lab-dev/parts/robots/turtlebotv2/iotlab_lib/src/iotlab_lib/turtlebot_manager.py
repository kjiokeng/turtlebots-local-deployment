# -*- coding:utf-8 -*-

# This file is a part of IoT-LAB robots
# Copyright (C) 2015 Univ. Strasbourg, INRIA (Contact: admin@iot-lab.info)
# Contributor(s) : see AUTHORS file
#
# This software is governed by the CeCILL license under French law
# and abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# http://www.cecill.info.
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Turtlebot's manager implementation.

Created for the IoT-LAB Project: https://www.iot-lab.info
"""

import time
import shutil
import tarfile
import logging

from iotlab_lib import (config, logger, helpers,
                        turtlebot,
                        mobility_manager, mobility,
                        user_env,
                        user_measure)

LOGGER = logging.getLogger('iotlab_lib')


class TurtlebotManager(object):
    """ Class to manage experiment on Turtlebot """

    ROBOT_VERSION = None
    NODE_HOSTNAME = None

    def __init__(self):
        """ Init """
        logger.init_loggers('/var/log/iotlab-ros')

        self._class_init()

        self.position_log = None
        self.robot = turtlebot.Turtlebot()
        self.mobility = mobility_manager.MobilityManager(self.robot)
        self.user_env = user_env.UserEnv(config.DOCKER0_ADDR,
                                         config.USER_ENV_FOLDER)

        LOGGER.debug('TurtlebotManager.init')

    @classmethod
    def _class_init(cls):
        """Init class attributes at runtime.

        It depends on specific config file that may not be present during
        tests. It allows testing.
        """
        cls.ROBOT_VERSION = config.robot_version()
        cls.NODE_HOSTNAME = config.node_hostname()

    def exp_start(self, user, exp_id,  # pylint:disable=too-many-arguments
                  ssh_keys, mobility_dict=None, robot_user_env=False):
        """Start an experiment."""
        LOGGER.debug('TurtlebotManager.start_exp')
        ret = 0

        # Verify mobility validity first
        try:
            mob = mobility.Mobility.from_dict(mobility_dict)
            LOGGER.debug('TurtlebotManager.start_exp mobility: %r',
                         mobility_dict)
        except ValueError:
            LOGGER.error('Invalid mobility: %r', mobility_dict)
            return 1

        self._position_log_start(user, exp_id)

        ret += self.mobility.start()
        if ret:
            LOGGER.error('Could not start mobility')
            return ret

        self.user_env.setup(ssh_keys)

        # Configure experiment
        ret += self.mobility.update(mob)
        if robot_user_env:
            ret += self.user_env.start()

        return ret

    def exp_stop(self):
        """Stop an experiment."""
        LOGGER.debug('TurtlebotManager.stop_exp')
        self.user_env.stop()
        self.user_env.teardown()
        self.mobility.stop_async()
        self._position_log_stop()
        return 0

    def exp_update_mobility(self, mobility_dict=None):
        """Update profile."""
        LOGGER.debug('TurtlebotManager.exp_update_mobility')

        try:
            mob = mobility.Mobility.from_dict(mobility_dict)
        except ValueError:
            LOGGER.error('Invalid mobility: %r', mobility_dict)
            return 1
        return self.mobility.update(mob)

    def exp_user_env_start(self):
        """Start user environment."""
        LOGGER.debug('TurtlebotManager.exp_user_env_start')
        return self.user_env.start()

    def exp_user_env_stop(self):
        """Stop user environment."""
        LOGGER.debug('TurtlebotManager.exp_user_env_stop')
        return self.user_env.stop()

    def _position_log_start(self, user, exp_id):
        """Start logging position."""

        pose_topic = turtlebot.Turtlebot.POSE_TOPIC
        log_file = config.MEAS_FILE_FMT.format(user=user, exp_id=exp_id,
                                               hostname=self.NODE_HOSTNAME)

        self.position_log = user_measure.RobotPosition(pose_topic, log_file,
                                                       self.NODE_HOSTNAME,
                                                       exp_id)
        self.position_log.start()

    def _position_log_stop(self):
        """Stop logging position."""
        if self.position_log is not None:
            self.position_log.stop()
        self.position_log = None

    @staticmethod
    def exp_results_get(user, exp_id):
        """Create experiment results archive and return file path."""
        cfg = {'user': user, 'exp_id': exp_id}

        archive = config.MEAS_ARCHIVE_FMT.format(**cfg)
        measure_folder = config.MEAS_DIR_FMT.format(**cfg)

        with tarfile.open(archive, "w:gz") as tar:
            tar.add(measure_folder, arcname=str(exp_id))

        return archive

    @staticmethod
    def exp_results_del(user, exp_id):
        """Delete results from a finished experiment."""
        meas_dir = config.MEAS_DIR_FMT.format(user=user, exp_id=exp_id)
        try:
            shutil.rmtree(meas_dir)
        except OSError:
            pass
        return 0

    def exp_go_to_point(self, x, y,  # pylint:disable=invalid-name
                        theta=0.0, name=None):
        """Goto point."""
        try:
            mob = mobility.Mobility.from_point(x, y, theta, name)
        except ValueError as err:
            LOGGER.error('Invalid mobility: %r', err)
            return 1

        LOGGER.debug('TurtlebotManager.exp_go_to_point')
        return self.mobility.update(mob)

    def exp_go_to_dock(self):
        """Go to dock."""
        LOGGER.debug('TurtlebotManager.exp_go_to_dock')
        return self.mobility.dock()

    def admin_dock(self):
        """Run robot dock command."""
        LOGGER.debug('TurtlebotManager.admin_dock')
        #Go to the docking zone
        self.robot.go_to_dock()
        #Go to dock
        return self.robot._dock()

    def admin_undock(self):
        """Run robot undock command."""
        LOGGER.debug('TurtlebotManager.admin_undock')
        return self.robot.undock()

    def iotlab_node_reset(self):
        """Reset iotlab node."""
        LOGGER.debug('TurtlebotManager.iotlab_node_reset')
        ret = 0
        ret += self.robot.iotlab_node_alim(0)
        time.sleep(10)
        ret += self.robot.iotlab_node_alim(1)
        return ret

    def status(self):
        """Get the status of the robot."""
        return self.robot.status()

    def is_reachable(self, x, y):  # pylint:disable=invalid-name
        """Tell if point is reachable by the robot."""
        try:
            pose = helpers.pose(x, y)
            return self.robot.is_reachable(pose)
        except ValueError as err:
            LOGGER.error('Invalid pose: %r', err)
            return False

    def position(self):
        """Return robot position dict: x, y and theta."""
        return self.robot.position_dict()

    def robot_version(self):
        """Return robot version."""
        return self.ROBOT_VERSION

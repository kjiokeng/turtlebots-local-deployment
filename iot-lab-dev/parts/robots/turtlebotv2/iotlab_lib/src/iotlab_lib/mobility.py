# -*- coding:utf-8 -*-

# This file is a part of IoT-LAB robots
# Copyright (C) 2015 Univ. Strasbourg, INRIA (Contact: admin@iot-lab.info)
# Contributor(s) : see AUTHORS file
#
# This software is governed by the CeCILL license under French law
# and abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# http://www.cecill.info.
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Mobility object."""

import logging

from iotlab_lib import helpers

LOGGER = logging.getLogger('iotlab_lib')


class Mobility(object):
    """Mobility object. Implementing an Iterator.

    :param mobility_type: in ('predefined', 'userdefined', 'point', 'stop')
    :param points: list of points names
    :param coordinates: dict of point
    :param loop: should the circuit loop
    """

    TYPES = ('predefined', 'userdefined', 'point', 'stop')

    def __init__(self, mobility_type, points, coordinates, loop=True, **_):
        self._assertValueError(points is not None, 'points is None')
        self._assertValueError(coordinates is not None, 'coordinates is None')

        goals_dict = self._as_goals_dict(coordinates)
        # Disable 'loop' if there are only 1, 0 or None points
        is_circuit = len(points) >= 2
        loop = loop if is_circuit else False

        self._type = mobility_type
        self._goals = [(name, goals_dict[name]) for name in points]
        self._goals_iter = iter(self.goals)
        self._loop = loop
        self._circuit_round = 0
        # TODO add 'some "state" or just "name" to say
        # 'circuit' / 'go_to_point' / 'don't move'

        self._validate()

    @property  # Read only property
    def goals(self):
        """Return circuit goals list of tuple: (name, Pose)."""
        return self._goals

    @staticmethod
    def _as_goals_dict(coords):
        """Convert points dict to goals dict."""
        try:
            ret = {name: helpers.pose(**coord)
                   for name, coord in coords.items()}
            return ret
        except (TypeError, ValueError) as err:
            LOGGER.warning('Mobility:Invalid coordinate: %r', err)
            raise ValueError('Mobility:Invalid coordinate: %r' % err)

    def __iter__(self):
        return self

    def next(self):
        """Return next goal.

        :raises StopIteration: when no more goal is available
        """
        # TODO clean and handle log
        try:
            return self._goals_iter.next()
        except StopIteration:
            if not self._loop:
                raise

        # Reload goals and increment circuit_round
        # Without circuit_round we could use itertools.cycle
        self._circuit_round += 1
        LOGGER.debug('Mobility.next: circuit round %d', self._circuit_round)

        self._goals_iter = iter(self.goals)
        return self._goals_iter.next()

    def _validate(self):
        """Validate mobility."""
        self._assertValueError(self._type in self.TYPES,
                               "%s not in %s" % (self._type, self.TYPES))
        assert not (self._type in ('point', 'stop') and self._loop)

    @classmethod
    def from_dict(cls, mob_dict):
        """Create mobility from mobility_dict."""
        if mob_dict is None:
            return None
        try:
            # don't modify original dict
            mob_dict = mob_dict.copy()
            return cls(mob_dict.pop('type'), **mob_dict)
        except (KeyError, TypeError) as err:
            LOGGER.warning('Mobility.from_dict error: %r %r', err, mob_dict)
            raise ValueError('%r' % err)

    @classmethod
    def from_point(cls, x, y,  # pylint:disable=invalid-name
                   theta=0.0, name=None):
        """Create mobility from point."""
        name = name or 'point'
        points = [name]
        coordinates = {name: {'x': x, 'y': y, 'theta': theta, 'name': name}}
        return cls('point', points, coordinates, loop=False)

    @classmethod
    def from_stop(cls):
        """Create mobility to stop."""
        return cls('stop', [], {}, loop=False)

    @staticmethod
    def _assertValueError(cond, message):
        if cond:
            return
        raise ValueError(message)

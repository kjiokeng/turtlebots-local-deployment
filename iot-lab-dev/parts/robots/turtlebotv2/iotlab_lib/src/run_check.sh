#! /bin/sh


if [[ "$1" != "nolint" ]]
then
    pep8 iotlab_lib
    pylint --rcfile=pylint.rc iotlab_lib
fi

rm -rf cover  # issue with cover version
nosetests -v

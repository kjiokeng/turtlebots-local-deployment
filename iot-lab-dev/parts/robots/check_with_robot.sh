#! /usr/bin/bash

HOST=${1:-m3-204.devlille.iot-lab.info}
HOST=turtlebot@${HOST}

DEST='~/clochette_robot_tests/'
SRC_DIR='turtlebotv2/iotlab_lib/src'

rsync -e "ssh -F ssh_config" -avz --delete scripts/    ${HOST}:~/tests_scripts
rsync -e "ssh -F ssh_config" -avz --delete turtlebotv2 ${HOST}:${DEST}

ssh -t -F ssh_config ${HOST} "\
    source ~/.bashrosrc && cd ${DEST}/${SRC_DIR} && bash -x run_check.sh $@
    "

scp -F ssh_config ${HOST}:${DEST}/${SRC_DIR}/*xml ${SRC_DIR}

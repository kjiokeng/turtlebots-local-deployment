# TurtleBots Local Deployment

## Setup instructions
These instructions are adapted from [original Turtlebot-Installation.md](iot-lab-dev.wiki/Turtlebot-Installation.md)

### Hardware prerequisite

* a fully assembled Turtlebot version 2 :
   * 1x Kinect with electrical cable 12V@1A
   * 1x Netbook running under Ubuntu LTS with electrical cable 19V@2A. These instructions have been tested with Ubuntu 14.04 (and thus ROS indigo) but are supposed to work with Ubuntu 16.04 also (ROS kinetic)
   * 1x a Dock with AC adaptater
* an IoT-LAB M3 node :
   * 1x IoT-LAB nodes, with eletrical cable on the 5V@1A connector
   * 1x RJ45 between the IoT-LAB (uplink port) node and the netbook


### Resources configuration

Add your robots in nodes configuration file (eg. m3.csv) located in ``iot-lab-dev/install_lib/resources/dev/devlille``

  * node_id = M3 node id (eg. m3-<id>.site_name.iot-lab.info)
  * archi = m3:at86rf231
  * hwaddr = **Laptop Mac address** (eg. dynamic DHCP for M3 gateway node)
  * mobile = 1
  * mobility_type = turtlebot2
  * midspan_port = -1


### Wi-Fi prerequisite

You must have a **Wi-Fi hotspot** in your experimental room which routing Iot-LAB Gateway nodes private network of your site (eg. 172.16.x.0/21)

You can create this hotspot using any software of your choice. On Ubuntu operating system, this is quite simple. Here is an [example short tutorial](https://www.linuxuprising.com/2018/09/how-to-create-wi-fi-hotspot-in-ubuntu.html) on how to do it on Ubuntu 18.04.

Once you create your hotspot (with or without security), connect your robots and make sure to create IP reservation using robots MAC addresses.
If you are using NetworkManager (Ubuntu default), you can do this by creating a file named ``/etc/NetworkManager/dnsmasq-shared.d/<hotspot-name>.conf``. Here is a sample configuration you might add to this file.


```
log-dhcp
dhcp-range=172.16.0.2,172.16.255.254,24h
dhcp-option=option:netmask,255.240.0.0
dhcp-option=option:router,172.16.0.1
dhcp-option=option:ntp-server,ntp.inria.fr

dhcp-host=28:e3:47:bb:ea:18,172.16.74.101
dhcp-host=28:e3:47:b7:6e:8e,172.16.74.102
```


### Turtlebot configuration

Get ROS application for managing robots

```
your_computer$ cd iot-lab-dev
your_computer$ make setup-robots
your_computer$ cd parts/robots
your_computer$ git checkout <indigo|kinetic>
```

You have to create and add configuration files for your Client site (in ``iot-lab-dev/parts/robots/turtlebotv2/iotlab_maps/`` folder), or just update them if already existing.
Here, replace ``<site>`` with ``devlille`` (configuration files already existing for FIT room).

* a map site used by the AMCL navigation
   * maps/**\<site\>-iotlab.yaml** => description and scale
   * maps/**\<site\>-iotlab.pgm** => bitmap image1
* coordinates for Turtlebot's docks **(1)**
   * goals/**\<site\>_iotlab_docks_pose.yaml** 
* coordinates for Turtlebot's docking area **(2)**
   * goals/**\<site\>_showroom_docking_area.yaml**

**Be careful**, the name of the robot in this files have a pre-defined syntax **\<mobility_type\>\_\<archi\>\_\<node_id\>** (e.g. turtlebot2_m3_381)

**(1)** These coordinates are used for the initial position set at the beginning of the navigation

**(2)** These coordinates ared used for the docking approach (one meter in front of the dock's target)

See [iot-lab-lille-deployment.jpg] for coordinate system on Lille site.



### Server configuration file

In [iot-lab-dev/install_lib/config/dev.cfg], edit the entries in subsection ``localhost`` with your corresponding values.

**Note:** As from here, you can replace ``<host_kvm>`` with ``localhost`` everywhere you it appears.


**robot_netbook_*** and **robot_node_ipaddr** parameters are the network configuration for the M3 Iot-LAB node

**robot_battery_*** are the battery life threshold parameters. For example if the battery is below 20 percent the robot will go automatically to DOCK station.

**robot_ros_version** fix the version of ROS: indigo (if robots are using Ubuntu 14.04) or kinectic (if Ubuntu 16.04)



### Turtlebot Installation

1. Choose your Ubuntu Desktop version

   * for **ROS Indigo** use **Ubuntu 14.04.x** download the last version http://releases.ubuntu.com/14.04/
   * for **ROS Kinetic** use **Ubuntu 16.04.x** download the last version http://releases.ubuntu.com/16.04/ 
  
   Make a bootable USB stick with [Etcher](https://etcher.io).

2. Boot up your image on the robot netbook (Asus X200M **Echap key** for boot sequence)

3. Install options :

  * Language : English
  * Locale : en_US.UTF-8
  * Name and computer name = turtlebot
  * User : login = turtlebot password = turtlebot

  **At the end of the installation** you have to verify your options in Power Management (e.g. Don't Suspend the activity of the netbook)

4. Install Fabric
From [official website](https://www.fabfile.org/): Fabric is a high level Python (2.7, 3.4+) library designed to execute shell commands remotely over SSH, yielding useful Python objects in return.

Fabric is used in the project to automate intallation tasks.
There has been breaking API changes in newer versions of Fabric. Version 1.14 works well with the project.
``pip install Fabric==1.14.0``


5. Configure manually netbook network interfaces
  
  * Reminder about robot network interfaces
    * Wi-Fi : link to the IoT-LAB infrastructure
    * Ethernet : link to the embedded IoT-LAB node

  For the Wi-Fi configuration, we use wpa_supplicant configuration https://doc.ubuntu-fr.org/wpa
  with DHCP.

  * Add your robot in the DHCP server of your Client site

       ```
       your_computer$ cd iot-lab-dev/install-lib
       your_computer$ fab <prod|dev> srvdhcp.setup -H root@<host_kvm>:2222
       ```
  * Edit WPA configuration, by copying the content of `template/srvturtlebot2/wpa_<site>.conf`.
 
       ```
       netbook$ vi /etc/wpa.conf
       ```
      
  * Remove if installed **network-manager** package in order to use the static configuration file `/etc/network/interfaces` (see below)

       ```
       netbook$ sudo apt-get remove network-manager
       ```

  * Edit netbook's `/etc/network/interfaces` file and add this section (both Wi-Fi **and** Ethernet), please adapt interface name according to your kernel (check current names with ifconfig -a)

       ```
       auto wlan0
       iface wlan0 inet dhcp
            wpa-conf /etc/wpa.conf

       auto eth0
       iface eth0 inet static
            address 10.255.255.254
            netmask 255.255.255.0
            network 10.255.255.0
       ```

  * Test your Wi-Fi configuration 
       
       ```
       netbook$ sudo reboot
       ```

5. Install openssh-server on the netbook

    ```
    netbook$ sudo apt-get update
    netbook$ sudo apt-get install openssh-server
    ```

  <em>
  **Tip1** - When you specify `fab <dev|prod> ...` command, Fabric will execute the task on all the robots list based on your resources file (eg. based on mobility_type attribute = turtlebot2). If you want to execute the task only on a subset of your robots you can use a robot list id (e.g. based on node_id attribute) with the following command :

    ```
    your_computer$ fab <dev|prod>:robot_ids=1-3+8 <task_name> -H root@<host_kvm>:2222
    ```
  </em>


6. Add your public SSH key to ``iot-lab-dev/install_lib/template/sshkeys/admin_authorized_keys``

7. Configure SSH access on the netbook with admins and KVM host keys:

    ```
    your_computer$ fab <dev|prod> turtlebot2.add_ssh -H root@<host_kvm>:2222
    ```

  After this step your robot's netbook is reachable by KVM host with SSH on port 2222

  <em>
  **Tip2** - Add (or create) on your computer a SSH config file (eg. ~/.ssh/config) with this configuration:

  ```
  Host server_name
      User root
      Port 2222
      Hostname <host_kvm>

 Host turtlebot2-<id>
      User root
      Port 2222
      ProxyCommand ssh server_name -W m3-<id>.<site>.iot-lab.info:%p
      StrictHostKeyChecking no
  ```
 
  It allows you to connect directly to the robot netbook from your computer with this command:

  ```
  your_computer$ ssh turtlebot2-<id>
  ```
  </em>
        

8. Configure netbook system and install ROS

  ```
  your_computer$ fab <dev|prod> turtlebot2.setup_ros -H root@<host_kvm>:2222
  ```

after netbook reboot, fix ROS sources:

  ```
  your_computer$ fab <dev|prod> turtlebot2.setup_rosdep -H root@<host_kvm>:2222
  ``` 


9. Test your ROS installation 

  * View ROS service list

     ```
     your_computer$ ssh turtlebot2-<id>
     netbook$ su turtlebot
     netbook$ rosservice list |grep handle
       /turtlebot2_m3_381/handle_dock
       /turtlebot2_m3_381/handle_goal_planner
       /turtlebot2_m3_381/handle_led1
       /turtlebot2_m3_381/handle_led2
       /turtlebot2_m3_381/handle_node_power
     ```

  * DOCK and UNDOCK your robot

    ```
    netbook$ rosservice call /turtlebot2_m3_<id>/handle_dock "Dock_Value: UNDOCK"
    netbook$ rosservice call /turtlebot2_m3_<id>/handle_dock "Dock_Value: DOCK"
    ```

  * View ROS topic diagnostics_agg

    ```
    netbook$ rostopic echo /turtlebot2_m3_<id>/diagnostics_agg
    ```

10. Setup the M3 node installation

  We install on the netbook a server NFS/TFTP and DHCP for booting M3 node.

  ```
  your_computer$ fab <dev|prod> turtlebot2.setup_node -H root@<host_kvm>:2222
  ```

This command may fail. If this is the case, just continue to the next step.


11. Fix TFTP for booting M3 nodes
	- Make sure to install a TFTP server on your computer. [Here](https://doc.ubuntu-fr.org/tftpd) is how you can proceed for Ubuntu.
	- Then, configure the served directory to ``[tftpd one](tftpd)``



### The installation complete
At this step, your installation should be fully functional, with everything working well.

If it is not the case, take a look at the [Common issues section](#common-issues) below.




### [On your computer] Install ntp and sync with ntp.inria.fr
sudo apt-get install ntpdate
sudo ntpdate ntp.inria.fr
sudo timedatectl set-ntp off

sudo apt-get install ntp
echo "server ntp.inria.fr prefer iburst" >> /etc/ntp.conf
sudo service ntp restart
ntpq -p # Check the synchronization status (ntp.inria.fr should be marked with an asterisk)




## Common issues

### Gateway seems not to be working
**Problem:** It might happen that the gateway does not respond to any HTTP request. This means that it probably did not start well.
**Solution:** 
1. Launch wireshark on the netbook and capture traffic on eth0 interface.
2. Unplug the alim cable from the gateway and, few seconds later, plug it again
3. Repeat step 2 until you see TFTP protocol traffic and then NFS traffic between the gateway (10.255.255.1) and the netbook (10.255.255.254). This means that the gateway properly loaded its kernel image (TFTP) and properly mounted its filesystem (NFS).


### Gateway not getting IP address
**Problem:** In certain circumstances, you might observe in wireshark only DHCP request packets, but not subsequent traffic. The gateway is asking for an IP address but not getting anything back.

This might be due either to the dhcp server on the netbook not properly started or, more likely, to an IP address already reserved.
In fact this dhcp server is configured to give only a single IP address (10.255.255.254). So, if you plug another gateway, it might happen that the previous lease is still active (and associated to that previous gateway MAC address).

**Solution:** You have to free up all the leases and restart the dhcp server.
This command will do the job ``echo "" | sudo tee /var/lib/dhcp/dhcpd.leases && sudo service isc-dhcp-server restart``

